# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import re

from stickerface import callbacks
from stickerface import ctrl_flag
from stickerface import extra_attr
from stickerface.utils import error

reload(ctrl_flag)
reload(extra_attr)
reload(callbacks)
reload(error)

LOG = logging.getLogger(__name__)


#  _____          _       _   _   _      _
# |  ___|_ _  ___(_) __ _| | | | | | ___| |_ __   ___ _ __
# | |_ / _` |/ __| |/ _` | | | |_| |/ _ \ | '_ \ / _ \ '__|
# |  _| (_| | (__| | (_| | | |  _  |  __/ | |_) |  __/ |
# |_|  \__,_|\___|_|\__,_|_| |_| |_|\___|_| .__/ \___|_|
#                                         |_|

class FacialHelper(object):
    def __init__(self, cmds):
        self._cmds = cmds

    @property
    def cmds(self):
        return self._cmds

    def _filter_by_flag(self, obj, mask):
        attr = extra_attr.ExtraAttr.CTRL_FLAGS
        if not self.cmds.exists(obj=obj, attr=attr):
            return False
        return bool(self.cmds.get_attribute(obj=obj, attr=attr) & mask)

    def _filter_by_type(self, obj, typ):
        return self.cmds.get_type(obj=obj) == typ

    def get_faces(self):
        return self.cmds.filter_objects(
            cb=lambda obj: self._filter_by_flag(
                obj=obj,
                mask=ctrl_flag.CtrlFlag.ROOT))

    def get_controllers(self, top):
        ret = self.cmds.filter_descendents(
            obj=top,
            cb=lambda obj: self._filter_by_flag(
                obj=obj,
                mask=ctrl_flag.CtrlFlag.ANIM),
            keep_1st=False)
        LOG.info('{} controllers for {}\n\t{}'.format(len(ret), top, ret))
        return ret

    def get_material(self, root):
        ret = self.cmds.filter_outputs(
            obj=root,
            attr=extra_attr.ExtraAttr.GLOBAL_SCALE,
            keep_1st=True,
            cb=lambda obj: self._filter_by_type(obj=obj, typ='cgfxShader'))
        return ret[0] if ret else ''

    def get_atlas_path(self, mat):
        atlas_file = self.cmds.filter_inputs(
            obj=mat,
            attr='sprite_sheet',
            keep_1st=True,
            cb=lambda obj: self._filter_by_type(obj=obj, typ='file'))
        atlas_file = atlas_file[0] if atlas_file else ''
        LOG.info('file = {} ---> material = {}'.format(atlas_file, mat))
        if not atlas_file:
            return ''

        path = self.cmds.get_attribute(obj=atlas_file, attr='fileTextureName')
        return path if path else ''

    def get_atlas_grid_resolution(self, mat):
        num_cols = self.cmds.get_attribute(obj=mat, attr='uNumSpriteCols')
        num_rows = self.cmds.get_attribute(obj=mat, attr='uNumSpriteRows')
        return (
            num_cols if num_cols is not None else 0,
            num_rows if num_rows is not None else 0)

    #    ____    _ _       _     _   _
    #   / __ \  | (_) __ _| |__ | |_(_)_ __   __ _
    #  / / _` | | | |/ _` | '_ \| __| | '_ \ / _` |
    # | | (_| | | | | (_| | | | | |_| | | | | (_| |
    #  \ \__,_| |_|_|\__, |_| |_|\__|_|_| |_|\__, |
    #   \____/       |___/                   |___/

    def get_proxy_objects(self):
        return self.cmds.filter_objects(
            cb=lambda obj: self.cmds.exists(
                obj=obj,
                attr=extra_attr.decorate(
                    name=extra_attr.ExtraAttr.HAS_STICKERS)))

    def get_baked_face(self, asset_name):

        def _is_face_shape(obj):
            is_shape = any([
                self.cmds.get_type(obj=obj) == _
                for _ in ['mesh', 'nurbsSurface']
            ])
            if not is_shape:
                return False

            return self.cmds.get_attribute(
                obj=obj,
                attr=extra_attr.decorate(name=extra_attr.ExtraAttr.ASSET_NAME)
            ) == asset_name

        ret = self.cmds.filter_objects(cb=_is_face_shape)
        return ret[0] if ret else ''

    def setup_vray_texture(self, prx_data):

        def _get_attribute_from(attrs, tokens):
            '''
            Returns: the 1st attribute from the specified list whose name
            contains ALL required tokens.
            '''
            if not isinstance(tokens, list):
                raise error.Error(
                    'Invalid tokens argumments: {}'.format(tokens))
            ret = [
                attr
                for attr in attrs
                if tokens and all([_ in attr.lower() for _ in tokens])
            ]
            return ret[0] if ret else ''

        # get input values from the proxy data structure
        prx = prx_data.proxy
        asset_name = prx_data.asset_name
        body_path = prx_data.body_path
        atlas_path = prx_data.atlas_path
        shape = prx_data.shape

        LOG.info(
            'Setup VRay texture for proxy "{}"\n'
            '\t- asset = "{}"\n'
            '\t- body texture: {}\n'
            '\t- sprite sheet: {}\n'
            '\t- face shape: {}\n'
            '\t- gamma = {}'.format(
                prx, asset_name, body_path, atlas_path, shape, prx_data.gamma))

        # create a VRay texture depending on the proxy's projection type
        vr_tex = self.cmds.create_new_vraytexture(
            proj_type=prx_data.proj_type,
            name=asset_name + '_VRayTex')
        LOG.info('VRay texture -> "{}"'.format(vr_tex))

        # connect as many attributes from the VRay texture as possible
        prx_attrs = self.cmds.get_attributes(obj=prx)
        vr_attrs = self.cmds.get_attributes(obj=vr_tex)

        # set/connect general attributes
        for tokens in [
            ['sprite', 'sheet'],
            ['num', 'sprite', 'rows'],
            ['num', 'sprite', 'cols'],
        ]:
            prx_attr = _get_attribute_from(attrs=prx_attrs, tokens=tokens)
            vr_attr = _get_attribute_from(attrs=vr_attrs, tokens=tokens)
            if not prx_attr or not vr_attr:
                continue
            if self.cmds.connectable(obj=vr_tex, attr=vr_attr):
                self.cmds.connect(
                    src=prx,
                    dst=vr_tex,
                    attrs={vr_attr: prx_attr})
            else:
                prx_value = self.cmds.get_attribute(obj=prx, attr=prx_attr)
                if prx_value is not None:
                    self.cmds.set_attribute(
                        obj=vr_tex,
                        attr=vr_attr,
                        value=prx_value)

        # connect attributes related to individual sprites
        for suffix, rgx in {
            'frontWPosition':
                re.compile(r'\Asmks_uFrontW*Position_(\w+)([XYZ])\Z'),
            'index':
                re.compile(r'\Asmks_uIndex_(\w+)\Z'),
            'mapping':
                re.compile(r'\Asmks_uMapping_(\w+)([XYZ])\Z'),
        }.items():
            for prx_attr in prx_attrs:
                m = rgx.match(prx_attr)
                if not m:
                    continue
                groups = m.groups()
                sprt = groups[0]
                c = groups[1] if len(groups) >= 2 else ''
                vr_attr = sprt + '_' + suffix + c
                if vr_attr not in vr_attrs:
                    continue
                self.cmds.connect(
                    src=prx,
                    dst=vr_tex,
                    attrs={vr_attr: prx_attr})

        # connect the attributes related to the face's main frame
        rgx = re.compile(r'\Asmks_u(\w+)WPosition([XYZ])\Z')
        for prx_attr in prx_attrs:
            m = rgx.match(prx_attr)
            if not m:
                continue
            vr_attr = 'cube{}WPosition{}'.format(m.group(1), m.group(2))
            if vr_attr not in vr_attrs:
                continue
            self.cmds.connect(
                src=prx,
                dst=vr_tex,
                attrs={vr_attr: prx_attr})

        for prx_attr, vr_attr in {
            'smks_uCenter': 'centerWPosition',
            'smks_uUpPosition': 'upWPosition',
        }.items():
            for c in ['X', 'Y', 'Z']:
                self.cmds.connect(
                    src=prx,
                    dst=vr_tex,
                    attrs={vr_attr + c: prx_attr + c})

        # create a maya texture for the body
        body_file, body_p2dtex = self.cmds.create_texture_from_path(
            path=body_path)

        # create a gamma correction node and have it natively handle the
        # correction of the body texture injected to the VRay texture.
        # gamma correction for the sprite atlas is different since it is
        # directly performed by the Vray texture itself: for consistency,
        # the gamma value from the node must be connected to the Vray texture.
        gamma_node = self.cmds.gamma_correct(
            name=asset_name + '_gamma',
            gamma=prx_data.gamma,
            input_value=(body_file, 'outColor'))
        LOG.info('Gamma correct -> "{}"'.format(gamma_node))

        self.cmds.connect(
            src=gamma_node,
            dst=vr_tex,
            attrs={'spriteGamma' + _: 'gamma' + _ for _ in ['X', 'Y', 'Z']})

        vr_attr = _get_attribute_from(
            attrs=vr_attrs,
            tokens=['body', 'texture'])
        LOG.info('Body texture attribute from {}: {}'.format(vr_tex, vr_attr))

        self.cmds.connect(
            src=gamma_node,
            dst=vr_tex,
            attrs={
                vr_attr + _1: 'outValue' + _2
                for _1, _2 in zip(['R', 'G', 'B'], ['X', 'Y', 'Z'])})

        # assign a VRay material to the shape
        vr_mtl, vr_sg = self.cmds.assign_new_vraymaterial(
            obj=shape,
            name=asset_name + '_VRayMtl')
        LOG.info(
            'VRay material -> "{}" (shading group = "{}")'.format(
                vr_mtl, vr_sg))

        # connect the VRay texture to the assign material
        self.cmds.connect(
            src=vr_tex,
            dst=vr_mtl,
            attrs={'color': 'outColor'})
