# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import os
import sys

from maya import cmds as mc
from maya import mel as mm

from stickerface import attr_type
from stickerface import callbacks
from stickerface import ctrl_flag
from stickerface import m_gui
from stickerface.cgfx import shader_proj
from stickerface.utils import cond
from stickerface.utils import error
from stickerface.utils import math

reload(callbacks)
reload(attr_type)
reload(ctrl_flag)
reload(error)
reload(math)
reload(cond)
reload(shader_proj)
reload(m_gui)

LOG = logging.getLogger(__name__)


#  __  __                                        _
# |  \/  | __ _ _   _  __ _    ___ _ __ ___   __| |___
# | |\/| |/ _` | | | |/ _` |  / __| '_ ` _ \ / _` / __|
# | |  | | (_| | |_| | (_| | | (__| | | | | | (_| \__ \
# |_|  |_|\__,_|\__, |\__,_|  \___|_| |_| |_|\__,_|___/
#               |___/

class Callbacks(callbacks.Callbacks):
    ATTR_FILTERS = {
        attr_type.AttrType.INT: ['long'],
        attr_type.AttrType.NUMBER: ['doubleLinear', 'double', 'long'],
        attr_type.AttrType.V3F: ['float3'],
    }

    @staticmethod
    def is_str(s):
        return any([isinstance(s, _) for _ in [str, unicode]])

    def begin_undo(self):
        LOG.info('begin undo chunk...')
        mc.undoInfo(openChunk=True, infinity=True, state=True)

    def end_undo(self):
        mc.undoInfo(closeChunk=True, state=True)
        LOG.info('end undo chunk.')

    def exists(self, obj, attr=''):
        if not mc.objExists(obj):
            return False
        if attr:
            return mc.attributeQuery(attr, node=obj, exists=True)
        return True

    def get_type(self, obj):
        return mc.objectType(obj) if self.exists(obj=obj) else ''

    def _get_attribute_type(self, obj, attr):
        try:
            return mc.getAttr(obj + '.' + attr, type=True)
        except ValueError:
            return ''

    def _is_color_attribute(self, obj, attr):
        return \
            self._get_attribute_type(obj=obj, attr=attr) == 'float3' and \
            self.exists(obj=obj, attr=attr + 'R')

    def filter_objects(self, cb=None):
        return [
            _
            for _ in mc.ls(long=True)
            if not cb or cb(obj=_)
        ]

    def filter_descendents(self, obj, cb=None, keep_1st=False):
        ret = []
        for _ in mc.listRelatives(obj, fullPath=True, allDescendents=True):
            if not cb or cb(obj=_):
                ret.append(_)
                if keep_1st:
                    break
        return ret

    def _add_attribute(
        self,
        obj,
        attr,
        value,
        as_color=False,
        min_value=None,
        max_value=None
    ):
        if not self.exists(obj=obj) or self.exists(obj=obj, attr=attr):
            return False

        try:
            if self.is_str(s=value):
                mc.addAttr(obj, longName=attr, dataType='string')
                LOG.info('(+ str) {}.{}'.format(obj, attr))
            elif isinstance(value, bool):
                mc.addAttr(obj, longName=attr, attributeType='bool')
                LOG.info('(+ bool) {}.{}'.format(obj, attr))
            else:
                attr_type = ''
                if isinstance(value, float):
                    attr_type = 'double'
                elif isinstance(value, int):
                    attr_type = 'long'
                elif math.is_v3f(v=value):
                    attr_type = 'double3'
                kwargs = {}
                if any([isinstance(value, _) for _ in [float, int]]):
                    if min_value is not None:
                        kwargs['minValue'] = min_value
                    if max_value is not None:
                        kwargs['maxValue'] = max_value
                LOG.info('(+) {} <- {}'.format(attr, kwargs))
                mc.addAttr(
                    obj,
                    longName=attr,
                    attributeType=attr_type,
                    **kwargs)
                LOG.info('(+ {}) {}.{}'.format(attr_type, obj, attr))
            mc.setAttr(obj + '.' + attr, edit=True, keyable=True)
            return True

        except RuntimeError:
            LOG.info(
                'Failed to create attribute "{}" on "{}".'.format(
                    attr, obj))
            return False

    def set_attribute(
        self,
        obj,
        attr,
        value,
        as_color=False,
        lock=False,
        min_value=None,
        max_value=None
    ):
        if not self.exists(obj=obj):
            return False

        if value is None:
            raise error.Error(
                'Attribute value cannot be None: "{}" on "{}"'.format(
                    attr, obj))

        if not self.exists(obj=obj, attr=attr):
            if not self._add_attribute(
                obj=obj,
                attr=attr,
                value=value,
                as_color=as_color,
                min_value=min_value,
                max_value=max_value
            ):
                return False  # failed to create new attribute
        try:
            attr_name = obj + '.' + attr
            if math.is_v3f(v=value):
                channels = \
                    ['R', 'G', 'B'] \
                    if self._is_color_attribute(obj=obj, attr=attr) \
                    else ['X', 'Y', 'Z']
                for c, v in zip(channels, value):
                    mc.setAttr(attr_name + c, v)

            elif self.is_str(s=value):
                value = value.replace('\\', '/')
                mc.setAttr(attr_name, value, type='string')

            elif isinstance(value, bool):
                mc.setAttr(attr_name, 1 if value else 0)

            else:
                channels = []
                if self._is_color_attribute(obj=obj, attr=attr):
                    channels = ['R', 'G', 'B']
                elif self._get_attribute_type(obj=obj, attr=attr) == 'float3':
                    channels = ['X', 'Y', 'Z']
                if not channels:
                    mc.setAttr(attr_name, value)
                else:
                    for c in channels:
                        mc.setAttr(attr_name + c, value)
            LOG.info('{:s} := {}'.format(attr_name, value))

            if lock:
                mc.setAttr(attr_name, lock=True)
                LOG.info('{:s} locked.'.format(attr_name))
            return True
        except RuntimeError as e:

            LOG.info('/!\\ Failed to set {:s}.\n{:s}'.format(
                attr_name, e.message))
            return False

    def get_attribute(self, obj, attr):
        if not self.exists(obj=obj, attr=attr):
            return None
        return mc.getAttr(obj + '.' + attr)

    def get_attribute_range(self, obj, attr):
        if not self.exists(obj=obj, attr=attr):
            return (None, None)

        def _process(v):
            typ = self._get_attribute_type(obj=obj, attr=attr)
            if typ == 'long':
                return int(v[0])
            elif typ == 'float':
                return v[0]
            else:
                return v

        return (
            _process(v=mc.attributeQuery(attr, node=obj, min=True))
            if mc.attributeQuery(attr, node=obj, minExists=True)
            else None,
            _process(v=mc.attributeQuery(attr, node=obj, max=True))
            if mc.attributeQuery(attr, node=obj, maxExists=True)
            else None,
        )

    def force_step_key_tangents(self, obj, attr):
        if not self.exists(obj=obj, attr=attr):
            return
        # get attribute value before tangent editing
        value = self.get_attribute(obj=obj, attr=attr)
        # force all anim tangents at all keys to behave as steps
        mc.keyTangent(
            obj,
            attribute=attr,
            edit=True,
            time=self.get_time_range(),
            inTangentType='clamped',
            outTangentType='step')
        # set attribute value again, in case tangent editing changed it
        if value is not None:
            self.set_attribute(obj=obj, attr=attr, value=value)

    def get_time_range(self):
        return (
            mc.playbackOptions(query=True, min=True),
            mc.playbackOptions(query=True, max=True))

    def get_current_time(self):
        return mc.currentTime(query=True)

    def eval_key_at_current_time(self, obj, attr):
        '''
        Returns: key value at current time if any, else None.
        '''
        if not self.exists(obj=obj, attr=attr):
            return None
        t = self.get_current_time()
        if not mc.keyframe(
            obj,
            attribute=attr,
            time=(t, t),
            query=True,
            keyframeCount=True
        ):
            return None
        ret = mc.keyframe(
            obj,
            attribute=attr,
            time=(t, t),
            query=True,
            eval=True)
        return ret[0] if ret else None

    def get_attributes(self, obj, typ=attr_type.AttrType.ALL):

        filters = Callbacks.ATTR_FILTERS[typ] \
            if typ in Callbacks.ATTR_FILTERS \
            else []
        attrs = mc.listAttr(obj) if self.exists(obj=obj) else []
        if filters:
            attrs = [
                attr
                for attr in attrs
                if any([
                    self._get_attribute_type(obj=obj, attr=attr) == t
                    for t in filters
                ])
            ]
        return attrs

    def _split_plug(self, plug):
        obj, attr = plug.split('.', 1)
        obj = mc.ls(obj, long=True)
        return obj[0], attr if obj else ()

    def get_input_connection(self, obj, attr):
        '''
        Returns: (obj, attr)
        '''
        cnx = \
            mc.listConnections(obj + '.' + attr, plugs=True, source=True) \
            if self.exists(obj=obj, attr=attr) \
            else []
        return self._split_plug(plug=cnx[0]) if cnx else ()

    def _filter_connections(self, obj, attr, cb, traverse_inputs, keep_1st):

        def _get_attribute_connections(obj, attr):
            if traverse_inputs:
                cnx = self.get_input_connection(obj=obj, attr=attr)
                return [cnx[0]] if cnx else []
            else:
                return list(set([
                    _[0]
                    for _ in self.get_output_connections(obj=obj, attr=attr)
                ]))

        def _get_all_connections(obj):
            ret = []
            for attr in self.get_attributes(obj=obj):
                ret.extend(_get_attribute_connections(obj=obj, attr=attr))
            return list(set(ret))

        def _traverse(obj, visited_wrap, ret_wrap):
            if obj in visited_wrap[0]:
                return
            visited_wrap[0].append(obj)
            if cb(obj=obj):
                ret_wrap[0].append(obj)
                if keep_1st:
                    return
            for _ in _get_all_connections(obj=obj):
                _traverse(obj=_, visited_wrap=visited_wrap, ret_wrap=ret_wrap)
                if keep_1st and len(ret_wrap[0]) > 0:
                    break

        ret = []
        if (cb(obj=obj)):
            ret.append(obj)
        cnx = _get_attribute_connections(obj=obj, attr=attr)
        if cnx:
            visited = [obj]
            _traverse(obj=cnx[0], visited_wrap=[visited], ret_wrap=[ret])
        return ret

    def filter_inputs(self, obj, attr, cb, keep_1st=False):
        '''
        Returns: list of input connected nodes s.t. cb(obj) = True (recursive)
        '''
        return self._filter_connections(
            obj=obj,
            attr=attr,
            cb=cb,
            traverse_inputs=True,
            keep_1st=keep_1st)

    def get_output_connections(self, obj, attr):
        '''
        Returns: [(obj, attr)]
        '''
        cnx = \
            mc.listConnections(obj + '.' + attr, plugs=True, destination=True)\
            if self.exists(obj=obj, attr=attr) \
            else []
        if not cnx:
            cnx = []
        return list(set([self._split_plug(plug=_) for _ in cnx]))

    def filter_outputs(self, obj, attr, cb, keep_1st=False):
        '''
        Returns: list of output connected nodes s.t. cb(obj) = True (recursive)
        '''
        return self._filter_connections(
            obj=obj,
            attr=attr,
            cb=cb,
            traverse_inputs=False,
            keep_1st=keep_1st)

    def connectable(self, obj, attr):
        try:
            return mc.attributeQuery(attr, node=obj, connectable=True)
        except RuntimeError:
            return False
        except TypeError:
            return False

    def connect(self, src, dst, attrs):
        '''
        attrs: either a list of attribute names, either a dictionary in
        {dst_attr: src_attr} format.
        Returns: True upon success
        '''
        status = []
        if isinstance(attrs, list):
            status = [
                self._connect(src=src, src_attr=_, dst=dst, dst_attr=_)
                for _ in attrs
            ]
        elif isinstance(attrs, dict):
            status = [
                self._connect(src=src, src_attr=s, dst=dst, dst_attr=d)
                for d, s in attrs.items()
            ]
        return all(status)

    def _connect(self, src, src_attr, dst, dst_attr):
        try:
            if all([
                self._get_attribute_type(obj=src, attr=src_attr) == 'float',
                self._get_attribute_type(obj=dst, attr=dst_attr) == 'float3',
            ]):
                channels = \
                    ['R', 'G', 'B'] \
                    if self._is_color_attribute(obj=dst, attr=dst_attr) \
                    else ['X', 'Y', 'Z']
                for c in channels:
                    mc.connectAttr(
                        src + '.' + src_attr,
                        dst + '.' + dst_attr + c,
                        force=True)
            else:
                mc.connectAttr(
                    src + '.' + src_attr,
                    dst + '.' + dst_attr,
                    force=True)
            return True
        except RuntimeError as e:
            LOG.info(
                '/!\\ Failed to connect {:s}.{:s} to {:s}.{:s}\n{:s}'.format(
                    src, src_attr, dst, dst_attr, e.message))
            return False

    def set_selection(self, objs=[]):
        try:
            mc.select(objs)
        except ValueError:
            pass

    def get_selection(self):
        return mc.ls(long=True, selection=True)

    def _get_shapes(self, obj):
        ret = mc.ls(obj, long=True, shapes=True)
        if not ret and self.exists(obj=obj):
            ret = mc.listRelatives(obj, fullPath=True, shapes=True)
        return ret if ret else []

    def get_selected_shapes(self):
        return list(set([
            _
            for obj in self.get_selection()
            for _ in self._get_shapes(obj=obj)
        ]))

    def get_shading_groups(self, obj):
        return list(set([
            _
            for _ in mc.listConnections(
                self._get_shapes(obj=obj),
                type='shadingEngine')
        ]))

    def assign_new_material(self, obj, typ, name=''):
        shapes = self._get_shapes(obj=obj)
        if not shapes or not typ:
            return []

        mat = mc.shadingNode(
            typ,
            asShader=True,
            name=name if name else typ)
        sg = mc.sets(
            noSurfaceShader=True,
            empty=True,
            renderable=True,
            name=mat + '_SG')
        self.connect(src=mat, dst=sg, attrs={'surfaceShader': 'outColor'})
        mc.sets(shapes, edit=True, forceElement=sg)
        LOG.info(
            'assign material "{}" (shading group "{}") to \n\t{}'.format(
                mat, sg, '\n\t'.join(shapes)))
        return [mat, sg]

    def assign_new_cgfxshader(self, obj, shader_path, name=''):
        '''
        Returns: created nodes
        '''
        self._load_plugin(plugin='cgfxShader')
        nodes = self.assign_new_material(
            obj=obj,
            typ='cgfxShader',
            name=name)
        if nodes:
            mat, sg = nodes
            shader_path = shader_path.replace('\\', '/')
            mm.eval('cgfxShader -e -fx "{}" {};'.format(shader_path, mat))
        return nodes

    def assign_new_vraymaterial(self, obj, name=''):
        '''
        Returns: created nodes
        '''
        self._load_plugin(plugin='vrayformaya')
        return self.assign_new_material(
            obj=obj,
            typ='VRayMtl',
            name=name)

    def _load_plugin(self, plugin):
        if mc.pluginInfo(plugin, query=True, loaded=True):
            return
        LOG.info("about to load '{}' plugin from {}".format(plugin, os.path.dirname(__file__)))
        
        # Attempt to load plugin directly
        try:
            mc.loadPlugin(plugin)
        except RuntimeError:
            # Attempt to load plugin locally
            try:
                dirname = os.path.dirname(__file__)
                mc.loadPlugin(os.path.join(dirname, 'plugin', plugin))
            except RuntimeError as e:
                raise error.Error('Failed to load plugin "{}"\n{}'.format(
                    plugin, e.message))

    def create_new_vraytexture(self, proj_type, name):
        '''
        Returns: created node
        '''
        PLUGINS = {
            shader_proj.ShaderProj.SPHERICAL: 'StickerFace_SphereTexture',
            shader_proj.ShaderProj.CUBIC: 'StickerFace_CubeTexture',
            shader_proj.ShaderProj.PLANAR: 'StickerFace_PlaneTexture',
        }

        self._load_plugin(plugin='vrayformaya')
        plugin = PLUGINS.get(proj_type, '')
        if not plugin:
            raise error.Error(
                'Unsupported projection type ({}).'.format(proj_type))

        cmd = 'vrayCreateNodeFromDll("{}", "texture", "{}", 0)'.format(
            name,
            plugin)
        LOG.info('Creating instance of VRay texture:\n\t{}'.format(cmd))
        return mm.eval(cmd)

    def create_node(self, name, typ, parent=''):
        '''
        Returns: created node
        '''
        PLUGINS = {
            'decomposeMatrix': 'matrixNodes',
            'GetSpriteRotate': 'StickerFace_GetSpriteRotate.py',
            'GetDeterminant3x3': 'StickerFace_GetDeterminant3x3.py',
        }
        if typ in PLUGINS:
            self._load_plugin(plugin=PLUGINS[typ])

        node = \
            mc.createNode(typ, name=name, parent=parent) \
            if parent \
            else mc.createNode(typ, name=name)
        return mc.ls(node)[0]

    def create_group(
        self,
        name,
        parent='',
        ws_mtx=None
    ):
        '''
        Returns: created node
        '''
        trf = self.create_node(typ='transform', name=name, parent=parent)
        if math.is_m44f(m=ws_mtx):
            self.set_world_matrix(obj=trf, mtx=ws_mtx)
        return trf

    def _rename(self, obj, name):
        ret = mc.ls(mc.rename(obj, name), long=True)
        LOG.info('Rename "{:s}" to "{:s}"'.format(obj, name))
        return ret[0] if ret else ''

    def create_curve(
        self,
        name,
        parent='',
        cvs=[],
        ctrl_flags=0
    ):
        '''
        Returns: created nodes
        '''
        trf = ''
        curv = ''
        if cvs and (len(cvs) - 1) % 3 == 0:
            kts = [i for i in range(len(cvs) / 3 + 1) for j in range(3)]
            trf = mc.curve(bezier=True, degree=3, point=cvs, knot=kts)
            trf = self.set_parent(child=trf, parent=parent, keep_local=True)
            trf = self._rename(obj=trf, name=name)
            curv = self._get_shapes(obj=trf)[0]
        else:
            trf = self.create_group(name=name, parent=parent)
            curv = self.create_node(typ='nurbsCurve', parent=trf)
        curv = self._rename(obj=curv, name=self.get_short_name(trf) + '_curv')

        # configure display if controller
        if ctrl_flags != 0:
            color = m_gui.OverrideColor.Yellow
            if (ctrl_flags & ctrl_flag.CtrlFlag.LEFT) != 0:
                color = m_gui.OverrideColor.Blue
            elif (ctrl_flags & ctrl_flag.CtrlFlag.RIGHT) != 0:
                color = m_gui.OverrideColor.Red
            self.set_attribute(obj=curv, attr='overrideEnabled', value=1)
            self.set_attribute(obj=curv, attr='overrideColor', value=color)

        return [trf, curv]

    def create_locator(
        self,
        name,
        parent='',
        ws_pos=None,
        show=False
    ):
        '''
        Returns: created nodes
        '''
        LOG.info('create locator w_pos = {}'.format(ws_pos))
        ws_mtx = math.translation_matrix(v=ws_pos) if ws_pos else None
        trf = self.create_group(name=name, parent=parent, ws_mtx=ws_mtx)
        loc = self.create_node(typ='locator', name=trf + '_shape', parent=trf)
        if not show:
            mc.hide(loc)
        return [trf, loc]

    def create_texture_from_path(self, path):
        '''
        Returns: created nodes
        '''
        if not os.path.isfile(path):
            raise error.Error('"{:s}"" is not a valid file.'.format(path))

        name, ext = os.path.splitext(os.path.basename(path))
        file = self.create_node(typ='file', name=name)
        self.set_attribute(obj=file, attr='fileTextureName', value=path)

        p2dtex = self.create_node(typ='place2dTexture', name=name + '_p2dtex')
        self.connect(
            src=p2dtex,
            dst=file,
            attrs=[
                'coverage',
                'mirrorU',
                'mirrorV',
                'noiseUV',
                'offset',
                'repeatUV',
                'rotateFrame',
                'rotateUV',
                'stagger',
                'translateFrame',
                'vertexCameraOne',
                'vertexUvOne',
                'vertexUvTwo',
                'vertexUvThree',
                'wrapU',
                'wrapV'
            ])
        self.connect(
            src=p2dtex,
            dst=file,
            attrs={
                'uvCoord': 'outUV',
                'uvFilterSize': 'outUvFilterSize',
            })

        return [file, p2dtex]

    def gamma_correct(self, name, gamma, input_value):
        obj = self.create_node(typ='gammaCorrect', name=name)
        self._set_or_connect2(obj=obj, attr='gamma', value=gamma)
        self._set_or_connect2(obj=obj, attr='value', value=input_value)
        return obj

    def mathop(self, op, name, in1, in2, attrs):
        if len(attrs) == 1:
            attr1, attr2 = attrs[0]
            return self._mathop_1f(
                op=op, name=name, in1=in1, attr1=attr1, in2=in2, attr2=attr2)
        elif len(attrs) <= 3:
            return self._mathop_3f(
                op=op, name=name, in1=in1, in2=in2, attrs=attrs)
        return ()

    def _mathop_1f(self, op, name, in1, attr1, in2, attr2):
        FUNCS = {
            math.Op.ADD: self._add_1f,
            math.Op.MUL: self._mul_1f,
        }
        if op in FUNCS:
            return FUNCS[op](
                name=name, in1=in1, attr1=attr1, in2=in2, attr2=attr2)
        raise NotImplementedError(
            '_mathop_1f for op = {}'.format(op))

    def _mathop_3f(self, op, name, in1, in2, attrs):
        FUNCS = {
            math.Op.MUL: self._mul_3f,
            math.Op.SUB: self._sub_3f,
        }
        if op in FUNCS:
            return FUNCS[op](
                name=name, in1=in1, in2=in2, attrs=attrs)
        raise NotImplementedError(
            '_mathop_3f for op = {}'.format(op))

    def _set_or_connect(self, obj, attr, op_obj, op_attr):
        if obj and self.is_str(s=attr):
            self.connect(src=obj, dst=op_obj, attrs={op_attr: attr})
        else:
            self.set_attribute(obj=op_obj, attr=op_attr, value=attr)

    def _set_or_connect2(self, obj, attr, value, as_color=False):

        def _is_connectable(value):
            return \
                isinstance(value, tuple) and \
                len(value) == 2 and \
                all([
                    any([isinstance(v, _) for _ in [str, unicode]])
                    for v in value])

        if _is_connectable(value=value):
            self.connect(
                src=value[0],
                dst=obj,
                attrs={attr: value[1]})
        elif value is not None:
            self.set_attribute(
                obj=obj,
                attr=attr,
                value=value,
                as_color=as_color)

    def condition(self, lvalue, op, rvalue, if_true, if_false, name):
        '''
        Returns: (created node, output attribute)
        '''
        OP = {
            cond.Comp.EQ: 0,
            cond.Comp.NOT_EQ: 1,
            cond.Comp.GREATER: 2,
            cond.Comp.GREATER_EQ: 3,
            cond.Comp.LESS: 4,
            cond.Comp.LESS_EQ: 5,
        }
        if op not in OP:
            raise error.Error('Unknown comparison operator (={})'.format(op))

        obj = self.create_node(typ='condition', name=name)
        for attr, value in {
            'firstTerm': lvalue,
            'operation': op,
            'secondTerm': rvalue,
            'colorIfTrue': if_true,
            'colorIfFalse': if_false,
        }.items():
            self._set_or_connect2(
                obj=obj,
                attr=attr,
                value=value,
                as_color='color' in attr)
        return (obj, 'outColor')

    def apply_matrix(self, mtx, v3f, as_point, name):
        '''
        Returns: (created node, output attribute)
        '''
        obj = self.create_node(typ='vectorProduct', name=name)
        for attr, value in {
            'operation': 4 if as_point else 3,  # mtx * vec/pos
            'input1': v3f,
            'matrix': mtx,
        }.items():
            self._set_or_connect2(obj=obj, attr=attr, value=value)
        return (obj, 'output')

    def dot(self, in1, in2, name):
        '''
        Returns: (created node, output attribute)
        '''
        obj = self.create_node(typ='vectorProduct', name=name)
        for attr, value in {
            'operation': 1,  # dot product
            'input1': in1,
            'input2': in2,
        }.items():
            self._set_or_connect2(obj=obj, attr=attr, value=value)
        return (obj, 'outputX')

    def _add_1f(self, name, in1, attr1, in2, attr2):
        op = self.create_node(typ='addDoubleLinear', name=name)
        self._set_or_connect(obj=in1, attr=attr1, op_obj=op, op_attr='input1')
        self._set_or_connect(obj=in2, attr=attr2, op_obj=op, op_attr='input2')
        return (op, 'output')

    def _mul_1f(self, name, in1, attr1, in2, attr2):
        op = self.create_node(typ='multDoubleLinear', name=name)
        self._set_or_connect(obj=in1, attr=attr1, op_obj=op, op_attr='input1')
        self._set_or_connect(obj=in2, attr=attr2, op_obj=op, op_attr='input2')
        return (op, 'output')

    def _mul_3f(self, name, in1, in2, attrs):
        op = self.create_node(typ='multiplyDivide', name=name)
        for c, (attr1, attr2) in zip(['X', 'Y', 'Z'], attrs):
            self._set_or_connect(
                obj=in1,
                attr=attr1,
                op_obj=op,
                op_attr='input1' + c)
            self._set_or_connect(
                obj=in2,
                attr=attr2,
                op_obj=op,
                op_attr='input2' + c)
        return (op, 'output')

    def _sub_3f(self, name, in1, in2, attrs):
        op = self.create_node(typ='plusMinusAverage', name=name)
        self.set_attribute(obj=op, attr='operation', value=2)  # subtract

        for c, (attr1, attr2) in zip(['x', 'y', 'z'], attrs):
            self._set_or_connect(
                obj=in1,
                attr=attr1,
                op_obj=op,
                op_attr='input3D[0].input3D' + c)
            self._set_or_connect(
                obj=in2,
                attr=attr2,
                op_obj=op,
                op_attr='input3D[1].input3D' + c)
        return (op, 'output3D')

    def switch(self, ctrl, ctrl_name, in0, in1, attrs):
        self.set_attribute(obj=ctrl, attr=ctrl_name, value=False)
        if len(attrs) == 1:
            attr0, attr1 = attrs[0]
            return self._switch_1f(
                ctrl=ctrl,
                ctrl_name=ctrl_name,
                inputs=[(in0, attr0), (in1, attr1)])
        return ()

    def _switch_1f(self, obj, attr, inputs):
        if not self.exists(obj=obj, attr=attr):
            self.set_attribute(obj=obj, attr=attr, value=False)

        op = self.create_node(typ='blendTwoAttr', name=attr + '_blend')
        for i, (obj, attr) in inputs:
            op_attr = 'input[{:d}]'.format(i)
            self._set_or_connect(
                obj=obj, attr=attr, op_obj=op, op_attr=op_attr)
        self._connect(src=obj, dst=op, attrs={'attributesBlender': attr})
        return (op, 'output')

    def parent_constrain(self, obj, parent, keep_offset=True):
        if not all([self.exists(obj=_) for _ in [obj, parent]]):
            return
        mc.parentConstraint(
            parent,
            obj,
            weight=1.0,
            maintainOffset=keep_offset)

    def set_parent(self, child, parent, keep_local=False):
        '''
        Returns: child's new fullpath
        '''
        if not self.exists(obj=child):
            return ''
        if self.exists(obj=parent):
            child = mc.parent(child, parent, relative=keep_local)
        return mc.ls(child, long=True)[0]

    def _get_relatives(self, obj, parent=False, children=False):
        if not self.exists(obj=obj):
            return []
        ret = mc.listRelatives(
            obj, parent=parent, children=children, fullPath=True)
        return ret if ret else []

    def get_parents(self, obj):
        return self._get_relatives(obj=obj, parent=True)

    def get_children(self, obj):
        return self._get_relatives(obj=obj, children=True)

    def _get_members(self, obj):
        ret = mc.sets(obj, query=True) if self.exists(obj=obj) else []
        return ret if ret else []

    def _add_member(self, obj_set, obj):
        try:
            mc.sets(obj, add=obj_set)
        except Exception as e:
            LOG.warning('Failed to add "{:s}" to "{:s}"\n{:s}'.format(
                obj, obj_set, e.message))

    def create_object_set(self, objs, name, parent=''):
        LOG.info(
            'Create selection {:s} for...\n\t{:s}'.format(
                name, '\n\t'.join(objs)))

        obj_sets = self.get_object_sets()
        if name in obj_sets:
            LOG.info('Returns object set {:s}'.format(name))
            for obj in objs:
                self._add_member(obj_set=name, obj=obj)
            return name

        ret = mc.sets(objs, name=name)
        if parent:
            self._add_member(obj_set=parent, obj=ret)
        return ret

    def get_object_sets(self):
        '''
        Returns: {'set': ['parent sets']}
        '''
        def _valid_member(obj):
            return any([
                self.get_type(obj=obj) == t
                for t in ['objectSet', 'joint', 'transform']
            ])

        ret = {}
        for s in mc.ls(type='objectSet', long=True):
            members = self._get_members(obj=s)
            if members and all([_valid_member(obj=_) for _ in members]):
                ret[s] = []
        for s in ret.keys():
            for m in self._get_members(obj=s):
                if m in ret:
                    ret[m].append(s)
        return ret

    def set_pivot(self, obj, ws_pos):
        if not self.exists(obj=obj) or not math.is_v3f(v=ws_pos):
            return
        px, py, pz = ws_pos
        for _ in ['rotate', 'scale']:
            LOG.info('maya.cmds.move({}, {}, {}, {})'.format(
                px, py, pz, '{:s}.{:s}Pivot'.format(obj, _)))
            mc.move(
                px, py, pz,
                '{:s}.{:s}Pivot'.format(obj, _),
                absolute=True,
                worldSpace=True)

    def set_world_matrix(self, obj, mtx):
        if isinstance(mtx, list) and len(mtx) == 16:
            mc.xform(obj, worldSpace=True, matrix=mtx)
            LOG.info('"{:s}" world_mtx := {}'.format(obj, mtx))

    def get_world_matrix(self, obj):
        return mc.xform(obj, query=True, matrix=True, worldSpace=True)

    def get_world_bbox(self, objs):
        ret = [sys.float_info.max] * 3 + [-sys.float_info.max] * 3
        shapes = [_ for obj in objs for _ in self._get_shapes(obj=obj)]
        for _ in [_ for shp in shapes for _ in self.get_parents(obj=shp)]:
            bbox = mc.xform(_, query=True, boundingBox=True, worldSpace=True)
            ret[0:3] = [min(ret[i], bbox[i]) for i in range(0, 3)]
            ret[3:6] = [max(ret[i], bbox[i]) for i in range(3, 6)]
        return [0.0] * 6 if ret[0] > ret[3] else ret

    def get_short_name(self, obj):
        ret = mc.ls(obj, long=False)
        return ret[0] if ret else ''

    def get_asset_name(self):
        ret = mc.file(sceneName=True, query=True)
        return os.path.splitext(os.path.basename(ret))[0] if ret else ''
