# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #


def _decl_sprite_parms(sprites):
    return '\n'.join([
        '''
int uIndex_{sprt:}
<
    string UIName = "[{sprt:}] index";
> = 0;
float3 uFrontWPosition_{sprt:}
<
    string UIName   = "[{sprt:}] front world pos";
> = {{0.0f, 0.0f, 1.0f}};
float3 uMapping_{sprt:}
<
    string UIName   = "[{sprt:}] scale-xy|rotate";
> = {{1.0f, 1.0f, 0.0f}};
'''.format(sprt=_)
        for _ in sprites
    ])


def _get_sprite_texels(sprites):
    return '\n'.join([
        '''
    float4 texel_{sprt:} = getSpriteTexel(
        in_,
        uIndex_{sprt:},
        uFrontWPosition_{sprt:},
        uMapping_{sprt:});
'''.format(sprt=_)
        for _ in sprites
    ])


def _impose_inside_constraints(inside_constraints):
    return '\n'.join([
        '''
    texel_{containee:}.w *= texel_{container:}.w;
'''.format(containee=containee, container=container)
        for containee, container in inside_constraints.items()
    ])


def _compose_sprite_texels(sprites):
    return '\n'.join([
        '''
    ret = mix(ret, texel_{sprt:}, texel_{sprt:}.w);
'''.format(sprt=_)
        for _ in sprites
    ])


def get_source(sprites, inside_constraints):
    return '''
float Script : STANDARDSGLOBAL <
    string UIWidget = "none";
    string ScriptClass = "object";
    string ScriptOrder = "standard";
    string ScriptOutput = "color";
    string Script = "Technique=Main;";
> = 0.8;

float4x4 uWorldMatrix : World <
    string UIWidget="None";
>;

float4x4 uNormalWorldMatrix : WorldInverseTranspose <
    string UIWidget="None";
>;

float4x4 uNormalMatrix : WorldViewInverseTranspose <
    string UIWidget="None";
>;

float4x4 uWorldViewProjMatrix : WorldViewProjection <
    string UIWidget="None";
>;

texture uBodyTexture
<
    string UIName =  "body texture";
    string ResourceType = "2D";
>;

sampler2D body_texture = sampler_state
{{
    Texture = <uBodyTexture>;
    MinFilter = LinearMipMapLinear;
    MagFilter = Linear;
    WrapS = Repeat;
    WrapT = Repeat;
}};

float3 uCenterWPosition
<
    string UIName   = "cube center world pos";
    string UIWidget = "Color";
> = {{0.0f, 0.0f, 0.0f}};

float3 uUpWPosition
<
    string UIName   = "cube up world pos";
    string UIWidget = "Color";
> = {{0.0f, 1.0f, 0.0f}};

float3 uFrontWPosition
<
    string UIName   = "cube front world pos";
    string UIWidget = "Color";
> = {{0.0f, 0.0f, 1.0f}};

texture uSpriteSheet
<
    string UIName =  "sprite sheet";
    string ResourceType = "2D";
>;

sampler2D sprite_sheet = sampler_state
{{
    Texture = <uSpriteSheet>;
    MinFilter = LinearMipMapLinear;
    MagFilter = Linear;
    WrapS = Repeat;
    WrapT = Repeat;
}};

int uNumSpriteCols
<
    string UIName = "#sprite columns in sheet";
    int UIMin = 1;
    int UIMax = 32;
> = 4;

int uNumSpriteRows
<
    string UIName = "#sprite rows in sheet";
    int UIMin = 1;
    int UIMax = 32;
> = 4;


//-------------------------------------------
{decl:}
//-------------------------------------------

/* data passed to vertex shader */
struct appdata
{{
    float3 Position : POSITION;
    float4 UV       : TEXCOORD0;
    float4 Normal   : NORMAL0;
}};

/* data passed from vertex shader to pixel shader */
struct vertexOutput
{{
    float4 CPosition : POSITION;  // position in clipspace
    float3 WPosition : TEXCOORD0; // position in worldspace
    float3 WNormal   : TEXCOORD1; // normal in worldspace
    float3 ENormal   : TEXCOORD2; // normal in eyespace
    float2 UV        : TEXCOORD3;

    // face frame
    float3 FaceSide  : TEXCOORD4;
    float3 FaceUp    : TEXCOORD5;
    float3 FaceFront : TEXCOORD6;

    // rcp(# sprite rows, # sprite columns)
    float2 RNumSprites : TEXCOORD7;
}};

//#####################################################################

float3x3
getFaceFrame()
{{
    float3x3 mtx;

    mtx[2] = normalize(uFrontWPosition - uCenterWPosition);   // new z axis
    mtx[1] = normalize(uUpWPosition - uCenterWPosition);  // new y axis
    mtx[0] = cross(mtx[1], mtx[2]);
    mtx[1] = cross(mtx[2], mtx[0]);

    return mtx;
}}

vertexOutput
stickerface_cube_vertexprogram(appdata in_)
{{
    vertexOutput out_ = (vertexOutput)0;

    out_.CPosition = mul(uWorldViewProjMatrix, float4(in_.Position, 1));
    out_.WPosition = mul(uWorldMatrix, float4(in_.Position, 1)).xyz;
    out_.WNormal   = mul(uNormalWorldMatrix, in_.Normal).xyz;
    // normal in eye space
    out_.ENormal   = mul(uNormalMatrix, normalize(in_.Normal)).xyz;
    out_.UV        = in_.UV.xy;

    float3x3 faceMtx = getFaceFrame();
    out_.FaceSide  = faceMtx[0];
    out_.FaceUp    = faceMtx[1];
    out_.FaceFront = faceMtx[2];

    out_.RNumSprites = float2(1.0) / float2(uNumSpriteCols, uNumSpriteRows);

    return out_;
}}

//#####################################################################

float3x3
getReferenceFrame(
    vertexOutput in_,
    float3 sprtFrontPos)
{{
    float3x3 faceMtx = float3x3(in_.FaceSide, in_.FaceUp, in_.FaceFront);
    float3 coords = mul(faceMtx, sprtFrontPos - uCenterWPosition);
    // in cube space
    float3 absCoords = abs(coords);

    int axis = 0;

    if (absCoords.x < absCoords.y)
        axis = absCoords.y < absCoords.z ? 2 : 1;
    else
        axis = absCoords.x < absCoords.z ? 2 : 0;

    float3x3 mtx;
    if (axis == 0)
    {{
        float s = sign(coords.x);
        mtx[2] = float3(s, 0.0, 0.0);
        mtx[0] = float3(0.0, 0.0, -s);
    }}
    else if (axis == 1)
    {{
        float s = sign(coords.y);
        mtx[2] = float3(0.0, s, 0.0);
        mtx[0] = float3(0.0, 0.0, s);
    }}
    else
    {{
        float s = sign(coords.z);
        mtx[2] = float3(0.0, 0.0, s);
        mtx[0] = float3(s, 0.0, 0.0);
    }}
    mtx[1] = cross(mtx[2], mtx[0]);
    // account for cube transform as dictated by positions
    // (uCenterWPosition, uUpWPosition, uFrontWPosition)
    mtx = mul(faceMtx, mtx);

    return mtx;
}}

float3x3
getSpriteFrame(
    vertexOutput in_,
    float3 sprtFrontPos,
    float sprtRotateD)
{{
    float3x3 mtx = getReferenceFrame(in_, sprtFrontPos);

    // rotate x/y axes
    float cAng = 1.0f;
    float sAng = 0.0f;
    sincos(radians(sprtRotateD), sAng, cAng);

    float3 rotatedX = cAng * mtx[0] + sAng * mtx[1];  // rotated side
    float3 rotatedY = - sAng * mtx[0] + cAng * mtx[1];  // rotated up
    mtx[0] = rotatedX;
    mtx[1] = rotatedY;

    return mtx;
}}

float4
getSpriteTexel(
    vertexOutput in_,
    int sprtIndex,
    float3 sprtFrontWPos,
    float3 sprtMapping)
{{
    float3x3 mtx = getSpriteFrame(in_, sprtFrontWPos, sprtMapping.z);
    float2 proj = mul(mtx, in_.WPosition - sprtFrontWPos).xy;

    // account for sprite distortion
    float K = 0.2;
    proj /= sprtMapping.xy * K;

    // map proj to texture coordinates
    proj *= 0.5;
    proj += float2(0.5);    // -> UV at this point

    float frontFacing = dot(normalize(in_.WNormal), mtx[2]);

    bool mask = frontFacing > 0.8 &&
        all(float2(0.0) < proj && proj < float2(1.0));

    // map sprite's local texture coordinates to the complete spritesheet
    int2 ij = int2(sprtIndex % uNumSpriteCols, sprtIndex / uNumSpriteCols);
    proj = (proj + ij) * in_.RNumSprites;   // -> sprite UV at this point

    // fetch texel
    float4 texel = tex2D(sprite_sheet, proj);
    texel.w *= float(mask);

    return texel;
}}

float4
stickerface_cube_fragmentprogram (vertexOutput in_) : COLOR
{{
    float3x3 faceMtx = float3x3(in_.FaceSide, in_.FaceUp, in_.FaceFront);

    // get sprites' texels
    {texl:}
    // impose inside constraints between sprites
    {cstr:}

    float4 ret = tex2D(body_texture, in_.UV); // already in premultiplied alpha
    {comp:}
    return ret * dot(float3(0.0, 0.0, 1.0), normalize(in_.ENormal));
}}

technique Main <
    string Script = "Pass=p0;";
> {{
    pass p0 <
    string Script = "Draw=geometry;";
    > {{
        VertexProgram = compile vp40 stickerface_cube_vertexprogram();
        DepthTestEnable = true;
        DepthMask = true;
        CullFaceEnable = false;
        BlendEnable = false;
        DepthFunc = LEqual;
        FragmentProgram = compile fp40 stickerface_cube_fragmentprogram();
    }}
}}
'''.format(
        decl=_decl_sprite_parms(sprites=sprites),
        texl=_get_sprite_texels(sprites=sprites),
        cstr=_impose_inside_constraints(inside_constraints=inside_constraints),
        comp=_compose_sprite_texels(sprites=sprites))
