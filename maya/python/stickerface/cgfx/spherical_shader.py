# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #


def _decl_sprite_parms(sprites):
    return '\n'.join([
        '''
int uIndex_{sprt:}
<
    string UIName = "[{sprt:}] index";
> = 0;
float3 uFrontPosition_{sprt:}
<
    string UIName   = "[{sprt:}] front position";
> = {{0.0f, 0.0f, 1.0f}};
float3 uMapping_{sprt:}
<
    string UIName   = "[{sprt:}] scale-xy|rotate";
> = {{1.0f, 1.0f, 0.0f}};
'''.format(sprt=_)
        for _ in sprites
    ])


def _get_sprite_texels(sprites):
    return '\n'.join([
        '''
    float4 texel_{sprt:} = getSpriteTexel(
        in_,
        uIndex_{sprt:},
        uFrontPosition_{sprt:},
        dp,
        up,
        uMapping_{sprt:});
'''.format(sprt=_)
        for _ in sprites
    ])


def _impose_inside_constraints(inside_constraints):
    return '\n'.join([
        '''
    texel_{containee:}.w *= texel_{container:}.w;
'''.format(containee=containee, container=container)
        for containee, container in inside_constraints.items()
    ])


def _compose_sprite_texels(sprites):
    return '\n'.join([
        '''
    ret = mix(ret, texel_{sprt:}, texel_{sprt:}.w);
'''.format(sprt=_)
        for _ in sprites
    ])


def get_source(sprites, inside_constraints):
    return '''
float Script : STANDARDSGLOBAL <
    string UIWidget     = "none";
    string ScriptClass  = "object";
    string ScriptOrder  = "standard";
    string ScriptOutput = "color";
    string Script       = "Technique=Main;";
> = 0.8;

float4x4 uNormalMatrix : WorldViewInverseTranspose <
    string UIWidget="None";
>;

float4x4 uWorldViewProjMatrix : WorldViewProjection <
    string UIWidget="None";
>;

texture uBodyTexture
<
    string UIName =  "body texture";
    string ResourceType = "2D";
>;

sampler2D body_texture = sampler_state
{{
    Texture = <uBodyTexture>;
    MinFilter = LinearMipMapLinear;
    MagFilter = Linear;
    WrapS = Repeat;
    WrapT = Repeat;
}};

float3 uCenter
<
    string UIName   = "center";
    string UIWidget = "Color";
> = {{0.0f, 0.0f, 0.0f}};

float3 uUpPosition
<
    string UIName   = "up position";
    string UIWidget = "Color";
> = {{0.0f, 1.0f, 0.0f}};

texture uSpriteSheet
<
    string UIName =  "sprite sheet";
    string ResourceType = "2D";
>;
sampler2D sprite_sheet = sampler_state
{{
    Texture = <uSpriteSheet>;
    MinFilter = LinearMipMapLinear;
    MagFilter = Linear;
    WrapS = Repeat;
    WrapT = Repeat;
}};

int uNumSpriteCols
<
    string UIName = "#sprite columns in sheet";
    int UIMin = 1;
    int UIMax = 32;
> = 4;

int uNumSpriteRows
<
    string UIName = "#sprite rows in sheet";
    int UIMin = 1;
    int UIMax = 32;
> = 4;

//-------------------------------------------
{decl:}
//-------------------------------------------

/* data passed to vertex shader */
struct appdata
{{
    float3 Position : POSITION;
    float4 UV       : TEXCOORD0;
    float4 Normal   : NORMAL0;
}};

/* data passed from vertex shader to pixel shader */
struct vertexOutput
{{
    float4 CPosition   : POSITION;  // position in clipspace
    float3 ENormal     : TEXCOORD0; // normal in eyespace
    float3 Position    : TEXCOORD1; // position in objectspace
    float2 UV          : TEXCOORD2;

    // rcp(# sprite rows, # sprite columns)
    float2 RNumSprites : TEXCOORD3;
}};

//#####################################################################

vertexOutput
stickerface_sphere_vertexprogram(appdata in_)
{{
    vertexOutput out_ = (vertexOutput)0;
    out_.Position     = in_.Position;
    // normal in eye space
    out_.ENormal      = mul(uNormalMatrix, normalize(in_.Normal)).xyz;
    out_.CPosition    = mul(uWorldViewProjMatrix, float4(in_.Position, 1));
    out_.UV           = in_.UV.xy;

    out_.RNumSprites = float2(1.0) / float2(uNumSpriteCols, uNumSpriteRows);

    return out_;
}}

float3x3
getSpriteFrame(
    float3 up,
    float3 sprtFrontPosition,
    float sprtRotateD
)
{{
    float3 sprtFront = normalize (sprtFrontPosition - uCenter);
    float3 sprtSide = cross(up, sprtFront);
    float3 sprtUp = cross(sprtFront, sprtSide);

    float sprtCRotate = 1.0f;
    float sprtSRotate = 0.0f;
    sincos(radians(sprtRotateD), sprtSRotate, sprtCRotate);

    float3x3 sprtFrame;
    sprtFrame[0] = sprtCRotate * sprtSide + sprtSRotate * sprtUp;  // rotd side
    sprtFrame[1] = - sprtSRotate * sprtSide + sprtCRotate * sprtUp;  // rotd up
    sprtFrame[2] = sprtFront;

    return sprtFrame;
}}

float4
getSpriteTexel(
    vertexOutput in_,
    int index,
    float3 frontPos,
    float3 dp,
    float3 up,
    float3 mapping
)
{{
    /* determine sprite (row, column) via its index */
    int2 ij = int2(index % uNumSpriteCols, index / uNumSpriteCols);
    /* get dp projection in sprite's local frame_sprt##INDEX */
    float3x3 frame = getSpriteFrame(up, frontPos, mapping.z);
    float3 dpCoords = float3(
        dot(dp, frame[0]),
        dot(dp, frame[1]),
        dot(dp, frame[2]));
    /* distort projection coords to account for stretching ratios */
    dpCoords.xy /= mapping.xy;

    float K = rsqrt(2.0f) / sin(radians(16.0)); // 16-deg aperture
    float2 UV = float2(0.5f) + K * dpCoords.xy;

    bool mask = (dot(dp, frame[2]) > 0.0f) &&
        all(float2(0.0f) < UV &&
        UV < float2(1.0f));
    UV = (UV + ij) * in_.RNumSprites;

    float4 texel = tex2D(sprite_sheet, UV);  // already in premultiplied alpha
    texel.w *= float(mask);
    return texel;
}}

float4
stickerface_sphere_fragmentprogram (vertexOutput in_) : COLOR
{{
    float3 up = normalize(uUpPosition - uCenter);
    float3 dp = normalize(in_.Position - uCenter);

    // get sprites' texels
    {texl:}
    // impose inside constraints between sprites
    {cstr:}

    float4 ret = tex2D(body_texture, in_.UV); // already in premultiplied alpha
    {comp:}
    return ret * dot(float3(0.0, 0.0, 1.0), normalize(in_.ENormal));
}}

technique Main <
    string Script = "Pass=p0;";
> {{
    pass p0 <
    string Script = "Draw=geometry;";
    > {{
        VertexProgram = compile vp40 stickerface_sphere_vertexprogram();
        DepthTestEnable = true;
        DepthMask = true;
        CullFaceEnable = false;
        BlendEnable = false;
        DepthFunc = LEqual;
        FragmentProgram = compile fp40 stickerface_sphere_fragmentprogram();
    }}
}}
'''.format(
        decl=_decl_sprite_parms(sprites=sprites),
        texl=_get_sprite_texels(sprites=sprites),
        cstr=_impose_inside_constraints(inside_constraints=inside_constraints),
        comp=_compose_sprite_texels(sprites=sprites))
