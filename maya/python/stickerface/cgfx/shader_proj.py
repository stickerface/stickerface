# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #


#  ____            _           _   _               _
# |  _ \ _ __ ___ (_) ___  ___| |_(_) ___  _ __   | |_ _   _ _ __   ___
# | |_) | '__/ _ \| |/ _ \/ __| __| |/ _ \| '_ \  | __| | | | '_ \ / _ \
# |  __/| | | (_) | |  __/ (__| |_| | (_) | | | | | |_| |_| | |_) |  __/
# |_|   |_|  \___// |\___|\___|\__|_|\___/|_| |_|  \__|\__, | .__/ \___|
#               |__/                                   |___/|_|

def _enum(**kwargs):
    return type('ShaderProj', (), kwargs)


ShaderProj = _enum(
    UNSET=0,
    SPHERICAL=1,
    CUBIC=2,
    PLANAR=3)


ICON_FILE = {
    ShaderProj.UNSET: 'question.png',
    ShaderProj.SPHERICAL: 'sphere.png',
    ShaderProj.CUBIC: 'cube.png',
    ShaderProj.PLANAR: 'plane.png',
}


DESCR = {
    ShaderProj.UNSET: 'Unknown projection',
    ShaderProj.SPHERICAL: 'Spherical projection',
    ShaderProj.CUBIC: 'Cubic projection',
    ShaderProj.PLANAR: 'Planar projection',
}
