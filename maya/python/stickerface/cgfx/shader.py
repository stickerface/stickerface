# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import os

from stickerface.cgfx import cubic_shader
from stickerface.cgfx import planar_shader
from stickerface.cgfx import shader_proj
from stickerface.cgfx import spherical_shader
from stickerface.utils import error

reload(shader_proj)
reload(spherical_shader)
reload(cubic_shader)
reload(planar_shader)
reload(error)

LOG = logging.getLogger(__name__)


def get_sprites(proj):
    SPRITES = {
        shader_proj.ShaderProj.SPHERICAL: [
            'extraBack',
            'eyeballL',
            'eyeballR',
            'pupilL',
            'pupilR',
            'lowerEyelidL',
            'lowerEyelidR',
            'upperEyelidL',
            'upperEyelidR',
            'mouth',
            'browL',
            'browR',
            'extraFront',
            'extraTop',
        ],
        shader_proj.ShaderProj.CUBIC: [
            'extraBack',
            'eyeballL',
            'eyeballR',
            'pupilL',
            'pupilR',
            'lowerEyelidL',
            'lowerEyelidR',
            'upperEyelidL',
            'upperEyelidR',
            'mouth',
            'browL',
            'browR',
            'extraFront',
            'extraTop',
        ],
        shader_proj.ShaderProj.PLANAR: [
            'extraBack',
            'eyeballL',
            'eyeballR',
            'pupilL',
            'pupilR',
            'lowerEyelidL',
            'lowerEyelidR',
            'upperEyelidL',
            'upperEyelidR',
            'mouth',
            'browL',
            'browR',
            'extraFront',
            'extraTop',
        ],
    }

    return SPRITES[proj] if proj in SPRITES else []


def get_inside_constraints(proj):
    INSIDE_CONSTRAINTS = {
        shader_proj.ShaderProj.SPHERICAL: {
            'pupilL': 'eyeballL',
            'pupilR': 'eyeballR',
            'upperEyelidL': 'eyeballL',
            'upperEyelidR': 'eyeballR',
            'lowerEyelidL': 'eyeballL',
            'lowerEyelidR': 'eyeballR',
        },
        shader_proj.ShaderProj.CUBIC: {
            'pupilL': 'eyeballL',
            'pupilR': 'eyeballR',
            'upperEyelidL': 'eyeballL',
            'upperEyelidR': 'eyeballR',
            'lowerEyelidL': 'eyeballL',
            'lowerEyelidR': 'eyeballR',
        },
        shader_proj.ShaderProj.PLANAR: {
            'pupilL': 'eyeballL',
            'pupilR': 'eyeballR',
            'upperEyelidL': 'eyeballL',
            'upperEyelidR': 'eyeballR',
            'lowerEyelidL': 'eyeballL',
            'lowerEyelidR': 'eyeballR',
        }
    }

    return INSIDE_CONSTRAINTS[proj] if proj in INSIDE_CONSTRAINTS else {}


def _get_source(proj):
    GET_SRC = {
        shader_proj.ShaderProj.SPHERICAL: spherical_shader.get_source,
        shader_proj.ShaderProj.CUBIC: cubic_shader.get_source,
        shader_proj.ShaderProj.PLANAR: planar_shader.get_source,
    }

    sprites = get_sprites(proj=proj)
    inside_constraints = get_inside_constraints(proj=proj)

    for containee, container in inside_constraints.items():
        for _ in [containee, container]:
            if _ not in sprites:
                raise error.Error(
                    '{:s} is not a valid sprite name.'.format(
                        _))

    return \
        GET_SRC[proj](
            sprites=sprites,
            inside_constraints=inside_constraints) \
        if proj in GET_SRC \
        else ''


def save(proj, path):
    base, ext = os.path.splitext(path)
    if ext.lower() != '.cgfx':
        path = base + '.cgfx'
    try:
        with open(path, 'w') as f:
            f.write(_get_source(proj=proj))
            LOG.info('Shader (type = {}) saved in "{}"'.format(proj, path))
    except IOError:
        LOG.error('Failed to save shader to "{}"'.format(path))
