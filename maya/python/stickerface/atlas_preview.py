# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging

from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import gui_util  # noqa I100
from stickerface import main_widget_base
from stickerface.cgfx import shader
from stickerface.utils import error
from stickerface.utils import qmisc

reload(error)
reload(qmisc)
reload(shader)
reload(gui_util)
reload(main_widget_base)

LOG = logging.getLogger(__name__)


#     _   _   _              ____      _     _
#    / \ | |_| | __ _ ___   / ___|_ __(_) __| |
#   / _ \| __| |/ _` / __| | |  _| '__| |/ _` |
#  / ___ \ |_| | (_| \__ \ | |_| | |  | | (_| |
# /_/   \_\__|_|\__,_|___/  \____|_|  |_|\__,_|

class _AtlasWidget(QtWidgets.QWidget):
    MAX_SIZE = 512
    ARROW = 6  # arrow size in pixels
    PEN_WIDTH = 1
    SQUARE_MARGIN = 2

    MODE__IDLE = 0
    MODE__SHOW_RANGE = 1
    MODE__SET_END = 2

    #  ____  _                   _
    # / ___|(_) __ _ _ __   __ _| |___
    # \___ \| |/ _` | '_ \ / _` | / __|
    #  ___) | | (_| | | | | (_| | \__ \
    # |____/|_|\__, |_| |_|\__,_|_|___/
    #          |___/

    class _Signals(QtCore.QObject):
        def __init__(self):
            self._indices_edited = QtCore.Signal(str, int, int, int)

    _SIGNALS = _Signals()
    indices_edited = _SIGNALS._indices_edited

    _STAR_PXM = QtGui.QPixmap(gui_util.path('outlined_star.png'))

    def __init__(
        self, 
        path, 
        num_cols=10, 
        num_rows=10, 
        parent=None, 
        f=QtCore.Qt.WindowFlags()
    ):
        super(_AtlasWidget, self).__init__(parent=parent, f=f)

        self._num_sprites = [num_cols, num_rows]
        self._pxm_size = QtCore.QSize(0, 0)
        self._pxm = QtGui.QPixmap(path)

        if not self._pxm.isNull():
            W = float(self._pxm.width())
            H = float(self._pxm.height())
            ratio = W / H
            if W >= H and W > self.MAX_SIZE:
                W = self.MAX_SIZE
                H = W / ratio
            elif H >= W and H > self.MAX_SIZE:
                W = H * ratio
                H = self.MAX_SIZE
            self._pxm_size = QtCore.QSize(int(W), int(H))

        shadow_color = self.palette().color(
            QtGui.QPalette.Normal,
            QtGui.QPalette.HighlightedText)

        # add a dropped shadow below the sprites for better readability.
        # must account for possible atlas resizing during rendering.
        pix_ratio = max(
            float(self._pxm.width()) / float(max(1, self._pxm_size.width())),
            float(self._pxm.height()) / float(max(1, self._pxm_size.height())))

        self._pxm = qmisc.add_dropped_shadow(
            pixmap=self._pxm,
            color=shadow_color,
            offset=2.0 * pix_ratio)

        self._mode = self.MODE__IDLE
        self._key = ''
        self._start = 0
        self._end = -1
        self._default = -1

        self.setToolTip(path)
        self.setMouseTracking(True)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)

        if self._pxm.isNull():
            raise error.Error(
                'Invalid sprite atlas path specified: "{:s}".'.format(path))

    @property
    def num_sprite_columns(self):
        return self._num_sprites[0]

    @property
    def num_sprite_rows(self):
        return self._num_sprites[1]

    @property
    def num_sprites(self):
        return self.num_sprite_columns * self.num_sprite_rows

    @num_sprite_columns.setter
    def num_sprite_columns(self, value):
        self._num_sprites[0] = value

    @num_sprite_rows.setter
    def num_sprite_rows(self, value):
        self._num_sprites[1] = value

    def minimumSizeHint(self):
        return QtCore.QSize(
            self._pxm_size.width() + self.ARROW + self.PEN_WIDTH * 2,
            self._pxm_size.height() + self.ARROW + self.PEN_WIDTH * 2)

    def _emit_indices_edited(self):
        if self._key:
            start, end, default = self._get_sprite_indices()
            self.indices_edited.emit(
                self._key,
                start,
                end if end < self.num_sprites - 1 else -1,
                default)

    def set_sprite_indices(self, key='', start=0, end=-1, default=-1):
        if key != self._key:
            self._emit_indices_edited()
        self._key = key
        self._start = start if key else 0
        self._end = end if key else -1
        self._default = default if key else -1
        self._mode = self.MODE__SHOW_RANGE if key else self.MODE__IDLE
        self.update()

    def _get_sprite_indices(self):
        def _clamp(n, lower=0, upper=self.num_sprites - 1):
            return max(lower, min(upper, n))
        start = _clamp(n=self._start)
        end = _clamp(n=self.num_sprites - 1 if self._end < 0 else self._end)
        if start > end:
            start, end = end, start
        default = _clamp(n=self._default, lower=start, upper=end)
        return (start, end, default)

    def _get_actual_image_size(self):
        w, h = self._get_square_size()
        return \
            (
                w * self.num_sprite_columns,
                h * self.num_sprite_rows
            )

    def _get_square_size(self):
        return \
            (
                self._pxm_size.width() / self.num_sprite_columns,
                self._pxm_size.height() / self.num_sprite_rows
            ) \
            if self.num_sprites > 0 \
            else (0, 0)

    def _get_square_coords(self, idx):
        return \
            (idx % self.num_sprite_columns, idx / self.num_sprite_columns) \
            if self.num_sprite_columns > 0 \
            else (0, 0)

    def _get_square_rect(self, idx):
        i, j = self._get_square_coords(idx=idx)
        w, h = self._get_square_size()
        if self.num_sprites == 0:
            return QtCore.QRect()
        x = w * i + self.ARROW
        y = h * (self.num_sprite_rows - 1 - j)
        return QtCore.QRect(x, y, w, h)

    def _get_square_index(self, x, y):
        W, H = self._get_actual_image_size()
        if W == 0 or H == 0:
            return -1

        left = self.ARROW
        top = 0
        w = W / self.num_sprite_columns
        h = H / self.num_sprite_rows
        i = (x - left) / w
        y = H - y
        j = (y - top) / h

        if any([
            i < 0, i >= self.num_sprite_columns,
            j < 0, j >= self.num_sprite_rows,
        ]):
            return -1
        return i + self.num_sprite_columns * j

    def mouseMoveEvent(self, e):
        if self._mode == self.MODE__SET_END:
            self._end = self._get_square_index(x=e.x(), y=e.y())
            self._emit_indices_edited()
        self.update()

    def mouseReleaseEvent(self, e):
        if self._mode == self.MODE__SHOW_RANGE:
            if e.button() == QtCore.Qt.RightButton:
                self._default = self._get_square_index(x=e.x(), y=e.y())
                self._emit_indices_edited()
            else:
                self._start = self._get_square_index(x=e.x(), y=e.y())
                self._end = self._start
                self._mode = self.MODE__SET_END
        elif self._mode == self.MODE__SET_END:
            self._mode = self.MODE__SHOW_RANGE
        self.update()

    def paintEvent(self, e):
        if self._pxm.isNull():
            return

        p = QtGui.QPainter(self)
        plt = self.palette()

        w, h = self._get_square_size()
        W, H = self._get_actual_image_size()

        left = self.ARROW
        top = 0
        right = left + W
        btm = top + H

        # draw the sprite atlas image
        p.setRenderHints(QtGui.QPainter.SmoothPixmapTransform)
        p.drawPixmap(left, top, W, H, self._pxm)

        # draw dashed lines between squares
        pen = QtGui.QPen(plt.color(
            QtGui.QPalette.WindowText
            if self._mode == self.MODE__IDLE
            else QtGui.QPalette.HighlightedText))
        pen.setWidth(1)
        pen.setStyle(QtCore.Qt.DashLine)

        p.setPen(pen)
        p.drawLines([
            QtCore.QLine(left + n * w, top, left + n * w, top + H)
            for n in range(1, self.num_sprite_columns)
        ] + [
            QtCore.QLine(left, top + n * h, left + W, top + n * h)
            for n in range(1, self.num_sprite_rows)
        ])

        if self._mode != self.MODE__IDLE:
            start, end, default = self._get_sprite_indices()

            # draw the star for the default sprite
            M = self.SQUARE_MARGIN
            pos = self._get_square_rect(idx=default).topLeft()
            p.drawPixmap(
                pos.x() + M,
                pos.y() + M,
                self._STAR_PXM)

            # highlight selected squares' outlines
            rects = [
                self._get_square_rect(idx=idx)
                for idx in range(start, end + 1)
            ]
            color = plt.color(QtGui.QPalette.Highlight)
            p.setBrush(QtCore.Qt.NoBrush)
            p.setPen(color)
            p.drawRects(rects)

            # draw obscuring squares
            rects = [
                self._get_square_rect(idx=idx)
                for idx in range(0, start) + range(end + 1, self.num_sprites)
            ]
            rects = [_.adjusted(M, M, -M, -M).normalized() for _ in rects]

            color = plt.color(QtGui.QPalette.Window)
            color.setAlpha(200 if self._mode == self.MODE__SHOW_RANGE else 175)
            p.setBrush(color)
            p.setPen(QtCore.Qt.NoPen)
            p.drawRects(rects)

        # highlight rectangle corresponding to range starting index
        if self._mode == self.MODE__SET_END:
            p.setBrush(QtCore.Qt.NoBrush)
            pen = QtGui.QPen()
            for k, color in enumerate([
                QtGui.QColor(255, 255, 255),
                plt.color(QtGui.QPalette.Highlight),
            ]):
                pen.setWidth(self.PEN_WIDTH * (4 - 2 * k))
                pen.setColor(color)
                p.setPen(pen)
                p.drawRect(self._get_square_rect(idx=self._start))

        # draw atlas axes
        pen = QtGui.QPen(plt.color(QtGui.QPalette.HighlightedText))
        pen.setWidth(1)

        p.setBrush(QtCore.Qt.NoBrush)
        p.setPen(pen)
        p.drawLines([
            QtCore.QLine(left, btm, right, btm),
            QtCore.QLine(left, btm, left, top),
            QtCore.QLine(left, top, left - self.ARROW, top + self.ARROW),
            QtCore.QLine(left, top, left + self.ARROW, top + self.ARROW),
            QtCore.QLine(right, btm, right - self.ARROW, btm - self.ARROW),
            QtCore.QLine(right, btm, right - self.ARROW, btm + self.ARROW),
        ])


#  __  __       _        __        ___     _            _
# |  \/  | __ _(_)_ __   \ \      / (_) __| | __ _  ___| |_
# | |\/| |/ _` | | '_ \   \ \ /\ / /| |/ _` |/ _` |/ _ \ __|
# | |  | | (_| | | | | |   \ V  V / | | (_| | (_| |  __/ |_
# |_|  |_|\__,_|_|_| |_|    \_/\_/  |_|\__,_|\__, |\___|\__|
#                                            |___/

class _MainWidget(main_widget_base.MainWidgetBase):

    #  ____             _ _         _____       _
    # / ___| _ __  _ __(_) |_ ___  | ____|_ __ | |_ _ __ _   _
    # \___ \| '_ \| '__| | __/ _ \ |  _| | '_ \| __| '__| | | |
    #  ___) | |_) | |  | | ||  __/ | |___| | | | |_| |  | |_| |
    # |____/| .__/|_|  |_|\__\___| |_____|_| |_|\__|_|   \__, |
    #       |_|                                          |___/

    class _SpriteMapEntry(object):
        def __init__(self):
            self._start = 0
            self._end = -1
            self._default = -1
            self._start_label = QtWidgets.QLabel()
            self._end_label = QtWidgets.QLabel()
            self._default_label = QtWidgets.QLabel()

            for _ in [self._start_label, self._end_label, self._default_label]:
                qmisc.set_fixed_width(widget=_, num_chars=3)
                _.setAlignment(QtCore.Qt.AlignLeft)
            self._start_label.setAlignment(QtCore.Qt.AlignRight)

        @property
        def start(self):
            return self._start

        @property
        def end(self):
            return self._end

        @property
        def default(self):
            return self._default

        @property
        def start_label(self):
            return self._start_label

        @property
        def end_label(self):
            return self._end_label

        @property
        def default_label(self):
            return self._default_label

        @start.setter
        def start(self, value):
            self._start = max(0, value)
            self._start_label.setText('{}'.format(self._start))

        @end.setter
        def end(self, value):
            self._end = -1
            self._end_label.setText('End')
            if value >= 0:
                self._end = value
                self._end_label.setText('{}'.format(value))

        @default.setter
        def default(self, value):
            self._default = -1
            self._default_label.setText('1st')
            if self._start < value:
                self._default = value
                self._default_label.setText('{}'.format(value))

    _STAR_PXM = QtGui.QPixmap(gui_util.path('star.png'))
    _RIGHT_PXM = QtGui.QPixmap(gui_util.path('right.png'))

    def __init__(
        self,
        cmds,
        atlas_path,
        shader_proj,
        num_sprite_cols,
        num_sprite_rows,
        parent=None,
        f=QtCore.Qt.WindowFlags()
    ):
        super(_MainWidget, self).__init__(cmds=cmds, parent=parent, f=f)
        self._finalize(
            atlas_path=atlas_path,
            shader_proj=shader_proj,
            num_sprite_cols=num_sprite_cols,
            num_sprite_rows=num_sprite_rows)

    def _finalize(
        self,
        atlas_path,
        shader_proj,
        num_sprite_cols,
        num_sprite_rows
    ):
        self._atlas_view = _AtlasWidget(
            path=atlas_path,
            num_cols=num_sprite_cols,
            num_rows=num_sprite_rows)
        self._sprites = {
            _: self._SpriteMapEntry()
            for _ in shader.get_sprites(proj=shader_proj)
        }
        self._buttons = QtWidgets.QButtonGroup(self)
        self._button_keys = self._sprites.keys()
        self._button_keys.sort()

        main_layout = QtWidgets.QVBoxLayout()
        body_layout = QtWidgets.QHBoxLayout()
        atlas_layout = QtWidgets.QGridLayout()
        sprites_layout = QtWidgets.QGridLayout()
        footnote_layout = QtWidgets.QHBoxLayout()
        footnote_tools = QtWidgets.QToolBar()
        ok_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('exec_2.png')),
            'Execute',
            self)

        edits = [QtWidgets.QLineEdit() for i in range(2)]
        for _ in edits:
            qmisc.set_line_edit_fixed_width(edit=_, num_chars=3)
            _.setAlignment(QtCore.Qt.AlignRight)
            _.setValidator(QtGui.QIntValidator())
        edits[0].setToolTip('Number of sprites per row')
        edits[1].setToolTip('Number of sprite rows')

        edits[0].textChanged .connect(
            self._num_sprite_cols_text_changed)
        edits[1].textChanged .connect(
            self._num_sprite_rows_text_changed)

        edits[0].setText('{:d}'.format(self._atlas_view.num_sprite_columns))
        edits[1].setText('{:d}'.format(self._atlas_view.num_sprite_rows))

        align = QtCore.Qt.AlignTop | QtCore.Qt.AlignRight
        atlas_layout.addWidget(edits[1], 0, 0, align)
        atlas_layout.addWidget(self._atlas_view, 0, 1)
        atlas_layout.addWidget(edits[0], 1, 1, align)

        def _scale_pixmap(pxm):
            fm = QtGui.QFontMetrics(self.font())
            size = fm.height() * 0.8
            return pxm.scaled(
                size,
                size,
                QtCore.Qt.KeepAspectRatio,
                QtCore.Qt.SmoothTransformation)

        right_pxm = _scale_pixmap(pxm=self._RIGHT_PXM)
        star_pxm = _scale_pixmap(pxm=self._STAR_PXM)

        for row, key in enumerate(self._button_keys):
            button = QtWidgets.QPushButton(key)
            arrow = QtWidgets.QLabel()
            star = QtWidgets.QLabel()
            entry = self._sprites[key]

            button.setCheckable(True)
            button.setStyleSheet('''
                QPushButton
                {
                    padding: 2px;
                }
                QPushButton:checked
                {
                    background-color: palette(highlight);
                    color: white;
                }''')
            arrow.setPixmap(right_pxm)
            star.setPixmap(star_pxm)
            self._buttons.addButton(button, row)
            for col, wdg in enumerate([
                button,
                entry.start_label,
                arrow,
                entry.end_label,
                star,
                entry.default_label
            ]):
                sprites_layout.addWidget(wdg, row, col, QtCore.Qt.AlignVCenter)
        sprites_layout.setRowStretch(len(self._button_keys), 1)

        self._ghost_button = QtWidgets.QPushButton()
        self._ghost_button.setCheckable(True)
        self._ghost_button.hide()
        self._buttons.addButton(self._ghost_button, len(self._button_keys))

        sprites_layout.setSpacing(2)
        self._buttons.setExclusive(True)
        self._buttons.buttonPressed[int].connect(
            self._button_from_group_clicked)
        self._buttons.buttonReleased[int].connect(
            self._button_from_group_released)

        for title, layout in [
            ('Atlas Grid', atlas_layout),
            ('Sprite Ranges and Defaults', sprites_layout),
        ]:
            grp = QtWidgets.QGroupBox(title)
            grp.setLayout(layout)
            body_layout.addWidget(grp)
        body_layout.addStretch(1)

        footnote_tools.setIconSize(self.ICON_SIZE)
        footnote_tools.addAction(ok_act)

        notes_layout = QtWidgets.QVBoxLayout()
        notes_layout.setSpacing(0)
        for _ in [
            '- Adjust the sprite grid resolution',
            '- Select a sprite, then click on the grid to set its valid range',
            '- Select a sprite, then right-click on the grid to set its '
            'default value'
        ]:
            notes_layout.addWidget(QtWidgets.QLabel(_))

        footnote_layout.addLayout(notes_layout)
        footnote_layout.addStretch(1)
        footnote_layout.addWidget(footnote_tools)

        main_layout.addLayout(body_layout)
        main_layout.addLayout(footnote_layout)
        main_layout.addStretch(1)
        self.setLayout(main_layout)

        ok_act.triggered.connect(self._emit_finished)
        self._atlas_view.indices_edited.connect(self._sprite_indices_edited)

    @property
    def num_sprite_columns(self):
        return self._atlas_view.num_sprite_columns

    @property
    def num_sprite_rows(self):
        return self._atlas_view.num_sprite_rows

    @property
    def sprite_indices(self):
        return {
            k:
            (
                # index offset
                v.start,
                # index range
                0
                if v.end == 0 or v.end < v.start
                else v.end - v.start + 1,
                # default index
                0
                if v.default < v.start or (v.end >= 0 and v.end < v.default)
                else v.default
            )
            for k, v in self._sprites.items()
        }

    def _emit_finished(self):
        self.finished.emit()

    def _num_sprite_cols_text_changed(self, txt):
        self._atlas_view.num_sprite_columns = self._to_int(txt=txt)
        self.update()

    def _num_sprite_rows_text_changed(self, txt):
        self._atlas_view.num_sprite_rows = self._to_int(txt=txt)
        self.update()

    def _button_from_group_clicked(self, button_id):
        # trick used to uncheck visible push buttons
        if button_id == self._buttons.checkedId():
            self._buttons.setExclusive(False)
            self._ghost_button.setChecked(True)
            self._buttons.setExclusive(True)

    def _button_from_group_released(self, button_id):
        try:
            key = self._button_keys[self._buttons.checkedId()]
        except IndexError:
            key = ''
        start = self._sprites[key].start if key in self._sprites else 0
        end = self._sprites[key].end if key in self._sprites else -1
        default = self._sprites[key].default if key in self._sprites else -1
        self._atlas_view.set_sprite_indices(
            key=key,
            start=start,
            end=end,
            default=default)

    def _sprite_indices_edited(
        self,
        sprite_key,
        start_idx,
        end_idx,
        default_idx
    ):
        if sprite_key in self._sprites:
            self._sprites[sprite_key].start = start_idx
            self._sprites[sprite_key].end = end_idx
            self._sprites[sprite_key].default = default_idx

    @staticmethod
    def _to_int(txt):
        try:
            return max(1, int(txt))
        except ValueError:
            return 1


#  ____  _       _
# |  _ \(_) __ _| | ___   __ _
# | | | | |/ _` | |/ _ \ / _` |
# | |_| | | (_| | | (_) | (_| |
# |____/|_|\__,_|_|\___/ \__, |
#                        |___/

class AtlasPreviewDialog(QtWidgets.QDialog):

    def __init__(
        self,
        cmds,
        atlas_path,
        shader_proj,
        num_sprite_cols=10,
        num_sprite_rows=10,
        parent=None,
        f=QtCore.Qt.WindowFlags()
    ):
        super(AtlasPreviewDialog, self).__init__(parent=parent, f=f)
        self._main_widget = _MainWidget(
            cmds=cmds,
            atlas_path=atlas_path,
            shader_proj=shader_proj,
            num_sprite_cols=num_sprite_cols,
            num_sprite_rows=num_sprite_rows)

        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setContentsMargins(0, 0, 0, 0)
        main_layout.addWidget(self._main_widget)
        self.setLayout(main_layout)
        self.setWindowTitle('Sprite Atlas Preview')

        self._main_widget.finished.connect(self.accept)

    @property
    def num_sprite_columns(self):
        return self._main_widget.num_sprite_columns

    @property
    def num_sprite_rows(self):
        return self._main_widget.num_sprite_rows

    @property
    def sprite_indices(self):
        return self._main_widget.sprite_indices
