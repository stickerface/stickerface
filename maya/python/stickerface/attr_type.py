# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #


def _enum(*args, **kwargs):
    d = dict(zip(args, range(len(args))), **kwargs)
    return type('AttrType', (), d)


#     _   _   _        _ _           _         _____
#    / \ | |_| |_ _ __(_) |__  _   _| |_ ___  |_   _|   _ _ __   ___
#   / _ \| __| __| '__| | '_ \| | | | __/ _ \   | || | | | '_ \ / _ \
#  / ___ \ |_| |_| |  | | |_) | |_| | ||  __/   | || |_| | |_) |  __/
# /_/   \_\__|\__|_|  |_|_.__/ \__,_|\__\___|   |_| \__, | .__/ \___|
#                                                   |___/|_|

AttrType = _enum(
    'ALL',
    'INT',
    'NUMBER',
    'V3F')
