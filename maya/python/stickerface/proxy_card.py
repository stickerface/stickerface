# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import os

from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import extra_attr  # noqa I100
from stickerface import gui_util
from stickerface import main_widget_base
from stickerface.cgfx import shader_proj
from stickerface.utils import qmisc

reload(shader_proj)
reload(qmisc)
reload(gui_util)
reload(extra_attr)
reload(main_widget_base)

LOG = logging.getLogger(__name__)


#  ____                         ____              _
# |  _ \ _ __ _____  ___   _   / ___|__ _ _ __ __| |
# | |_) | '__/ _ \ \/ / | | | | |   / _` | '__/ _` |
# |  __/| | | (_) >  <| |_| | | |__| (_| | | | (_| |
# |_|   |_|  \___/_/\_\\__, |  \____\__,_|_|  \__,_|
#                      |___/

class ProxyCard(QtWidgets.QGroupBox):

    _BODY_PXM = QtGui.QPixmap(gui_util.path('body_texture.png'))
    _NO_BODY_PXM = QtGui.QPixmap(gui_util.path('no_body_texture.png'))
    _ATLAS_PXM = QtGui.QPixmap(gui_util.path('sprite_sheet.png'))
    _NO_ATLAS_PXM = QtGui.QPixmap(gui_util.path('no_sprite_sheet.png'))

    def __init__(self, prx_data, parent=None):
        super(ProxyCard, self).__init__(parent=parent)
        self._prx_data = prx_data
        self._body_icon = QtWidgets.QLabel()
        self._body_edit = QtWidgets.QLineEdit()
        self._atlas_icon = QtWidgets.QLabel()
        self._atlas_edit = QtWidgets.QLineEdit()
        self._gamma_edit = QtWidgets.QLineEdit()

        self._body_icon.setPixmap(self._NO_BODY_PXM)
        self._body_icon.setToolTip('Body texture')
        self._atlas_icon.setPixmap(self._NO_ATLAS_PXM)
        self._atlas_icon.setToolTip('Sprite atlas')

        for _ in [self._body_edit, self._atlas_edit]:
            _.setReadOnly(True)
            if parent:
                _.setMinimumWidth(parent.LINE_EDIT_WIDTH)

        self._gamma_edit.setValidator(QtGui.QDoubleValidator(0.0, 100.0, 2))
        self._gamma_edit.setFixedWidth(36)

        asset_icon = QtWidgets.QLabel()
        asset_label = QtWidgets.QLabel(prx_data.asset_name)
        shape_icon = QtWidgets.QLabel()
        shape_label = QtWidgets.QLabel(prx_data.shape)
        proxy_icon = QtWidgets.QLabel()
        proxy_label = QtWidgets.QLabel(prx_data.proxy_label)
        proj_icon = QtWidgets.QLabel()
        gamma_icon = QtWidgets.QLabel()

        body_tools = QtWidgets.QToolBar()
        atlas_tools = QtWidgets.QToolBar()

        body_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('browse_location.png')),
            'Browse location',
            self)
        atlas_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('browse_location.png')),
            'Browse location',
            self)

        body_tools.addAction(body_act)
        atlas_tools.addAction(atlas_act)
        for _ in [body_tools, atlas_tools]:
            _.setIconSize(main_widget_base.MainWidgetBase.ICON_SIZE)

        asset_icon.setPixmap(QtGui.QPixmap(gui_util.path('asset.png')))
        asset_icon.setToolTip('Asset name')

        shape_icon.setPixmap(QtGui.QPixmap(gui_util.path('face_shape.png')))
        shape_icon.setToolTip('Face mesh')
        if not prx_data.shape:
            shape_label.setPixmap(QtGui.QPixmap(gui_util.path('question.png')))

        proxy_icon.setPixmap(QtGui.QPixmap(gui_util.path('proxy.png')))
        proxy_icon.setToolTip('Backed proxy object')

        proj_icon.setPixmap(QtGui.QPixmap(gui_util.path(
            shader_proj.ICON_FILE.get(prx_data.proj_type, 'question.png'))))
        proj_icon.setToolTip(
            shader_proj.DESCR.get(prx_data.proj_type, ''))

        gamma_icon.setPixmap(QtGui.QPixmap(gui_util.path('gamma.png')))
        gamma_icon.setToolTip('Gamma correction')

        self.set_body_path(p=prx_data.body_path)
        self.set_atlas_path(p=prx_data.atlas_path)

        all_content = [
            [asset_icon, asset_label, 0, proj_icon, ],
            [shape_icon, shape_label, 0, ],
            [proxy_icon, proxy_label, 0, ],
            [self._atlas_icon, self._atlas_edit, atlas_tools, 0, ],
            [self._body_icon, self._body_edit, body_tools, 0, ],
            [gamma_icon, self._gamma_edit, 0, ],
        ]

        main_layout = QtWidgets.QVBoxLayout()
        h_layouts = [QtWidgets.QHBoxLayout() for _ in range(len(all_content))]
        for row, row_content in enumerate(all_content):
            for _ in row_content:
                if isinstance(_, int):
                    h_layouts[row].addStretch(1)
                else:
                    h_layouts[row].addWidget(_)

        for _ in h_layouts:
            main_layout.addLayout(_)
        main_layout.addStretch(1)
        self.setLayout(main_layout)
        self.setStyleSheet('''
            QGroupBox
            {
                border: 1px solid palette(text);
                border-radius: 8px;
            }''')

        self._gamma_edit.textChanged.connect(
            self._handle_gamma_edit_changed)

        self._gamma_edit.setText('{:.2f}'.format(prx_data.gamma))

        body_act.triggered.connect(self._browse_body_texture_location)
        atlas_act.triggered.connect(self._browse_sprite_atlas_location)

    @property
    def proxy_data(self):
        return self._prx_data

    def _browse_body_texture_location(self):
        self.set_body_path(
            p=qmisc.browse_location(
                action=qmisc.BrowseAct.OPEN_FILE,
                parent=self,
                cur_path=self._prx_data.body_path,
                caption='Find body texture',
                filtr='textures (*.jpg *.png *.exr *.tiff)'))

    def _browse_sprite_atlas_location(self):
        self.set_atlas_path(
            p=qmisc.browse_location(
                action=qmisc.BrowseAct.OPEN_FILE,
                parent=self,
                cur_path=self._prx_data.body_path,
                caption='Find sprite atlas',
                filtr='textures (*.jpg *.png *.exr *.tiff)'))

    def set_body_path(self, p):
        self._prx_data.body_path = qmisc.display_location(
            path=p,
            line_edit=self._body_edit)
        self._body_icon.setPixmap(
            self._BODY_PXM
            if os.path.isfile(p)
            else self._NO_BODY_PXM)

    def set_atlas_path(self, p):
        self._prx_data.atlas_path = qmisc.display_location(
            path=p,
            line_edit=self._atlas_edit)
        self._atlas_icon.setPixmap(
            self._ATLAS_PXM
            if os.path.isfile(p)
            else self._NO_ATLAS_PXM)

    def _handle_gamma_edit_changed(self, txt):
        try:
            gamma = float(txt)
        except ValueError:
            gamma = self._prx_data.gamma
        self._prx_data.gamma = gamma
