# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #


def _enum(**kwargs):
    return type('CtrlFlag', (), kwargs)


CtrlFlag = _enum(
    # side
    CENTRAL=1 << 0,
    LEFT=1 << 1,
    RIGHT=1 << 2,
    # transform categories
    ANIM=1 << 3,
    ANIM_NULL=1 << 4,
    CURV_NULL=1 << 5,
    CNX_NULL=1 << 6,
    ROOT=1 << 7)
