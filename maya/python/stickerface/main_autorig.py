# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import sys
import traceback

from stickerface import m_callbacks
from stickerface import m_widget
from stickerface import main_widget_autorig
from stickerface import top_dialog
from stickerface.utils import error_dialog

reload(error_dialog)
reload(m_widget)
reload(m_callbacks)
reload(top_dialog)
reload(main_widget_autorig)


LOG = logging.getLogger(__name__)


#     _         _             _                         _
#    / \  _   _| |_ ___  _ __(_) __ _   _ __ ___   __ _(_)_ __
#   / _ \| | | | __/ _ \| '__| |/ _` | | '_ ` _ \ / _` | | '_ \
#  / ___ \ |_| | || (_) | |  | | (_| | | | | | | | (_| | | | | |
# /_/   \_\__,_|\__\___/|_|  |_|\__, | |_| |_| |_|\__,_|_|_| |_|
#                               |___/

def main():
    dialog = None

    def _show_error_dialog(exc_type, exc_value, exc_traceback):
        message = ''.join(traceback.format_exception(
            exc_type,
            exc_value,
            exc_traceback))
        sys.stderr.write(message)
        if dialog:
            dialog.reject()
        error_dialog.ErrorDialog(message=message).exec_()

    def _excepthook(exc_type, exc_value, exc_traceback):
        LOG.info('Error handling via hook')
        _show_error_dialog(
            exc_type=exc_type,
            exc_value=exc_value,
            exc_traceback=exc_traceback)

    try:
        sys.excepthook = _excepthook
        widget = main_widget_autorig.MainWidget(
            cmds=m_callbacks.Callbacks())
        dialog = top_dialog.TopDialog(
            main_widget=widget,
            parent=m_widget.main_window())
        dialog.setWindowTitle('Sticker Face Autorig')
        dialog.show()

    except Exception:
        LOG.info('Error handling via except')
        exc_type, exc_value, exc_traceback = sys.exc_info()
        _show_error_dialog(
            exc_type=exc_type,
            exc_value=exc_value,
            exc_traceback=exc_traceback)
