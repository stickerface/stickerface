# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #

#  _____      _                  _   _   _
# | ____|_  _| |_ _ __ __ _     / \ | |_| |_ _ __ ___
# |  _| \ \/ / __| '__/ _` |   / _ \| __| __| '__/ __|
# | |___ >  <| |_| | | (_| |  / ___ \ |_| |_| |  \__ \
# |_____/_/\_\\__|_|  \__,_| /_/   \_\__|\__|_|  |___/


class ExtraAttr(object):
    CTRL_FLAGS = 'facial_ctrl_flags'
    CTRL_NAME = 'facial_ctrl_name'
    SPRT_IDX = 'facial_sprite_index'
    _SCALE_TWEAK = '_scale_tweak'  # only a suffix
    _IDX_OFFSET = '_index_offset'  # only a suffix
    _IDX_DEFAULT = '_index_default'  # only a suffix
    GLOBAL_SCALE = 'global' + _SCALE_TWEAK
    SPRT_NAME = 'facial_sprite'
    PROJ_TYPE = 'projection_type'
    ASSET_NAME = 'asset_name'
    FLIP = 'flip'
    HAS_STICKERS = 'has_stickers'  # flags the maya entity as carrying stickers

    @staticmethod
    def scale_tweak(sprt):
        return sprt + ExtraAttr._SCALE_TWEAK

    @staticmethod
    def index_offset(sprt):
        return sprt + ExtraAttr._IDX_OFFSET

    @staticmethod
    def index_default(sprt):
        return sprt + ExtraAttr._IDX_DEFAULT


_BAKING_PREFIX = 'smks_'


def decorate(name):
    '''
    Returns: decorate attribute name compatible with baking.
    '''
    return _BAKING_PREFIX + name if name else ''


def is_decorated(attr):
    return attr.startswith(_BAKING_PREFIX)
