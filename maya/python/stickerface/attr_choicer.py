# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging

from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import gui_util  # noqa I100

reload(gui_util)

LOG = logging.getLogger(__name__)


#    _   _   _        _ _           _         ____ _           _
#   / \ | |_| |_ _ __(_) |__  _   _| |_ ___  / ___| |__   ___ (_) ___ ___ _ __
#  / _ \| __| __| '__| | '_ \| | | | __/ _ \| |   | '_ \ / _ \| |/ __/ _ \ '__|
# / ___ \ |_| |_| |  | | |_) | |_| | ||  __/| |___| | | | (_) | | (_|  __/ |
# _/   \_\__|\__|_|  |_|_.__/ \__,_|\__\___| \____|_| |_|\___/|_|\___\___|_|

class AttributeChoicer(QtWidgets.QDialog):

    class ListItem(QtWidgets.QListWidgetItem):
        ICON = QtGui.QPixmap(gui_util.path('dot.png'))

        def __init__(
            self,
            attr,
            idx,
            view=None,
            type=QtWidgets.QListWidgetItem.Type
        ):
            super(AttributeChoicer.ListItem, self).__init__(view, type)
            self._attr = attr
            self._idx = idx

        def data(self, role):
            if role == QtCore.Qt.DisplayRole:
                return self._attr
            elif role == QtCore.Qt.UserRole:
                return self._idx
            elif role == QtCore.Qt.DecorationRole:
                return AttributeChoicer.ListItem.ICON

    def __init__(self, obj, attrs, parent=None, f=QtCore.Qt.WindowFlags()):
        super(AttributeChoicer, self).__init__(parent=parent, f=f)
        self._obj = obj
        self._attrs = attrs
        self._attrs.sort()
        self._idx = -1

        main_layout = QtWidgets.QVBoxLayout()
        list_widget = QtWidgets.QListWidget()

        for idx, attr in enumerate(self._attrs):
            list_widget.addItem(AttributeChoicer.ListItem(attr=attr, idx=idx))

        main_layout.addWidget(list_widget)
        self.setLayout(main_layout)

        list_widget.itemActivated.connect(
            self._handle_item_activated)

    @property
    def choice(self):
        return \
            (self._obj, self._attrs[self._idx]) \
            if all([
                bool(self._obj),
                self._idx >= 0,
                self._idx < len(self._attrs)
            ]) \
            else ()

    def _handle_item_activated(self, item):
        self._idx = item.data(QtCore.Qt.UserRole)
        LOG.info('activated item = {}'.format(self._idx))
        self.accept()
