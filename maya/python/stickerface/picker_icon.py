# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import os

from stickerface.Qt import QtCore
from stickerface.Qt import QtGui

from stickerface.utils import qmisc  # noqa I202

reload(qmisc)

LOG = logging.getLogger(__name__)
SIZE = QtCore.QSize(48, 48)


#  ___                   ____           _
# |_ _|___ ___  _ __    / ___|__ _  ___| |__   ___
#  | |/ __/ _ \| '_ \  | |   / _` |/ __| '_ \ / _ \
#  | | (_| (_) | | | | | |__| (_| | (__| | | |  __/
# |___\___\___/|_| |_|  \____\__,_|\___|_| |_|\___|

class Cache(object):

    class AtlasDeclaration(object):
        def __init__(self, path, grid):
            self.path = path
            self.grid = grid  # (num columns, num rows)
            self.full_width = 0
            self.full_height = 0
            self.sprite_width = 0.0
            self.sprite_height = 0.0

    def __init__(self, shadow_color=QtCore.Qt.white):
        self._atlas_decl = {}  # path -> declaration
        self._atlas_map = {}  # path -> pixmap
        self._icon_requests = {}  # (path, idx) -> [list_item]
        self._icon_map = {}  # (path, idx) -> icon

        self._shadow_color = shadow_color

    @staticmethod
    def _get_atlas_key(p):
        return \
            os.path.normpath(os.path.normcase(os.path.abspath(p))) \
            if os.path.isfile(p) \
            else ''

    def clear(self):
        self._atlas_decl = {}
        self._atlas_map = {}
        self._icon_requests = {}
        self._icon_map = {}

    def declare_atlas(self, path, grid_resolution):
        key = self._get_atlas_key(p=path)
        if key:
            decl = self.AtlasDeclaration(path=path, grid=grid_resolution)
            self._atlas_decl[key] = decl

    def set_item_icon(self, list_item, path, idx):
        atlas_key = self._get_atlas_key(p=path)
        if not atlas_key:
            LOG.warning('"{:s}" is not a valid file.'.format(path))
            return

        key = (atlas_key, idx)
        if key in self._icon_map:
            list_item.setIcon(self._icon_map[key])
            return

        # queue assignment request
        requests = self._icon_requests.get(key, [])
        requests.append(list_item)
        self._icon_requests[key] = requests
        LOG.info('{} requests for icon ({}, {})'.format(
            len(self._icon_requests[key]), key[0], key[1]))

    def _get_or_create_icon(self, key):
        if key in self._icon_map:
            return self._icon_map[key]
        atlas_key, idx = key
        LOG.info('Create icon for {:d}th sprite from "{:s}"'.format(
            idx,
            atlas_key))
        atlas = self._get_or_load_atlas(atlas_key=atlas_key)
        if atlas_key not in self._atlas_decl:
            return QtGui.QIcon()

        decl = self._atlas_decl[atlas_key]
        num_cols, num_rows = decl.grid
        if num_cols == 0:
            return QtGui.QIcon()
        i = idx % num_cols
        j = num_rows - 1 - idx / num_cols

        self._icon_map[key] = QtGui.QIcon(
            qmisc.add_dropped_shadow(
                pixmap=atlas.copy(
                    int(decl.sprite_width * i),  # left
                    int(decl.sprite_height * j),  # top
                    int(decl.sprite_width),
                    int(decl.sprite_height))
                .scaled(
                    SIZE.width(),
                    SIZE.height(),
                    QtCore.Qt.KeepAspectRatio,
                    QtCore.Qt.SmoothTransformation),
                color=self._shadow_color,
                offset=1.0))
        return self._icon_map[key]

    def _process_sprite(self, pxm, shadow_color):
        '''
        Add colored outline to the sprite for better readability.
        '''
        ret = QtGui.QPixmap(pxm.width(), pxm.height())
        ret.fill(QtGui.QColor(0, 0, 0, 0))
        painter = QtGui.QPainter(ret)

        pxm_shadow = QtGui.QPixmap(pxm.width(), pxm.height())
        pxm_shadow.fill(shadow_color)
        pxm_shadow.setAlphaChannel(pxm.alphaChannel())

        painter.drawPixmap(1, 1, pxm_shadow)
        painter.drawPixmap(0, 0, pxm)

        return ret

    def _get_or_load_atlas(self, atlas_key):
        if atlas_key in self._atlas_map:
            return self._atlas_map[atlas_key]
        if atlas_key not in self._atlas_decl:
            return QtGui.QPixmap()
        decl = self._atlas_decl[atlas_key]
        num_cols, num_rows = decl.grid
        LOG.info('Load {:d} x {:d} atlas from "{:s}"'.format(
            num_cols,
            num_rows,
            decl.path))

        atlas = QtGui.QPixmap(decl.path)
        decl.full_width = atlas.width()
        decl.full_height = atlas.height()
        decl.sprite_width = \
            float(decl.full_width) / float(num_cols) \
            if num_cols > 0 \
            else 0.0
        decl.sprite_height = \
            float(decl.full_height) / float(num_rows) \
            if num_rows > 0 \
            else 0.0
        self._atlas_map[atlas_key] = QtGui.QPixmap(decl.path)
        return self._atlas_map[atlas_key]

    def process_requests(self, num_requests):
        count = 0
        while self._icon_requests and count < num_requests:
            key = self._icon_requests.keys()[0]
            icon = self._get_or_create_icon(key=key)
            if not icon.isNull():
                for list_item in self._icon_requests[key]:
                    list_item.setIcon(icon)
            del self._icon_requests[key]
            count = count + 1
