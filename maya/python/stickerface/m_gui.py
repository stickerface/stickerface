# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #


#  __  __                     ____  _           _
# |  \/  | __ _ _   _  __ _  |  _ \(_)___ _ __ | | __ _ _   _
# | |\/| |/ _` | | | |/ _` | | | | | / __| '_ \| |/ _` | | | |
# | |  | | (_| | |_| | (_| | | |_| | \__ \ |_) | | (_| | |_| |
# |_|  |_|\__,_|\__, |\__,_| |____/|_|___/ .__/|_|\__,_|\__, |
#               |___/                    |_|            |___/


#   ___                      _     _                  _
#  / _ \__   _____ _ __ _ __(_) __| | ___    ___ ___ | | ___  _ __ ___
# | | | \ \ / / _ \ '__| '__| |/ _` |/ _ \  / __/ _ \| |/ _ \| '__/ __|
# | |_| |\ V /  __/ |  | |  | | (_| |  __/ | (_| (_) | | (_) | |  \__ \
#  \___/  \_/ \___|_|  |_|  |_|\__,_|\___|  \___\___/|_|\___/|_|  |___/

def _enum_override_color(**kwargs):
    return type('OverrideColor', (), kwargs)


OverrideColor = _enum_override_color(
    Yellow=17,
    Green=14,
    Blue=6,
    Red=13,
    Dark_Blue=5,
    Dark_Red=4,
)
