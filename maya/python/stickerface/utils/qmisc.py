# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import os

from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

LOG = logging.getLogger(__name__)


def _enum(**kwargs):
    return type('BrowseAct', (), kwargs)


def _get_ideal_width(widget, num_chars):
    fm = widget.fontMetrics()
    mrg = widget.contentsMargins()
    return fm.width('x') * num_chars + mrg.left() + mrg.right()


def set_fixed_width(widget, num_chars):
    widget.setFixedWidth(
        _get_ideal_width(widget=widget, num_chars=num_chars) + 8)


def set_line_edit_fixed_width(edit, num_chars):
    mrg = edit.textMargins()
    edit.setFixedWidth(
        _get_ideal_width(widget=edit, num_chars=num_chars) +
        mrg.left() + mrg.right() + 8)


#  ____  _                              _   _ _   _ _
# |  _ \(_)_  ___ __ ___   __ _ _ __   | | | | |_(_) |___
# | |_) | \ \/ / '_ ` _ \ / _` | '_ \  | | | | __| | / __|
# |  __/| |>  <| | | | | | (_| | |_) | | |_| | |_| | \__ \
# |_|   |_/_/\_\_| |_| |_|\__,_| .__/   \___/ \__|_|_|___/
#                              |_|

def create_empty_pixmap(size):
    pixmap = QtGui.QPixmap(size)
    pixmap.fill(QtGui.QColor(0, 0, 0, 0))
    return pixmap


def add_dropped_shadow(pixmap, color, offset=1.0):
    '''
    Add colored outline to the sprite for better readability.
    '''
    def _get_shadow(pixmap, color):
        ret = pixmap.copy()
        painter = QtGui.QPainter(ret)
        painter.setCompositionMode(QtGui.QPainter.CompositionMode_SourceIn)
        painter.fillRect(pixmap.rect(), color)
        return ret

    ret = QtGui.QPixmap(pixmap.width(), pixmap.height())
    ret.fill(QtGui.QColor(0, 0, 0, 0))
    painter = QtGui.QPainter(ret)

    shadow_pixmap = _get_shadow(pixmap=pixmap, color=color)
    painter.drawPixmap(offset, offset, shadow_pixmap)
    painter.drawPixmap(0, 0, pixmap)

    return ret


#  ____                                  _        _   _
# | __ ) _ __ _____      _____  ___     / \   ___| |_(_) ___  _ __
# |  _ \| '__/ _ \ \ /\ / / __|/ _ \   / _ \ / __| __| |/ _ \| '_ \
# | |_) | | | (_) \ V  V /\__ \  __/  / ___ \ (__| |_| | (_) | | | |
# |____/|_|  \___/ \_/\_/ |___/\___| /_/   \_\___|\__|_|\___/|_| |_|

BrowseAct = _enum(
    GET_DIR=0,
    OPEN_FILE=1,
    SAVE_FILE=2)


def display_location(path, line_edit=None):
    if os.path.exists(path):
        path = os.path.normcase(os.path.normpath(os.path.abspath(path)))
    path = path.replace('\\', '/')

    if line_edit:
        # depending on the width of the text field, try to display the
        # most comprehensible path reduction possible (aka, the largest).
        tokens = path.split('/')
        candidates = [path] + [
            '/'.join([tokens[0], '..'] + tokens[i:])
            for i in range(2, len(tokens))
        ] + [
            os.path.basename(path),
            os.path.splitext(os.path.basename(path))[0]
        ]
        max_width = line_edit.width()
        mrg = line_edit.contentsMargins()
        max_width = max_width - mrg.left() - mrg.right()
        mrg = line_edit.textMargins()
        max_width = max_width - mrg.left() - mrg.right()
        max_width = max_width - 24

        fm = QtGui.QFontMetricsF(line_edit.font())

        label = None
        for candidate in candidates:
            if fm.width(candidate) <= max_width:
                label = candidate
                break
        if label is None:
            label = candidates[-1]

        line_edit.setText(label)
        line_edit.setToolTip(path)
    return path


def browse_location(
    action,
    caption,
    filtr='',
    cur_path='',
    parent=None,
    line_edit=None
):
    FUNC = {
        BrowseAct.GET_DIR: QtWidgets.QFileDialog.getExistingDirectory,
        BrowseAct.OPEN_FILE: QtWidgets.QFileDialog.getOpenFileName,
        BrowseAct.SAVE_FILE: QtWidgets.QFileDialog.getSaveFileName,
    }

    path = ''
    if action in FUNC:
        path = FUNC[action](
            parent=parent,
            dir=os.path.dirname(cur_path) if cur_path else '.',
            caption=caption,
            filter=filtr)
        path = path[0] if isinstance(path, tuple) else ''

    return display_location(line_edit=line_edit, path=path)
