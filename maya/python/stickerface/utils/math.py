# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
from stickerface.utils import error

reload(error)


def _enum(*args, **kwargs):
    d = dict(zip(args, range(len(args))), **kwargs)
    return type('Op', (), d)


Op = _enum(
    'ADD',
    'SUB',
    'MUL',
    'DIV')


def _is_float_list(v, size):
    return \
        v and \
        isinstance(v, list) and \
        len(v) == size and \
        all([isinstance(_, float) for _ in v])


def is_v3f(v):
    return _is_float_list(v=v, size=3)


def is_m44f(m):
    return _is_float_list(v=m, size=16)


def add(v1, v2):
    if not isinstance(v1, list) or not isinstance(v2, list):
        raise error.Error('Cannot add {} and {}.'.format(v1, v2))
    return [_1 + _2 for _1, _2 in zip(v1, v2)]


def identity_matrix():
    ret = [0.0] * 16
    for i in range(4):
        ret[5 * i] = 1.0
    return ret


def translation_matrix(v):
    ret = identity_matrix()
    if is_v3f(v=v):
        ret[12:15] = v
    return ret
