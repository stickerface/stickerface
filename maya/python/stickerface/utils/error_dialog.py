# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging

from stickerface.Qt import QtCore
from stickerface.Qt import QtWidgets

LOG = logging.getLogger(__name__)


#  _____                       ____  _       _
# | ____|_ __ _ __ ___  _ __  |  _ \(_) __ _| | ___   __ _
# |  _| | '__| '__/ _ \| '__| | | | | |/ _` | |/ _ \ / _` |
# | |___| |  | | | (_) | |    | |_| | | (_| | | (_) | (_| |
# |_____|_|  |_|  \___/|_|    |____/|_|\__,_|_|\___/ \__, |
#                                                    |___/

class ErrorDialog(QtWidgets.QDialog):

    def __init__(self, message, parent=None, f=QtCore.Qt.WindowFlags()):
        super(ErrorDialog, self).__init__(parent=parent, f=f)
        self._message = message
        self._init_widget()

    def _init_widget(self):
        self.setWindowTitle('Error :(')
        main_layout = QtWidgets.QVBoxLayout()

        text_edit = QtWidgets.QPlainTextEdit()

        text_edit.setPlainText(self._message)
        text_edit.setReadOnly(True)
        text_edit.adjustSize()

        font_metrics = text_edit.fontMetrics()
        text_size = font_metrics.size(0, self._message)
        LOG.info(
            'error message size: {:d} x {:d}'.format(
                text_size.width(),
                text_size.height()))
        text_width = text_size.width() + 26
        text_height = text_size.height() + 26

        text_width = min(1024, text_width)
        text_height = min(800, text_height)
        text_edit.setFixedSize(text_width, text_height)

        ok_button = QtWidgets.QPushButton('OK')
        copy_to_clipboard_button = QtWidgets.QPushButton('Copy to clipboard')
        lower_layout = QtWidgets.QHBoxLayout()

        lower_layout.addStretch()
        lower_layout.addWidget(copy_to_clipboard_button)
        lower_layout.addWidget(ok_button)

        main_layout.addWidget(text_edit)
        main_layout.addLayout(lower_layout)
        main_layout.addStretch()

        self.setLayout(main_layout)

        ok_button.clicked.connect(
            self._ok_button_clicked)
        copy_to_clipboard_button.clicked.connect(
            self._copy_to_clipboard_button_clicked)

    def _ok_button_clicked(self):
        self.accept()

    def _copy_to_clipboard_button_clicked(self):
        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.setText(self._message)
        LOG.info('Copied to clipboard')
