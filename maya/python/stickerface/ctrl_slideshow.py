# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging

from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import picker_icon  # noqa I100
from stickerface.utils import qmisc

reload(qmisc)
reload(picker_icon)

LOG = logging.getLogger(__name__)


#  ____                 _ _ _                  _____                 _
# / ___|  ___ _ __ ___ | | | |__   __ _ _ __  | ____|_   _____ _ __ | |_
# \___ \ / __| '__/ _ \| | | '_ \ / _` | '__| |  _| \ \ / / _ \ '_ \| __|
#  ___) | (__| | | (_) | | | |_) | (_| | |    | |___ \ V /  __/ | | | |_
# |____/ \___|_|  \___/|_|_|_.__/ \__,_|_|    |_____| \_/ \___|_| |_|\__|

class _ScrollbarEventFilter(QtCore.QObject):

    def __init__(self, parent):
        super(_ScrollbarEventFilter, self).__init__(parent=parent)
        self._start_x = 0.0

    def eventFilter(self, object, event):
        try:
            evt_type = event.type()
        except AttributeError:
            return False

        if any([evt_type == _ for _ in [
            QtCore.QEvent.Type.MouseButtonPress,
            QtCore.QEvent.Type.MouseMove,
            QtCore.QEvent.Type.MouseButtonRelease,
            QtCore.QEvent.Type.MouseButtonDblClick,
            QtCore.QEvent.Type.Wheel,
        ]]):
            return True  # skips all mouse related events

        elif evt_type == QtCore.QEvent.Type.MouseButtonPress:
            self._start_x = event.x()
            self._start_pos = object.value()
            LOG.info('MouseButtonPress')
            return True

        elif evt_type == QtCore.QEvent.Type.MouseButtonRelease:
            LOG.info('MouseButtonRelease')
            return True

        elif evt_type == QtCore.QEvent.Type.MouseMove:
            item_width = self.parent().sizeHintForColumn(0)
            new_pos = self._start_pos + event.x() - self._start_x
            new_pos = (new_pos / item_width) * item_width
            object.setValue(new_pos)
            LOG.info('MouseMove')
            return True

        return False


#   ____ _        _   ____  _ _     _           _
#  / ___| |_ _ __| | / ___|| (_) __| | ___  ___| |__   _____      __
# | |   | __| '__| | \___ \| | |/ _` |/ _ \/ __| '_ \ / _ \ \ /\ / /
# | |___| |_| |  | |  ___) | | | (_| |  __/\__ \ | | | (_) \ V  V /
#  \____|\__|_|  |_| |____/|_|_|\__,_|\___||___/_| |_|\___/ \_/\_/

class ControllerSlideshow(QtWidgets.QListWidget):

    _BLANK_ICON = qmisc.create_empty_pixmap(size=picker_icon.SIZE)

    #  __  __                        ____                 _ _
    # |  \/  | ___  _   _ ___  ___  / ___|  ___ _ __ ___ | | |
    # | |\/| |/ _ \| | | / __|/ _ \ \___ \ / __| '__/ _ \| | |
    # | |  | | (_) | |_| \__ \  __/  ___) | (__| | | (_) | | |
    # |_|  |_|\___/ \__,_|___/\___| |____/ \___|_|  \___/|_|_|

    class _MouseScroll(object):
        def __init__(self, parent):
            self._parent = parent
            self._valid = False
            self._mouse_x = 0
            self._hscroll = 0

        def clear(self):
            self._valid = False
            self._mouse_x = 0
            self._hscroll = 0

        def _accept(self, event):
            mid_button = int(event.buttons()) & QtCore.Qt.MidButton
            alt_modifier = int(event.modifiers()) & QtCore.Qt.AltModifier
            return mid_button and alt_modifier

        def apply(self, event):
            if not event or not self._parent or not self._accept(event=event):
                return
            if not self._valid:
                # start mouse interaction
                self._valid = True
                self._mouse_x = event.x()
                self._hscroll = self._parent.horizontalScrollBar().value()

            item_width = self._parent.sizeHintForColumn(0)
            if item_width:
                delta = event.x() - self._mouse_x
                delta = int(delta / (item_width / 2))
                bar_value = self._hscroll + delta * item_width
                self._parent.horizontalScrollBar().setValue(bar_value)

    def __init__(self, icon_cache, parent=None):
        super(ControllerSlideshow, self).__init__(parent=parent)

        self._sprites = []
        self._icon_cache = icon_cache
        self._shift_pressed = False
        self._mouse_scroll = self._MouseScroll(parent=self)
        # (valid?, mouse xpos, scrollbar pos)

        # configure list item display
        self.setDragEnabled(False)
        self.setViewMode(QtWidgets.QListWidget.IconMode)
        self.setIconSize(picker_icon.SIZE)
        self.setWrapping(False)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setStyleSheet('''
            QListWidget {
                border: none;
                background-color: rgba(0, 0, 0, 0);
            }''')

        self.horizontalScrollBar().setStyleSheet('''
            QScrollBar:horizontal {
                border: none;
                background: palette(base);
                height: 4px;
                margin: 0 0 0 0;
            }
            QScrollBar::handle:horizontal {
                background: gray;
                width: 20px;
            }
            QScrollBar::add-line:horizontal {
                background: none;
                border: none;
            }
            QScrollBar::sub-line:horizontal {
                background: none;
                border: none;
            }
            QScrollBar:
            left-arrow:horizontal,
            QScrollBar::right-arrow:horizontal {
                background: none;
                border: none;
            }
            QScrollBar::
            add-page:horizontal,
            QScrollBar::sub-page:horizontal {
                background: none;
                border: none;
        }''')
        self.horizontalScrollBar().installEventFilter(
            _ScrollbarEventFilter(parent=self))

    def show_sprites(self, atlas_path, start_index, end_index):
        self.clear()
        for idx in range(start_index, end_index + 1):
            list_item = QtWidgets.QListWidgetItem()
            list_item.setIcon(self._BLANK_ICON)  # placeholder
            list_item.setToolTip('{:s} [{:d}]'.format(atlas_path, idx))
            list_item.setData(QtCore.Qt.UserRole, idx - start_index)
            self.addItem(list_item)

            self._icon_cache.set_item_icon(
                list_item=list_item,
                path=atlas_path,
                idx=idx)

    def minimumSizeHint(self):
        return QtCore.QSize(-1, picker_icon.SIZE.height() + 6)

    def mouseMoveEvent(self, event):
        self._mouse_scroll.apply(event=event)
        # IMPORTANT: do not let qmisc handle mouse move events naturally
        # in order to prevent drag/drop events from freezing maya

    def mouseReleaseEvent(self, event):
        self._mouse_scroll.clear()
        self._shift_pressed = \
            int(event.modifiers()) & int(QtCore.Qt.Modifier.SHIFT) != 0
        super(ControllerSlideshow, self).mouseReleaseEvent(event)
        event.setAccepted(False)  # needed to have the parent changes selection

    def resizeEvent(self, event):
        '''
        Catches resize event ordering the widget to resize so that
        its width remains a multiple of its items' common width.
        '''
        item_width = self.sizeHintForColumn(0)
        if item_width > 0:
            full_width = self.count() * item_width
            width = (event.size().width() / item_width) * item_width
            if width > full_width:
                width = full_width
            if full_width % width != 0:
                full_width = ((full_width / width) + 1) * width

            bar = self.horizontalScrollBar()
            bar.setMinimum(0)
            bar.setMaximum(full_width - width)
            self.resize(width, self.height())  # only the width is changed
        event.accept()
