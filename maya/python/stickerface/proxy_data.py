# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
from stickerface.cgfx import shader_proj

reload(shader_proj)


DEFAULT_GAMMA = 2.2


#  ____                        ____        _
# |  _ \ _ __ _____  ___   _  |  _ \  __ _| |_ __ _
# | |_) | '__/ _ \ \/ / | | | | | | |/ _` | __/ _` |
# |  __/| | | (_) >  <| |_| | | |_| | (_| | || (_| |
# |_|   |_|  \___/_/\_\\__, | |____/ \__,_|\__\__,_|
#                      |___/

class ProxyData(object):
    def __init__(self):
        self.asset_name = ''
        self.proxy = ''
        self.proxy_label = ''
        self.shape = ''
        self.proj_type = shader_proj.ShaderProj.UNSET
        self.body_path = ''
        self.atlas_path = ''
        self.gamma = DEFAULT_GAMMA
