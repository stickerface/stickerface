# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import sys

from stickerface.Qt import QtWidgets

try:
    from shiboken2 import wrapInstance
except ImportError:
    try:
        from shiboken import wrapInstance
    except ImportError as e:
        sys.stderr.write(e)

import maya



#  __  __                    __        ___           _
# |  \/  | __ _ _   _  __ _  \ \      / (_)_ __   __| | _____      __
# | |\/| |/ _` | | | |/ _` |  \ \ /\ / /| | '_ \ / _` |/ _ \ \ /\ / /
# | |  | | (_| | |_| | (_| |   \ V  V / | | | | | (_| | (_) \ V  V /
# |_|  |_|\__,_|\__, |\__,_|    \_/\_/  |_|_| |_|\__,_|\___/ \_/\_/
#               |___/

def main_window():
    return wrapInstance(
        long(maya.OpenMayaUI.MQtUtil.mainWindow()),
        QtWidgets.QMainWindow)
