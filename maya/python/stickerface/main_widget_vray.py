# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import os
import sys
import traceback

from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import extra_attr  # noqa I100
from stickerface import folder_replace
from stickerface import gui_util
from stickerface import main_widget_base
from stickerface import proxy_card
from stickerface import proxy_data
from stickerface.cgfx import shader
from stickerface.utils import error

reload(gui_util)
reload(main_widget_base)
reload(extra_attr)
reload(proxy_data)
reload(proxy_card)
reload(folder_replace)
reload(error)
reload(shader)

LOG = logging.getLogger(__name__)


# __     ______              __        ___     _            _
# \ \   / /  _ \ __ _ _   _  \ \      / (_) __| | __ _  ___| |_
#  \ \ / /| |_) / _` | | | |  \ \ /\ / /| |/ _` |/ _` |/ _ \ __|
#   \ V / |  _ < (_| | |_| |   \ V  V / | | (_| | (_| |  __/ |_
#    \_/  |_| \_\__,_|\__, |    \_/\_/  |_|\__,_|\__, |\___|\__|
#                     |___/                      |___/

class MainWidget(main_widget_base.MainWidgetBase):

    def __init__(self, cmds, parent=None, f=QtCore.Qt.WindowFlags()):
        super(MainWidget, self).__init__(cmds=cmds, parent=parent, f=f)
        self._finalize()

    def _finalize(self):
        self._proxy_card_map = {}  # full path -> proxy card widget
        self._proxy_layout = QtWidgets.QHBoxLayout()

        main_layout = QtWidgets.QVBoxLayout()
        scroll = QtWidgets.QScrollArea()
        in_scroll_widget = QtWidgets.QWidget()
        in_scroll_layout = QtWidgets.QVBoxLayout()

        header = self._create_header()

        in_scroll_layout.addLayout(self._proxy_layout)
        in_scroll_layout.addStretch(1)
        in_scroll_widget.setLayout(in_scroll_layout)
        # scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setStyleSheet('QWidget { background: rgba(0, 0, 0, 0); }')
        scroll.setWidget(in_scroll_widget)

        main_layout.addWidget(header)
        main_layout.addWidget(scroll)
        # main_layout.addStretch(1)
        main_layout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(main_layout)
        self._reload()

    def _create_header(self):

        frame = QtWidgets.QFrame()
        layout = QtWidgets.QHBoxLayout()
        tools_left = QtWidgets.QToolBar()
        tools_right = QtWidgets.QToolBar()
        reload_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('reload.png')),
            'Reload',
            self)
        remap_rsrc_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('replace_folder.png')),
            'Remap resource paths',
            self)
        exec_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('vray.png')),
            'Create VRay textures for all assets',
            self)

        frame.setStyleSheet('''
QWidget
{
    background: palette(dark);
    color: palette(windowtext);
    font-size: 16px;
}
        ''')

        for _ in [tools_left, tools_right]:
            _.setIconSize(self.ICON_SIZE)
        for _ in [reload_act]:
            tools_left.addAction(_)
        for _ in [remap_rsrc_act, exec_act]:
            tools_right.addAction(_)

        layout.addWidget(tools_left)
        layout.addStretch(1)
        layout.addWidget(tools_right)
        layout.setContentsMargins(4, 4, 4, 4)
        frame.setLayout(layout)

        reload_act.triggered.connect(
            self._reload)
        remap_rsrc_act.triggered.connect(
            self._remap_resource_paths)
        exec_act.triggered.connect(
            self._setup_vray_textures)

        return frame

    def _clear(self):
        self.clear_layout(layout=self._proxy_layout)
        self._proxy_card_map = {}

    def _reload(self):

        self._clear()

        # create a card widget for all proxy objects
        self._proxy_card_map = {
            obj: proxy_card.ProxyCard(
                prx_data=self._create_proxy_data(obj=obj),
                parent=self)
            for obj in self._helper.get_proxy_objects()
        }
        for k, v in self._proxy_card_map.items():
            self._proxy_layout.addWidget(v)
        self._proxy_layout.addStretch(1)

    def _create_proxy_data(self, obj):
        # consider only the proxy's baked attributes
        attrs = [
            attr
            for attr in self.cmds.get_attributes(obj=obj)
            if extra_attr.is_decorated(attr=attr)
        ]

        def _get_attr(hints):
            '''
            Returns: first baked attributes containing one of the specified
            hints as subtring.
            '''
            ret = [
                attr
                for attr in attrs
                if any([_ in attr.lower() for _ in hints])
            ]
            return ret[0] if ret else ''

        values = {
            k: self.cmds.get_attribute(obj=obj, attr=_get_attr(hints=hints))
            for k, (default, hints) in {
                'asset_name': ('', ['asset_name']),
                'proj_type': (0, ['projection_type']),
                'body_path': ('', ['body_texture']),
                'atlas_path': ('', ['sprite_sheet']),
            }.items()
        }

        data = proxy_data.ProxyData()

        data.proxy = obj
        data.proxy_label = self.cmds.get_short_name(obj=obj)
        data.asset_name = values['asset_name']
        data.proj_type = values['proj_type']
        data.body_path = values['body_path']
        data.atlas_path = values['atlas_path']
        data.shape = self.cmds.get_short_name(
            obj=self._helper.get_baked_face(asset_name=data.asset_name))

        return data

    def _remap_resource_path(self, path, src_dir, dst_dir):
        if not any([
            os.path.isfile(path),
            os.path.isdir(src_dir),
            os.path.isdir(dst_dir)
        ]):
            return path

        try:
            p = os.path.relpath(path=path, start=src_dir).split(os.path.sep)
            if p and p[0] == '..':
                LOG.warning(
                    'Skip path remapping of "{}" from "{}" to "{}"'.format(
                        path, src_dir, dst_dir))
                return path
            LOG.info('relative path = {}'.format(p))
            return os.path.join(dst_dir, os.path.sep.join(p))

        except ValueError as e:
            LOG.warning(
                'Cannot remap path "{}" from "{}" to "{}":\n\t{}'.format(
                    path, src_dir, dst_dir, e.message))

        return path

    def _remap_resource_paths(self):
        dialog = folder_replace.FolderReplacingDialog(parent=self)
        dialog.setWindowTitle('Resource Path Remapping')

        status = dialog.exec_()
        if status != QtWidgets.QDialog.Accepted:
            return

        src_dir = dialog.source_folder
        dst_dir = dialog.destination_folder
        if any([not os.path.isdir(_) for _ in [src_dir, dst_dir]]):
            return
        if src_dir == dst_dir:
            return

        LOG.info(
            'Remap resources\n\tfrom "{}"\n\tto "{}"'.format(
                src_dir, dst_dir))
        for prx_card in self._proxy_card_map.values():
            prx_card.set_body_path(p=self._remap_resource_path(
                path=prx_card.proxy_data.body_path,
                src_dir=src_dir,
                dst_dir=dst_dir))
            prx_card.set_atlas_path(p=self._remap_resource_path(
                path=prx_card.proxy_data.atlas_path,
                src_dir=src_dir,
                dst_dir=dst_dir))

    def _setup_vray_textures(self):
        self.cmds.begin_undo()
        try:
            for prx_card in self._proxy_card_map.values():
                self._helper.setup_vray_texture(prx_data=prx_card.proxy_data)
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            raise error.Error(
                'Failed to setup VRay textures.\n{}\n{}'.format(
                    e.message,
                    ''.join(traceback.format_tb(exc_traceback))))
        finally:
            self.cmds.end_undo()
