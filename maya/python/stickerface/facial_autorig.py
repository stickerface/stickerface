# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import re

from stickerface import attr_type
from stickerface import ctrl_flag
from stickerface import extra_attr
from stickerface import sprite_offsets
from stickerface import sprite_shapes
from stickerface.cgfx import shader
from stickerface.utils import cond
from stickerface.utils import error
from stickerface.utils import math

reload(shader)
reload(error)
reload(math)
reload(cond)
reload(attr_type)
reload(extra_attr)
reload(sprite_shapes)
reload(sprite_offsets)
reload(ctrl_flag)

LOG = logging.getLogger(__name__)


#  ____     _   _____          _       _      _         _             _
# |___ \ __| | |  ___|_ _  ___(_) __ _| |    / \  _   _| |_ ___  _ __(_) __ _
#   __) / _` | | |_ / _` |/ __| |/ _` | |   / _ \| | | | __/ _ \| '__| |/ _` |
#  / __/ (_| | |  _| (_| | (__| | (_| | |  / ___ \ |_| | || (_) | |  | | (_| |
# |_____\__,_| |_|  \__,_|\___|_|\__,_|_| /_/   \_\__,_|\__\___/|_|  |_|\__, |
#                                                                       |___/

class FacialAutorig(object):

    def __init__(self, helper):
        self._helper = helper

    @property
    def cmds(self):
        return self._helper.cmds

    def _assign_textures_to_material(
        self,
        mat,
        body_texture_path,
        sprite_atlas_path
    ):
        '''
        Create textures and connect them to the material.
        Returns: list of generated nodes
        '''
        ret = []
        for attr, path in {
            'body_texture': body_texture_path,
            'sprite_sheet': sprite_atlas_path,
        }.items():
            file, p2dtex = self.cmds.create_texture_from_path(path=path)
            self.cmds.connect(
                src=file,
                dst=mat,
                attrs={attr: 'outColor'})
            ret.append([file, p2dtex])
        return ret

    def _get_world_center_and_range(self, objs):
        bounds = self.cmds.get_world_bbox(objs=objs)
        center = [0.5 * (bounds[i] + bounds[3 + i]) for i in range(3)]
        rng = [bounds[3 + i] - bounds[i] for i in range(3)]
        return [center, rng]

    def _setup_shader_frame(self, mat, shape, parent):
        '''
        Create center/front/up locators and connect them to the material.
        Returns: list of generated nodes
        '''
        center, rng = self._get_world_center_and_range(objs=[shape])
        LOG.info('Worldspace center = {}'.format(center))
        LOG.info('Worldspace range = {}'.format(rng))

        mat_attrs = self.cmds.get_attributes(
            obj=mat,
            typ=attr_type.AttrType.V3F)

        ret = []
        for i, location in enumerate(['Center', 'Front', 'Up']):
            for suffix in ['', 'Position', 'WPosition']:
                mat_attr = 'u' + location + suffix
                if mat_attr not in mat_attrs:
                    continue

                loc_name = ('face_' + location + '_loc').lower()
                offset = [
                    0.0,
                    1.0 if (i >> 1) & 0x1 != 0 else 0.0,
                    1.0 if i & 0x1 != 0 else 0.0
                ]
                offset = [offset[i] * 1.1 * rng[i] for i in range(3)]
                trf, loc = self.cmds.create_locator(
                    name=loc_name,
                    parent=parent,
                    ws_pos=math.add(v1=center, v2=offset))
                self.cmds.connect(
                    src=loc,
                    dst=mat,
                    attrs={mat_attr: 'worldPosition'})
                ret.append([trf, loc])
        return ret

    def _clear(self):
        pass

    def _create_root(self, shape, proj, idx_offsets, idx_defaults):
        center, rng = self._get_world_center_and_range(objs=[shape])
        wpos = math.add(v1=center, v2=[0.0, 0.0, 1.0 + rng[2] * 0.5])

        LOG.info('world center = {}\nworld range = {}\nwpos = {}'.format(
            center, rng, wpos))

        root = self.cmds.create_group(
            name='face_anim_root',
            ws_mtx=math.translation_matrix(v=wpos))
        # root = self.cmds.create_group(name='face_anim_root')

        # add extra attributes to the root
        self.cmds.set_attribute(
            obj=root,
            attr=extra_attr.ExtraAttr.PROJ_TYPE,
            value=int(proj),
            lock=True)
        self.cmds.set_attribute(
            obj=root,
            attr=extra_attr.ExtraAttr.GLOBAL_SCALE,
            value=1.0)
        for sprt in shader.get_sprites(proj=proj):
            self.cmds.set_attribute(
                obj=root,
                attr=extra_attr.ExtraAttr.scale_tweak(sprt=sprt),
                value=1.0)
        for sprt in shader.get_sprites(proj=proj):
            idx_offset = idx_offsets.get(sprt, 0)
            idx_default = max(0, idx_defaults.get(sprt, 0) - idx_offset)

            self.cmds.set_attribute(
                obj=root,
                attr=extra_attr.ExtraAttr.index_offset(sprt=sprt),
                value=idx_offset,
                min_value=0)
            self.cmds.set_attribute(
                obj=root,
                attr=extra_attr.ExtraAttr.index_default(sprt=sprt),
                value=idx_default,
                min_value=0)
        self._set_control_flags(obj=root, f=ctrl_flag.CtrlFlag.ROOT)

        # add an extra attribute to the root in order to trigger the
        # mirroring of the whole facial expression (used by the mirroring and
        # symmetrization tools)
        self.cmds.set_attribute(
            obj=root,
            attr=extra_attr.ExtraAttr.FLIP,
            value=False)

        flip_obj, flip_attr = self.cmds.condition(
            lvalue=(root, extra_attr.ExtraAttr.FLIP),
            op=cond.Comp.EQ,
            rvalue=0.0,
            if_true=[1.0, 1.0, 1.0],
            if_false=[-1.0, 1.0, 1.0],
            name='flip_cond')

        self.cmds.connect(
            src=flip_obj,
            dst=root,
            attrs={'scaleX': flip_attr + 'R'})

        return root

    def _create_all_sprite_controls(self, proj, mat, shape, root, idx_ranges):
        # get sprite names from the shader declarations
        sprts = shader.get_sprites(proj=proj)
        inside_cstrs = shader.get_inside_constraints(proj=proj)

        # use the shape's bounding box in world space to scale and position
        # controllers' displayed shapes.
        center, rng = self._get_world_center_and_range(objs=[shape])
        shape_scale = min(rng[0:2])

        # create controls for each recognized sprite
        for sprt in sprts:
            idx_range = idx_ranges.get(sprt, 0)
            self._create_sprite_controls(
                sprt=sprt,
                mat=mat,
                root=root,
                shape_scale=shape_scale,
                idx_range=idx_range if idx_range > 0 else None)

        # create additional control levels
        for _ in ['eyes', 'face']:
            if _ not in sprts:
                self._create_sprite_controls(
                    sprt=_,
                    mat=mat,
                    root=root,
                    shape_scale=shape_scale)
                sprts.append(_)

        # ensure the controller hierarchy preserves inclusion relationships
        for sprt in sprts:

            if sprt in inside_cstrs:
                parent_sprt = inside_cstrs[sprt]
            elif 'face' in sprt:
                parent_sprt = ''
            elif 'eyeball' in sprt:
                parent_sprt = 'eyes'
            else:
                parent_sprt = 'face'

            if not parent_sprt:
                continue

            parent = self._get_cnx_null(top=root, sprt=parent_sprt)
            if not parent:
                parent = root

            self.cmds.set_parent(
                child=self._get_anim_null(top=root, sprt=sprt),
                parent=parent,
                keep_local=True)

        # place the face animation controller's pivot at the shape center's
        # world position.
        self.cmds.set_pivot(
            obj=self._get_anim(top=root, sprt='face'),
            ws_pos=center)

        # connect all sprites' mappings to the cgfx material
        self._connect_all_sprite_mappings(proj=proj, root=root, mat=mat)

        # handle the left side of the face
        for sprt in sprts:
            side = sprt[-1]
            if side != 'L':
                continue
            if not any([_ in sprt for _ in ['brow', 'eyeball', 'pupil']]):
                continue
            obj = self._get_anim_null(top=root, sprt=sprt)
            tx = self.cmds.get_attribute(obj=obj, attr='translateX')
            if tx:
                self.cmds.set_attribute(obj=obj, attr='translateX', value=-tx)
            self.cmds.set_attribute(obj=obj, attr='scaleX', value=-1.0)

    def _create_sprite_controls(
        self,
        sprt,
        mat,
        root,
        shape_scale,
        idx_range=None
    ):
        LOG.info('Create sprite controls for {}'.format(sprt))

        flags = 0
        if sprt.endswith('L'):
            flags = flags | ctrl_flag.CtrlFlag.LEFT
        elif sprt.endswith('R'):
            flags = flags | ctrl_flag.CtrlFlag.RIGHT
        else:
            flags = flags | ctrl_flag.CtrlFlag.CENTRAL

        anim_null = self.cmds.create_group(
            name=sprt + '_anim_null',
            parent=root)

        curv_null = self.cmds.create_group(
            name=sprt + '_curve_null',
            parent=anim_null)

        anim, curv = self.cmds.create_curve(
            name=sprt + '_anim',
            parent=curv_null,
            cvs=sprite_shapes.get_cvs(sprt=sprt, scaling=shape_scale),
            ctrl_flags=flags)

        cnx_null = cnx_null = self.cmds.create_group(
            name=sprt + '_connected_null',
            parent=anim_null)

        for _, f in {
            anim: ctrl_flag.CtrlFlag.ANIM,
            anim_null: ctrl_flag.CtrlFlag.ANIM_NULL,
            curv_null: ctrl_flag.CtrlFlag.CURV_NULL,
            cnx_null: ctrl_flag.CtrlFlag.CNX_NULL,
        }.items():
            self._set_control_flags(obj=_, f=flags | f)

        # anim.SRT -> connected_null.SRT
        # anim.pivots -> connected_null.pivots
        self.cmds.connect(
            src=anim,
            dst=cnx_null,
            attrs=[
                'translate',
                'rotate',
                'scale',
                'rotatePivot',
                'scalePivot'])

        index_attr = 'uIndex_' + sprt
        if self.cmds.exists(obj=mat, attr=index_attr):
            # get default value for sprite index stored on root transform
            idx_value = self.cmds.get_attribute(
                obj=root,
                attr=extra_attr.ExtraAttr.index_default(sprt=sprt))

            # create extra attribute on the controller
            self.cmds.set_attribute(
                obj=anim,
                attr=extra_attr.ExtraAttr.SPRT_IDX,
                value=0 if idx_value is None else idx_value,
                max_value=idx_range)

            # controller's index value (+) root's index offset for sprite -> op
            op, op_attr = self._add(
                in1=root,
                in2=anim,
                attrs=[(
                    extra_attr.ExtraAttr.index_offset(sprt=sprt),
                    extra_attr.ExtraAttr.SPRT_IDX)],
                name='offset_' + sprt + '_index')

            # connect result to index input attribute on material
            self.cmds.connect(
                src=op,
                dst=mat,
                attrs={index_attr: op_attr})

        # account for automatic sprite offset
        LOG.info('Translate controller for sprite "{:s}"'.format(sprt))
        self.cmds.set_attribute(
            obj=anim_null,
            attr='translate',
            value=sprite_offsets.get(sprt=sprt, scaling=shape_scale))

        for _ in [anim_null, curv_null, anim, curv, cnx_null]:
            self._set_sprite_name(obj=_, sprt=sprt)

    def _connect_all_sprite_mappings(self, proj, root, mat):

        def _get_locator(key):
            descs = self.cmds.filter_descendents(
                obj=root,
                cb=lambda obj:
                self.cmds.get_type(obj=obj) == 'locator' and
                key in self.cmds.get_short_name(obj=obj),
                keep_1st=True)
            return descs[0] if descs else ''

        # get the locators placed at the center and above the face
        face_center = _get_locator(key='face_center')
        face_up_pos = _get_locator(key='face_up')
        if not face_center or not face_up_pos:
            raise error.Error(
                'Failed to locate face locators under {}.'.format(root))

        # get the face's main animation controller
        face_cnx_null = self._get_cnx_null(top=root, sprt='face')

        for sprt in shader.get_sprites(proj=proj):
            # get the sprite's animation controller
            sprt_cnx_null = self._get_cnx_null(top=root, sprt=sprt)

            f = self.cmds.get_attribute(
                obj=sprt_cnx_null,
                attr=extra_attr.ExtraAttr.CTRL_FLAGS)
            f = 0 if f is None else f

            # decompose the sprite's world matrix to get its position & scale
            decomp_world_mtx = self.cmds.create_node(
                typ='decomposeMatrix',
                name=sprt + '_decomp_world_mtx'.format(sprt))

            self.cmds.connect(
                src=sprt_cnx_null,
                dst=decomp_world_mtx,
                attrs={'inputMatrix': 'worldMatrix'})

            # FRONT POSITION
            # --------------
            for _ in ['', 'W']:
                dst_attr = 'uFront' + _ + 'Position_' + sprt
                self.cmds.connect(
                    src=decomp_world_mtx,
                    dst=mat,
                    attrs={dst_attr: 'outputTranslate'})

            # MAPPING
            # -------
            # create a specific node to handle the estimation of the sprite's
            # rotation angle.
            get_rotate = self.cmds.create_node(
                typ='GetSpriteRotate',
                name=sprt + '_get_rotate')

            self.cmds.set_attribute(
                obj=get_rotate,
                attr='on_left_side',
                value=bool(f & ctrl_flag.CtrlFlag.LEFT))
            for plug, (src, src_attr) in {
                'face_center': (face_center, 'worldPosition'),
                'face_up_pos': (face_up_pos, 'worldPosition'),
                'face_world_mtx': (face_cnx_null, 'worldMatrix'),
                'sprite_world_mtx': (sprt_cnx_null, 'worldMatrix')
            }.items():
                self.cmds.connect(
                    src=src,
                    dst=get_rotate,
                    attrs={plug: src_attr})

            # global scale (x) sprite's tweak scale -> op1
            op1, op1_attr = self._mul(
                in1=root,
                in2=root,
                attrs=[(
                    extra_attr.ExtraAttr.GLOBAL_SCALE,
                    extra_attr.ExtraAttr.scale_tweak(sprt=sprt))],
                name=sprt + '_scale_tweak_mul')

            # op1 (x) sprite's world scale -> op2
            op2, op2_attr = self._mul(
                in1=op1,
                in2=decomp_world_mtx,
                attrs=[(op1_attr, 'outputScale' + _) for _ in ['X', 'Y', 'Z']],
                name=sprt + '_scale_mul')

            # adjust the sprite's scale whenever the sprite's frame changes
            # handedness
            det = self.cmds.create_node(
                name=sprt + '_det',
                typ='GetDeterminant3x3')
            self.cmds.connect(
                src=sprt_cnx_null,
                dst=det,
                attrs={'matrix': 'worldMatrix'})

            cond_node, cond_attr = self.cmds.condition(
                lvalue=(det, 'output'),
                op=cond.Comp.LESS,
                rvalue=0.0,
                if_true=[-1.0, 1.0, 1.0],
                if_false=[1.0, 1.0, 1.0],
                name=sprt + '_det_cond')

            # op2 (x) side scaling factor -> op3
            op3, op3_attr = self._mul(
                in1=cond_node,
                in2=op2,
                attrs=[
                    (cond_attr + _1, op2_attr + _2)
                    for _1, _2 in zip(['R', 'G', 'B'], ['X', 'Y', 'Z'])],
                name=sprt + '_final_scale_mul'.format(sprt))

            # connect sprite mapping information to material
            sprt_mapping = 'uMapping_' + sprt
            for plug, (src, src_attr) in {
                sprt_mapping + 'X': (op3, op3_attr + 'X'),
                sprt_mapping + 'Y': (op3, op3_attr + 'Y'),
                sprt_mapping + 'Z': (get_rotate, 'rotate_z')
            }.items():
                self.cmds.connect(src=src, dst=mat, attrs={plug: src_attr})

    def _mul(self, in1, in2, attrs, name):
        return self.cmds.mathop(
            op=math.Op.MUL, name=name, in1=in1, in2=in2, attrs=attrs)

    def _add(self, in1, in2, attrs, name):
        return self.cmds.mathop(
            op=math.Op.ADD, name=name, in1=in1, in2=in2, attrs=attrs)

    def _sub(self, in1, in2, attrs, name):
        return self.cmds.mathop(
            op=math.Op.SUB, name=name, in1=in1, in2=in2, attrs=attrs)

    def _set_control_flags(self, obj, f):
        attr = extra_attr.ExtraAttr.CTRL_FLAGS
        self.cmds.set_attribute(obj=obj, attr=attr, value=f, lock=True)

    def _set_sprite_name(self, obj, sprt):
        attr = extra_attr.ExtraAttr.SPRT_NAME
        self.cmds.set_attribute(obj=obj, attr=attr, value=sprt, lock=True)

    def _get_control_flags(self, obj):
        attr = extra_attr.ExtraAttr.CTRL_FLAGS
        value = self.cmds.get_attribute(obj=obj, attr=attr)
        return value if value is not None else 0

    def _get_sprite_name(self, obj):
        attr = extra_attr.ExtraAttr.SPRT_NAME
        value = self.cmds.get_attribute(obj=obj, attr=attr)
        return value if value is not None else ''

    def _get_transforms(self, top, sprt='', f=-1):

        def _traverse(obj, ret_wrap):
            if (self._get_control_flags(obj=obj) & f) != 0 and (
                not sprt or self._get_sprite_name(obj=obj) == sprt
            ):
                ret_wrap[0].append(obj)
            for _ in self.cmds.get_children(obj=obj):
                _traverse(obj=_, ret_wrap=ret_wrap)

        ret = []
        _traverse(obj=top, ret_wrap=[ret])
        return ret

    def _get_anim(self, top, sprt=''):
        ret = self._get_transforms(
            top=top, sprt=sprt, f=ctrl_flag.CtrlFlag.ANIM)
        return ret[0] if ret else ''

    def _get_anim_null(self, top, sprt=''):
        ret = self._get_transforms(
            top=top, sprt=sprt, f=ctrl_flag.CtrlFlag.ANIM_NULL)
        return ret[0] if ret else ''

    def _get_cnx_null(self, top, sprt=''):
        ret = self._get_transforms(
            top=top, sprt=sprt, f=ctrl_flag.CtrlFlag.CNX_NULL)
        return ret[0] if ret else ''

    def _get_anims(self, top):
        return self._get_transforms(
            top=top, sprt='', f=ctrl_flag.CtrlFlag.ANIM)

    def _create_anim_selection(self, top):
        parent_sets = [
            k
            for k, v in self.cmds.get_object_sets().items()
            if not v and 'anim' in k
        ]
        return self.cmds.create_object_set(
            name='stickerface_anim_set',
            objs=self._get_anims(top=top),
            parent=parent_sets[0] if parent_sets else '')

    def _prepare_shader_baking(self, asset, mat, root):
        RE = [re.compile(pattern=_) for _ in [
            r'\AuIndex.*',
            r'\AuMapping.*',
            r'\AuCenter.*',
            r'\Au.*Position.*',
            r'\AuNumSprite.*'
        ]]
        PATH_ATTR = 'fileTextureName'
        SET_SUFFIX = '_BKSN'

        if not asset:
            LOG.warning('Cannot prepare shader baking: no asset name.')
            return

        # determine the list of attributes to bake as well as their source
        # - attributes from the material node
        attr_map = {
            attr: (mat, attr)
            for attr in self.cmds.get_attributes(obj=mat)
            if any([_.match(attr) for _ in RE])
        }
        # - attributes from the associated file nodes
        for attr in ['body_texture', 'sprite_sheet']:
            inputs = self.cmds.filter_inputs(
                obj=mat,
                attr=attr,
                cb=lambda obj: self.cmds.exists(obj=obj, attr=PATH_ATTR)
            )
            if inputs:
                attr_map[attr] = (inputs[0], PATH_ATTR)
        # - attributes from the root node
        for attr in ['asset_name', 'projection_type']:
            attr_map[attr] = (root, attr)

        LOG.info('Bakeable attributes\n\t{}'.format(
            '\n\t'.join([
                '{} <- {}.{}'.format(k, v[0], v[1])
                for k, v in attr_map.items()])))

        # create a proxy object used to gather connections and ensure baking
        dst = self.cmds.create_group(name=mat + '_baking_proxy', parent=root)
        # - flag the proxy object as a baked 'stickerface' entity
        self.cmds.set_attribute(
            obj=dst,
            attr=extra_attr.decorate(name=extra_attr.ExtraAttr.HAS_STICKERS),
            value=True,
            lock=True)
        # - connect all proxy's attributes to their respective source node
        for name, src in attr_map.items():
            value = self.cmds.get_attribute(obj=src[0], attr=src[1])
            if not any([
                isinstance(value, _) for _ in [int, float, str, unicode]
            ]):
                continue
            dst_attr = extra_attr.decorate(name=name)
            self.cmds.set_attribute(obj=dst, attr=dst_attr, value=value)
            self.cmds.connect(src=src[0], dst=dst, attrs={dst_attr: src[1]})

        self.cmds.create_object_set(objs=[dst], name=asset + SET_SUFFIX)
        return dst

    def create_rig(
        self,
        face_shape,
        shader_proj,
        shader_path,
        body_texture_path,
        sprite_atlas_path,
        num_sprite_cols,
        num_sprite_rows,
        sprite_indices,  # sprite key -> (offset, range, default)
        parent_source,
        scale_attr,
        asset_name
    ):
        self._clear()
        orig_selection = self.cmds.get_selection()
        try:
            self.cmds.begin_undo()
            LOG.info(
                '\n{}'
                '\nFacial autorig'
                '\n\t- face shape = {}'
                '\n\t- shader projection = {}'
                '\n\t- shader path = "{}"'
                '\n\t- body texture path = "{}"'
                '\n\t- sprite atlas path = "{}" ({} x {})'
                '\n\t- parent source = "{}"'
                '\n\t- scale attribute = "{}"'
                '\n\t- asset name = "{}"'.format(
                    '=' * 79,
                    face_shape, shader_proj, shader_path, body_texture_path,
                    sprite_atlas_path, num_sprite_cols, num_sprite_rows,
                    parent_source, scale_attr, asset_name))

            shader.save(
                proj=shader_proj,
                path=shader_path)

            nodes = self.cmds.assign_new_cgfxshader(
                obj=face_shape,
                shader_path=shader_path)
            if not nodes:
                raise error.Error(
                    'Failed to assign new cgfx shader to "{}".'.format(
                        face_shape))
            mat, sg = nodes

            idx_offsets = {k: v[0] for k, v in sprite_indices.items()}
            idx_ranges = {k: v[1] for k, v in sprite_indices.items()}
            idx_defaults = {k: v[2] for k, v in sprite_indices.items()}

            root = self._create_root(
                shape=face_shape,
                proj=shader_proj,
                idx_offsets=idx_offsets,
                idx_defaults=idx_defaults)

            self.cmds.set_attribute(
                obj=root,
                attr=extra_attr.ExtraAttr.ASSET_NAME,
                value=asset_name,
                lock=True)

            self._setup_shader_frame(
                mat=mat,
                shape=face_shape,
                parent=root)
            self._create_all_sprite_controls(
                mat=mat,
                shape=face_shape,
                proj=shader_proj,
                root=root,
                idx_ranges=idx_ranges)

            self._assign_textures_to_material(
                mat=mat,
                body_texture_path=body_texture_path,
                sprite_atlas_path=sprite_atlas_path)

            for attr, value in {
                'uNumSpriteCols': num_sprite_cols,
                'uNumSpriteRows': num_sprite_rows,
            }.items():
                self.cmds.set_attribute(obj=mat, attr=attr, value=value)

            if not parent_source:
                parents = self.cmds.get_parents(obj=face_shape)
                parent_source = parents[0] if parents else ''
            if parent_source:
                self.cmds.parent_constrain(
                    obj=root,
                    parent=parent_source,
                    keep_offset=True)

            if scale_attr:
                obj, attr = scale_attr
                self.cmds.connect(
                    src=obj,
                    dst=root,
                    attrs={extra_attr.ExtraAttr.GLOBAL_SCALE: attr})

            self._create_anim_selection(top=root)
            self._prepare_shader_baking(asset=asset_name, mat=mat, root=root)
            # add the asset's name as a bakeable extra attribute to the shape
            self.cmds.set_attribute(
                obj=face_shape,
                attr=extra_attr.decorate(name=extra_attr.ExtraAttr.ASSET_NAME),
                value=asset_name,
                lock=True)

        except Exception as e:
            LOG.info('caught error {}'.format(e))
            raise
        finally:
            self.cmds.end_undo()
            self.cmds.set_selection(objs=orig_selection)
