# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import re

from stickerface import ctrl_flag
from stickerface import extra_attr
from stickerface.utils import error

reload(ctrl_flag)
reload(extra_attr)
reload(error)

LOG = logging.getLogger(__name__)


def _enum(**kwargs):
    return type('FaceSide', (), kwargs)


#  _____                ____  _     _
# |  ___|_ _  ___ ___  / ___|(_) __| | ___
# | |_ / _` |/ __/ _ \ \___ \| |/ _` |/ _ \
# |  _| (_| | (_|  __/  ___) | | (_| |  __/
# |_|  \__,_|\___\___| |____/|_|\__,_|\___|

FaceSide = _enum(LEFT=0, RIGHT=1)


def other_side(side):
    return FaceSide.LEFT if side == FaceSide.RIGHT else FaceSide.RIGHT


#  _____                _____    _ _ _   _
# |  ___|_ _  ___ ___  | ____|__| (_) |_(_) ___  _ __
# | |_ / _` |/ __/ _ \ |  _| / _` | | __| |/ _ \| '_ \
# |  _| (_| | (_|  __/ | |__| (_| | | |_| | (_) | | | |
# |_|  \__,_|\___\___| |_____\__,_|_|\__|_|\___/|_| |_|

class FaceEdit(object):

    #  __  __ _                       _____     _     _
    # |  \/  (_)_ __ _ __ ___  _ __  |_   _|_ _| |__ | | ___
    # | |\/| | | '__| '__/ _ \| '__|   | |/ _` | '_ \| |/ _ \
    # | |  | | | |  | | | (_) | |      | | (_| | |_) | |  __/
    # |_|  |_|_|_|  |_|  \___/|_|      |_|\__,_|_.__/|_|\___|

    class MirrorTable(object):

        #  _____     _     _        _____       _
        # |_   _|_ _| |__ | | ___  | ____|_ __ | |_ _ __ _   _
        #   | |/ _` | '_ \| |/ _ \ |  _| | '_ \| __| '__| | | |
        #   | | (_| | |_) | |  __/ | |___| | | | |_| |  | |_| |
        #   |_|\__,_|_.__/|_|\___| |_____|_| |_|\__|_|   \__, |
        #                                                |___/

        class Entry(object):

            def __init__(self):
                self.objs = {FaceSide.LEFT: '', FaceSide.RIGHT: ''}

            @property
            def left(self):
                return self.objs[FaceSide.LEFT]

            @property
            def right(self):
                return self.objs[FaceSide.RIGHT]

            def __repr__(self):
                return '\t[L] {}\n\t[R] {}'.format(self.left, self.right)

        def __init__(self, cmds, face_data):
            self._cmds = cmds
            self._table = {}

            ctrl_map = face_data.ctrl_map if face_data else {}
            for ctrl_data in ctrl_map.values():
                key = self._get_sprite_key(obj=ctrl_data.obj)
                side = self._get_side(obj=ctrl_data.obj)
                if any([not key, side is None]):
                    continue
                entry = self._table.get(key, self.Entry())
                entry.objs[side] = ctrl_data.obj
                self._table[key] = entry

        def get_symmetric(self, obj):
            '''
            Returns: symmetric animation controller if the animation controller
            does have a side, itself otherwise.
            '''
            key = self._get_sprite_key(obj=obj)
            side = self._get_side(obj=obj)
            if any([not key, side is None]):
                return obj
            entry = self._table.get(key, self.Entry())
            if entry.objs[side] != obj:
                return obj  # most likely NOT an animation controller
            return entry.objs[other_side(side=side)]

        def _get_sprite_key(self, obj):
            attr = extra_attr.ExtraAttr.SPRT_NAME
            name = self._cmds.get_attribute(obj=obj, attr=attr)
            if name:
                m = re.match(pattern=r'(.*)[RL]\Z', string=name)
                return m.group(1) if m else ''
            return ''

        def _get_side(self, obj):
            f = self._cmds.get_attribute(
                obj=obj,
                attr=extra_attr.ExtraAttr.CTRL_FLAGS)
            f = 0 if f is None else f
            if f & ctrl_flag.CtrlFlag.LEFT:
                return FaceSide.LEFT
            elif f & ctrl_flag.CtrlFlag.RIGHT:
                return FaceSide.RIGHT
            return None

        def __repr__(self):
            return '\n'.join([
                '"{}"\n{}'.format(k, v)
                for k, v in self._table.items()
            ])

    def __init__(self, cmds):
        self._cmds = cmds

    def _change_sprite(self, obj, value):
        attr = extra_attr.ExtraAttr.SPRT_IDX
        self._cmds.set_attribute(obj=obj, attr=attr, value=value)
        self._cmds.force_step_key_tangents(obj=obj, attr=attr)

    def _postprocess_sprite_transform(self, obj):
        for attr in ['rotateX', 'rotateY', 'scaleZ']:
            self._cmds.force_step_key_tangents(obj=obj, attr=attr)

    def reset(self, face_data):
        #                     _
        #  _ __ ___  ___  ___| |_
        # | '__/ _ \/ __|/ _ \ __|
        # | | |  __/\__ \  __/ |_
        # |_|  \___||___/\___|\__|

        if not face_data:
            return

        self._cmds.begin_undo()
        try:
            LOG.info('Reset "{}" (starting @ {})'.format(
                face_data.asset_name,
                face_data.root))

            for ctrl_key, ctrl_data in face_data.ctrl_map.items():
                ctrl = ctrl_data.obj

                # transform
                for attr, value in {
                    'translate': [0.0] * 3,
                    'rotate': [0.0] * 3,
                    'scale': [1.0] * 3
                }.items():
                    self._cmds.set_attribute(obj=ctrl, attr=attr, value=value)
                self._postprocess_sprite_transform(obj=ctrl)

                # set default sprite index value
                sprt = ctrl_data.sprite_name
                idx_default = self._cmds.get_attribute(
                    obj=face_data.root,
                    attr=extra_attr.ExtraAttr.index_default(sprt=sprt))
                if idx_default is not None:
                    self._cmds.set_attribute(
                        obj=ctrl,
                        attr=extra_attr.ExtraAttr.SPRT_IDX,
                        value=idx_default)

        except Exception as e:
            raise error.Error('Failed to reset "{}"\n{}'.format(
                face_data.asset_name,
                e.message))
        finally:
            self._cmds.end_undo()

    def _get_sided_anim_controllers(self, face_data, side):
        FLAGS = {
            FaceSide.LEFT: ctrl_flag.CtrlFlag.LEFT,
            FaceSide.RIGHT: ctrl_flag.CtrlFlag.RIGHT,
        }
        ret = []
        for ctrl_data in face_data.ctrl_map.values():
            f = self._cmds.get_attribute(
                obj=ctrl_data.obj,
                attr=extra_attr.ExtraAttr.CTRL_FLAGS)
            LOG.info(f)
            if f is not None and f & FLAGS[side]:
                ret.append(ctrl_data.obj)
        return ret

    def _is_anim_controller(self, obj):
        f = self._cmds.get_attribute(
            obj=obj,
            attr=extra_attr.ExtraAttr.CTRL_FLAGS)
        f = f if f is not None else 0
        return bool(f & ctrl_flag.CtrlFlag.ANIM)

    def _get_sprite_indices(self, face_data):
        return {
            ctrl_data.obj: self._cmds.get_attribute(
                obj=ctrl_data.obj,
                attr=extra_attr.ExtraAttr.SPRT_IDX)
            for ctrl_data in face_data.ctrl_map.values()
            if all([
                self._is_anim_controller(obj=ctrl_data.obj),
                self._cmds.exists(
                    obj=ctrl_data.obj,
                    attr=extra_attr.ExtraAttr.SPRT_IDX)])
        }

    def _get_world_transforms(self, face_data):
        return {
            ctrl_data.obj: self._cmds.get_world_matrix(obj=ctrl_data.obj)
            for ctrl_data in face_data.ctrl_map.values()
            if self._is_anim_controller(obj=ctrl_data.obj)
        }

    def _flip_face(self, face_data, value):
        self._cmds.set_attribute(
            obj=face_data.root,
            attr=extra_attr.ExtraAttr.FLIP,
            value=bool(value))

    def _apply_symmetrical_change(self, obj, table, cb, accepted_objs=[]):
        if not accepted_objs or obj in accepted_objs:
            cb(obj=obj, symm_obj=table.get_symmetric(obj=obj))
        for child in self._cmds.get_children(obj=obj):
            self._apply_symmetrical_change(
                obj=child,
                table=table,
                cb=cb,
                accepted_objs=accepted_objs)

    def _apply_world_transforms(self, obj, table, mtx_map, accepted_objs=[]):

        def _apply_change(obj, symm_obj):
            if symm_obj in mtx_map:
                self._cmds.set_world_matrix(obj=obj, mtx=mtx_map[symm_obj])
                self._postprocess_sprite_transform(obj=obj)

        self._apply_symmetrical_change(
            obj=obj,
            table=table,
            cb=_apply_change,
            accepted_objs=accepted_objs)

    def _apply_sprite_indices(self, obj, table, idx_map, accepted_objs=[]):

        def _apply_change(obj, symm_obj):
            if symm_obj in idx_map:
                self._change_sprite(obj=obj, value=idx_map[symm_obj])

        self._apply_symmetrical_change(
            obj=obj,
            table=table,
            cb=_apply_change,
            accepted_objs=accepted_objs)

    @staticmethod
    def _stringify(m, name):
        return '{:s}\n\t{:s}'.format(
            name,
            '\n\t'.join([
                '{}: {}'.format(k[k.rfind('|') + 1:], v)
                for k, v in m.items()]))

    def _symmetrize(self, face_data, accepted_objs):
        if not face_data:
            return

        self._cmds.begin_undo()
        try:
            LOG.info('Mirror "{}" (starting @ {})'.format(
                face_data.asset_name,
                face_data.root))

            table = self.MirrorTable(cmds=self._cmds, face_data=face_data)
            LOG.info('Mirror table\n{:s}'.format(table))

            self._flip_face(face_data=face_data, value=True)
            mtx_map = self._get_world_transforms(face_data=face_data)
            self._flip_face(face_data=face_data, value=False)
            LOG.info(self._stringify(m=mtx_map, name='World matrices'))

            idx_map = self._get_sprite_indices(face_data=face_data)
            LOG.info(self._stringify(m=idx_map, name='Sprite indices'))

            self._apply_world_transforms(
                obj=face_data.root,
                table=table,
                mtx_map=mtx_map,
                accepted_objs=accepted_objs)
            self._apply_sprite_indices(
                obj=face_data.root,
                table=table,
                idx_map=idx_map,
                accepted_objs=accepted_objs)

        except Exception as e:
            raise error.Error('Failed to mirror "{}"\n{}'.format(
                face_data.asset_name,
                e.message))
        finally:
            self._cmds.end_undo()

    def mirror(self, face_data):
        #            _
        #  _ __ ___ (_)_ __ _ __ ___  _ __
        # | '_ ` _ \| | '__| '__/ _ \| '__|
        # | | | | | | | |  | | | (_) | |
        # |_| |_| |_|_|_|  |_|  \___/|_|

        self._symmetrize(face_data=face_data, accepted_objs=[])

    def symmetrize(self, face_data, target_side):
        #                                     _        _
        #  ___ _   _ _ __ ___  _ __ ___   ___| |_ _ __(_)_______
        # / __| | | | '_ ` _ \| '_ ` _ \ / _ \ __| '__| |_  / _ \
        # \__ \ |_| | | | | | | | | | | |  __/ |_| |  | |/ /  __/
        # |___/\__, |_| |_| |_|_| |_| |_|\___|\__|_|  |_/___\___|
        #      |___/

        accepted_objs = self._get_sided_anim_controllers(
            face_data=face_data,
            side=target_side)
        self._symmetrize(face_data=face_data, accepted_objs=accepted_objs)
        # if not face_data:
        #     return

        # self._cmds.begin_undo()
        # try:
        #     LOG.info('Symmetrize {} "{}" (starting @ {})'.format(
        #         'L->R' if target_side == FaceSide.RIGHT else 'R->L',
        #         face_data.asset_name,
        #         face_data.root))

        #     table = self.MirrorTable(cmds=self._cmds, face_data=face_data)
        #     LOG.info('Mirror table\n{:s}'.format(table))
        #     ctrls = self._get_sided_anim_controllers(
        #         face_data=face_data,
        #         side=target_side)
        #     for _ in ctrls:
        #         LOG.info('{} <- {}'.format(_, table.get_symmetric(obj=_)))
        # except Exception as e:
        #     raise error.Error('Failed to symmeterize "{}"\n{}'.format(
        #         face_data.asset_name,
        #         e.message))
        # finally:
        #     self._cmds.end_undo()
