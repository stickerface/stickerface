# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import os

from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import gui_util  # noqa I100
from stickerface import main_widget_base
from stickerface.utils import qmisc

reload(gui_util)
reload(main_widget_base)
reload(qmisc)

LOG = logging.getLogger(__name__)


#  _____     _     _             ____            _
# |  ___|__ | | __| | ___ _ __  |  _ \ ___ _ __ | | __ _  ___ ___
# | |_ / _ \| |/ _` |/ _ \ '__| | |_) / _ \ '_ \| |/ _` |/ __/ _ \
# |  _| (_) | | (_| |  __/ |    |  _ <  __/ |_) | | (_| | (_|  __/
# |_|  \___/|_|\__,_|\___|_|    |_| \_\___| .__/|_|\__,_|\___\___|
#                                         |_|

class FolderReplacingDialog(QtWidgets.QDialog):

    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags()):
        super(FolderReplacingDialog, self).__init__(parent=parent, f=f)

        self._src_folder = ''
        self._src_folder_edit = QtWidgets.QLineEdit()
        self._dst_folder = ''
        self._dst_folder_edit = QtWidgets.QLineEdit()

        main_layout = QtWidgets.QHBoxLayout()
        left_layout = QtWidgets.QVBoxLayout()
        src_folder_layout = QtWidgets.QHBoxLayout()
        dst_folder_layout = QtWidgets.QHBoxLayout()

        src_folder_icon = QtWidgets.QLabel()
        dst_folder_icon = QtWidgets.QLabel()

        src_folder_tools = QtWidgets.QToolBar()
        dst_folder_tools = QtWidgets.QToolBar()
        main_tools = QtWidgets.QToolBar()

        if parent:
            for _ in [self._src_folder_edit, self._dst_folder_edit]:
                _.setMinimumWidth(parent.LINE_EDIT_WIDTH)
            for _ in [src_folder_tools, dst_folder_tools, main_tools]:
                _.setIconSize(parent.ICON_SIZE)

        src_folder_icon.setPixmap(
            QtGui.QPixmap(gui_util.path('src_folder.png')))
        src_folder_icon.setToolTip('Folder to be replaced')
        dst_folder_icon.setPixmap(
            QtGui.QPixmap(gui_util.path('dst_folder.png')))
        dst_folder_icon.setToolTip('Replacing folder')

        src_folder_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('browse_location.png')),
            'Browse location',
            self)
        dst_folder_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('browse_location.png')),
            'Browse location',
            self)
        ok_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('exec_2.png')),
            'Remap resource paths',
            self)

        src_folder_tools.addAction(src_folder_act)
        dst_folder_tools.addAction(dst_folder_act)
        main_tools.addAction(ok_act)

        for _ in [src_folder_icon, self._src_folder_edit, src_folder_tools]:
            src_folder_layout.addWidget(_)
        src_folder_layout.addStretch(1)

        for _ in [dst_folder_icon, self._dst_folder_edit, dst_folder_tools]:
            dst_folder_layout.addWidget(_)
        dst_folder_layout.addStretch(1)

        left_layout.addLayout(src_folder_layout)
        left_layout.addLayout(dst_folder_layout)

        main_layout.addLayout(left_layout)
        main_layout.addWidget(main_tools)

        main_layout.addStretch(1)
        self.setLayout(main_layout)

        ok_act.triggered.connect(self.accept)
        src_folder_act.triggered.connect(self._browse_source_folder)
        dst_folder_act.triggered.connect(self._browse_destination_folder)

    @property
    def source_folder(self):
        return self._src_folder

    @property
    def destination_folder(self):
        return self._dst_folder

    def _browse_source_folder(self):
        path = os.path.dirname(self._src_folder) if self._src_folder else '.'
        self._src_folder = qmisc.browse_location(
            action=qmisc.BrowseAct.GET_DIR,
            caption='Browse location',
            cur_path=path,
            parent=self,
            line_edit=self._src_folder_edit)

    def _browse_destination_folder(self):
        path = os.path.dirname(self._dst_folder) if self._dst_folder else '.'
        self._dst_folder = qmisc.browse_location(
            action=qmisc.BrowseAct.GET_DIR,
            caption='Browse location',
            cur_path=path,
            parent=self,
            line_edit=self._dst_folder_edit)
