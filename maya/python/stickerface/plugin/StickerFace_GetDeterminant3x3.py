# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import sys
import traceback

from maya import OpenMaya as om
from maya import OpenMayaMPx as omMPx

# maya_useNewAPI = True  # incompatible with Maya2015 !


class GetDeterminant3x3(omMPx.MPxNode):

    NAME = 'GetDeterminant3x3'
    ID = om.MTypeId(0xf0f0f1)

    # inputs
    a_MTX = None

    # outputs
    a_OUTPUT = None

    def __init__(self):
        super(GetDeterminant3x3, self).__init__()

    def compute(self, plug, data_block):
        try:
            if (plug == self.a_OUTPUT):
                h_mtx = data_block.inputValue(
                    GetDeterminant3x3.a_MTX)

                mtx = h_mtx.asFloatMatrix()
                det = mtx.det3x3()
                # sys.stdout.write('det3x3 = {}'.format(det))

                h_out = data_block.outputValue(GetDeterminant3x3.a_OUTPUT)
                h_out.setFloat(det)
                data_block.setClean(plug)

        except Exception:
            sys.stderr.write('Error in {}.compute\n'.format(self.NAME))
            traceback.print_exc(file=sys.stderr)

    @staticmethod
    def create():
        return GetDeterminant3x3()

    @staticmethod
    def initialize():
        try:
            sys.stdout.write(
                'initialize {}\n'.format(GetDeterminant3x3.NAME))
            num_attr = om.MFnNumericAttribute()
            mtx_attr = om.MFnMatrixAttribute()

            GetDeterminant3x3.a_MTX = mtx_attr.create(
                'matrix',
                'matrix',
                om.MFnMatrixAttribute.kFloat)
            mtx_attr.setKeyable(True)
            mtx_attr.setStorable(False)
            mtx_attr.setConnectable(True)

            GetDeterminant3x3.a_OUTPUT = num_attr.create(
                'output',
                'output',
                om.MFnNumericData.kFloat,
                0.0)

            for _ in [
                GetDeterminant3x3.a_MTX,
                GetDeterminant3x3.a_OUTPUT
            ]:
                omMPx.MPxNode.addAttribute(_)
            for _ in [
                GetDeterminant3x3.a_MTX,
            ]:
                omMPx.MPxNode.attributeAffects(_, GetDeterminant3x3.a_OUTPUT)
            return True
        except Exception:
            traceback.print_exc(file=sys.stderr)


def initializePlugin(obj):
    try:
        sys.stdout.write(
            'initializing {}...\n'.format(GetDeterminant3x3.NAME))
        plugin = omMPx.MFnPlugin(obj)
        plugin.registerNode(
            GetDeterminant3x3.NAME,
            GetDeterminant3x3.ID,
            GetDeterminant3x3.create,
            GetDeterminant3x3.initialize)
    except Exception as e:
        sys.stderr.write(
            'Failed to register {}\n{}'.format(
                GetDeterminant3x3.NAME,
                e.message))


def uninitializePlugin(obj):
    try:
        plugin = omMPx.MFnPlugin(obj)
        plugin.deregisterNode(GetDeterminant3x3.ID)
        sys.stdout.write(
            'uninitialized {}\n'.format(GetDeterminant3x3.NAME))
    except Exception as e:
        sys.stderr.write(
            'Failed to deregister {}\n{}'.format(
                GetDeterminant3x3.NAME,
                e.message))
