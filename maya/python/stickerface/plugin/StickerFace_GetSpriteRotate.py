# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import math
import sys
import traceback

from maya import OpenMaya as om
from maya import OpenMayaMPx as omMPx

# maya_useNewAPI = True  # incompatible with Maya2015 !

def _read_matrix(mtx):
    '''
    Format the content of a MFloatMatrix into a list of floats.
    Highly inefficient. For debugging purpose only.
    '''
    data = []
    for i in range(4):
        _ = [0.0] * 4
        _[i] = 1.0
        row = om.MFloatPoint(_[0], _[1], _[2], _[3]) * mtx
        data.extend([row.x, row.y, row.z, row.w])
    return data


def _diff(p, q):
    return [p.x - q.x, p.y - q.y, p.z - q.z]


def _dot(v1, v2):
    return reduce(lambda x, y: x + y, [v1[i] * v2[i] for i in range(3)])


def _len(v):
    return math.sqrt(_dot(v1=v, v2=v))


def _normalize(v):
    L = _len(v=v)
    return [_ * (1.0 / L if L > 0.0 else 0.0) for _ in v]


def _normalize_vector(v):
    return _normalize(v=[v.x, v.y, v.z])


def _cross(v1, v2):
    return [
        v1[1] * v2[2] - v1[2] * v2[1],
        v1[2] * v2[0] - v1[0] * v2[2],
        v1[0] * v2[1] - v1[1] * v2[0]
    ]


def _neg(v):
    return [-_ for _ in v]


class GetSpriteRotate(omMPx.MPxNode):

    NAME = 'GetSpriteRotate'
    ID = om.MTypeId(0xf0f0f0)

    # inputs
    a_FACE_CENTER = None
    a_FACE_UP_POS = None
    a_SPRITE_WORLD_MTX = None

    # outputs
    a_ROTATE_Z = None

    def __init__(self):
        super(GetSpriteRotate, self).__init__()

    def compute(self, plug, data_block):
        try:
            if (plug == self.a_ROTATE_Z):
                h_face_center = data_block.inputValue(
                    GetSpriteRotate.a_FACE_CENTER)
                h_face_up_pos = data_block.inputValue(
                    GetSpriteRotate.a_FACE_UP_POS)
                h_sprt_mtx = data_block.inputValue(
                    GetSpriteRotate.a_SPRITE_WORLD_MTX)

                face_center = h_face_center.asFloatVector()
                face_up_pos = h_face_up_pos.asFloatVector()
                sprt_mtx = h_sprt_mtx.asFloatMatrix()

                # determine the reference frame from the face center, up
                # locators, and sprite center.
                sprt_center = om.MFloatPoint(0.0, 0.0, 0.0, 1.0) * sprt_mtx
                ref_normal = _normalize(v=_diff(p=sprt_center, q=face_center))
                ref_up = _normalize(v=_diff(p=face_up_pos, q=face_center))
                ref_side = _cross(v1=ref_up, v2=ref_normal)

                # estimate the sprite's rotation angle in the face plane
                sprt_side = _normalize_vector(
                    v=om.MFloatPoint(1.0, 0.0, 0.0, 0.0) * sprt_mtx)

                # project the rotated axis on the reference frame
                # to compute the angle
                x = _dot(v1=sprt_side, v2=ref_side)
                y = _dot(v1=sprt_side, v2=ref_up)

                if (sprt_mtx.det3x3() < 0.0):
                    x = -x

                ang_d = math.degrees(math.atan2(y, x))

                if (sprt_mtx.det3x3() < 0.0):
                    ang_d = -ang_d

                h_rotate = data_block.outputValue(GetSpriteRotate.a_ROTATE_Z)
                h_rotate.setFloat(ang_d)
                data_block.setClean(plug)

        except Exception:
            sys.stderr.write('Error in {}.compute\n'.format(self.NAME))
            traceback.print_exc(file=sys.stderr)

    @staticmethod
    def create():
        return GetSpriteRotate()

    @staticmethod
    def initialize():
        try:
            sys.stdout.write('initialize {}\n'.format(GetSpriteRotate.NAME))
            num_attr = om.MFnNumericAttribute()
            mtx_attr = om.MFnMatrixAttribute()

            GetSpriteRotate.a_FACE_CENTER = num_attr.createPoint(
                'face_center',
                'face_center')
            num_attr.setKeyable(True)
            num_attr.setStorable(False)
            num_attr.setConnectable(True)

            GetSpriteRotate.a_FACE_UP_POS = num_attr.createPoint(
                'face_up_pos',
                'face_up_pos')
            num_attr.setKeyable(True)
            num_attr.setStorable(False)
            num_attr.setConnectable(True)

            GetSpriteRotate.a_SPRITE_WORLD_MTX = mtx_attr.create(
                'sprite_world_mtx',
                'sprite_world_mtx',
                om.MFnMatrixAttribute.kFloat)
            mtx_attr.setKeyable(True)
            mtx_attr.setStorable(False)
            mtx_attr.setConnectable(True)

            GetSpriteRotate.a_ROTATE_Z = num_attr.create(
                'rotate_z',
                'rotate_z',
                om.MFnNumericData.kFloat,
                0.0)

            for _ in [
                GetSpriteRotate.a_FACE_CENTER,
                GetSpriteRotate.a_FACE_UP_POS,
                GetSpriteRotate.a_SPRITE_WORLD_MTX,
                GetSpriteRotate.a_ROTATE_Z
            ]:
                omMPx.MPxNode.addAttribute(_)
            for _ in [
                GetSpriteRotate.a_FACE_CENTER,
                GetSpriteRotate.a_FACE_UP_POS,
                GetSpriteRotate.a_SPRITE_WORLD_MTX,
            ]:
                omMPx.MPxNode.attributeAffects(_, GetSpriteRotate.a_ROTATE_Z)
            return True
        except Exception:
            traceback.print_exc(file=sys.stderr)


def initializePlugin(obj):
    try:
        sys.stdout.write('initializing {}...\n'.format(GetSpriteRotate.NAME))
        plugin = omMPx.MFnPlugin(obj)
        plugin.registerNode(
            GetSpriteRotate.NAME,
            GetSpriteRotate.ID,
            GetSpriteRotate.create,
            GetSpriteRotate.initialize)
    except Exception as e:
        sys.stderr.write(
            'Failed to register {}\n{}'.format(
                GetSpriteRotate.NAME,
                e.message))


def uninitializePlugin(obj):
    try:
        plugin = omMPx.MFnPlugin(obj)
        plugin.deregisterNode(GetSpriteRotate.ID)
        sys.stdout.write('uninitialized {}\n'.format(GetSpriteRotate.NAME))
    except Exception as e:
        sys.stderr.write(
            'Failed to deregister {}\n{}'.format(
                GetSpriteRotate.NAME,
                e.message))
