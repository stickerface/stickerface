# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging

from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import ctrl_flag  # noqa I100
from stickerface import ctrl_slideshow
from stickerface import extra_attr
from stickerface import picker_data

reload(picker_data)
reload(ctrl_flag)
reload(ctrl_slideshow)
reload(extra_attr)

LOG = logging.getLogger(__name__)


#  ___ _                   ____       _                  _
# |_ _| |_ ___ _ __ ___   |  _ \  ___| | ___  __ _  __ _| |_ ___
#  | || __/ _ \ '_ ` _ \  | | | |/ _ \ |/ _ \/ _` |/ _` | __/ _ \
#  | || ||  __/ | | | | | | |_| |  __/ |  __/ (_| | (_| | ||  __/
# |___|\__\___|_| |_| |_| |____/ \___|_|\___|\__, |\__,_|\__\___|
#                                            |___/

class _ListItemDelegate(QtWidgets.QStyledItemDelegate):

    def __init__(self, parent, cmds):
        super(_ListItemDelegate, self).__init__(parent)
        self._cmds = cmds
        self._obj = ''

    def set_controller(self, obj):
        self._obj = obj

    def paint(self, painter, option, model_index):
        option.state = option.state & ~QtWidgets.QStyle.State_Selected
        super(_ListItemDelegate, self).paint(painter, option, model_index)

        attr = extra_attr.ExtraAttr.SPRT_IDX
        cur_index = self._cmds.get_attribute(obj=self._obj, attr=attr)
        if cur_index is None:
            return

        item = self.parent().item(model_index.row())
        item_index = item.data(QtCore.Qt.UserRole)
        if cur_index == item_index:
            colorname = 'lightgreen'
            key = self._cmds.eval_key_at_current_time(obj=self._obj, attr=attr)
            if key is not None:
                colorname = 'crimson'
            pen = QtGui.QPen(QtGui.QColor(colorname))
            pen.setWidth(1)
            painter.setPen(pen)
            painter.setBrush(QtCore.Qt.NoBrush)
            painter.drawRect(option.rect.adjusted(1, 1, -1, -1).normalized())


#   ____ _        _   ____                 _
#  / ___| |_ _ __| | |  _ \ _ __ _____   _(_) _____      __
# | |   | __| '__| | | |_) | '__/ _ \ \ / / |/ _ \ \ /\ / /
# | |___| |_| |  | | |  __/| | |  __/\ V /| |  __/\ V  V /
#  \____|\__|_|  |_| |_|   |_|  \___| \_/ |_|\___| \_/\_/

class ControllerPreviewWidget(QtWidgets.QWidget):
    COLORS = {
        ctrl_flag.CtrlFlag.CENTRAL: 'gold',
        ctrl_flag.CtrlFlag.LEFT: 'deepskyblue',
        ctrl_flag.CtrlFlag.RIGHT: 'tomato',
    }

    def __init__(self, cmds, icon_cache, parent=None, f=QtCore.Qt.WindowFlags()):
        super(ControllerPreviewWidget, self).__init__(parent=parent, f=f)
        self._cmds = cmds
        self._obj = ''
        self._attr = ''
        self._groupbox = QtWidgets.QGroupBox()
        self._groupbox.setStyle(QtWidgets.QStyleFactory.create('plastique'))

        self._slideshow = ctrl_slideshow.ControllerSlideshow(
            icon_cache=icon_cache)
        self._item_delegate = _ListItemDelegate(
            parent=self._slideshow,
            cmds=self._cmds)

        main_layout = QtWidgets.QVBoxLayout()
        grp_layout = QtWidgets.QVBoxLayout()

        self._slideshow.setItemDelegate(self._item_delegate)
        grp_layout.addWidget(self._slideshow)
        grp_layout.addStretch(1)
        self._groupbox.setLayout(grp_layout)

        main_layout.addWidget(self._groupbox)
        main_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(main_layout)

        self._slideshow.currentItemChanged.connect(
            self._handle_slideshow_item_changed)

    def _update_style(self, ctrl_data):
        colorname = 'grey'
        for k, v in self.COLORS.items():
            if (ctrl_data.flags & k) != 0:
                colorname = v

        self._groupbox.setStyleSheet('''
            QGroupBox {{
               background: none;
               border: 1px solid {};
               border-radius: 4px;
               margin-top: 8px;
            }}
            QGroupBox::title {{
                subcontrol-origin: margin;
                color: {};
                left: 8px;
            }}'''.format(colorname, colorname))

    def configure(self, face_data, ctrl_data):
        self._obj = ctrl_data.obj
        self._attr = ctrl_data.attr
        self._groupbox.setTitle(ctrl_data.sprite_name)
        self._item_delegate.set_controller(obj=self._obj)
        self._update_style(ctrl_data=ctrl_data)

        total_sprites = face_data.atlas_grid[0] * face_data.atlas_grid[1]
        start_idx = max(0, min(total_sprites - 1, ctrl_data.index_offset))
        end_idx = \
            min(total_sprites - 1, start_idx + ctrl_data.index_range - 1) \
            if ctrl_data.index_range > 0 \
            else total_sprites - 1

        LOG.info(
            'show sprites for "{}": {} -> {} from "{}"'.format(
                ctrl_data.obj,
                start_idx,
                end_idx,
                face_data.atlas_path))

        self._slideshow.show_sprites(
            atlas_path=face_data.atlas_path,
            start_index=start_idx,
            end_index=end_idx)

    def _handle_slideshow_item_changed(self, cur_item, prv_item):
        cur_idx = cur_item.data(QtCore.Qt.UserRole) if cur_item else 0
        # change sprite
        self._cmds.set_attribute(obj=self._obj, attr=self._attr, value=cur_idx)
        self._cmds.force_step_key_tangents(obj=self._obj, attr=self._attr)

    def mouseReleaseEvent(self, event):
        self._cmds.set_selection(objs=[self._obj])
        super(ControllerPreviewWidget, self).mouseReleaseEvent(event)
