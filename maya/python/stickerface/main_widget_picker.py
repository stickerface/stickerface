# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging


from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import ctrl_flag  # noqa I100
from stickerface import ctrl_preview
from stickerface import extra_attr
from stickerface import face_edit
from stickerface import gui_util
from stickerface import main_widget_base
from stickerface import picker_data
from stickerface import picker_icon

reload(gui_util)
reload(picker_data)
reload(picker_icon)
reload(extra_attr)
reload(main_widget_base)
reload(ctrl_preview)
reload(ctrl_flag)
reload(face_edit)

LOG = logging.getLogger(__name__)


#  ____  _      _              __        ___     _            _
# |  _ \(_) ___| | _____ _ __  \ \      / (_) __| | __ _  ___| |_
# | |_) | |/ __| |/ / _ \ '__|  \ \ /\ / /| |/ _` |/ _` |/ _ \ __|
# |  __/| | (__|   <  __/ |      \ V  V / | | (_| | (_| |  __/ |_
# |_|   |_|\___|_|\_\___|_|       \_/\_/  |_|\__,_|\__, |\___|\__|
#                                                  |___/

class MainWidget(main_widget_base.MainWidgetBase):
    # ordered sequence used as hint to order sprite widgets in grid layout
    SPRT_SEQUENCE = [
        'brow', 'eyeball', 'upperEyelid', 'pupil', 'lowerEyelid', 'mouth'
    ]

    def __init__(self, cmds, parent=None, f=QtCore.Qt.WindowFlags()):
        super(MainWidget, self).__init__(cmds=cmds, parent=parent, f=f)
        self._finalize()

    def _finalize(self):
        plt = self.palette()

        self._face_map = {}  # full path -> face data
        self._grid_layout = QtWidgets.QGridLayout()
        self._icon_cache = picker_icon.Cache(
            shadow_color=plt.color(
                QtGui.QPalette.Normal,
                QtGui.QPalette.HighlightedText))

        main_layout = QtWidgets.QVBoxLayout()
        scroll = QtWidgets.QScrollArea()
        in_scroll_widget = QtWidgets.QWidget()

        header = self._create_header()

        in_scroll_widget.setLayout(self._grid_layout)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setStyleSheet('''
            QWidget {
                background: rgba(0, 0, 0, 0);
            }''')
        scroll.setWidget(in_scroll_widget)

        main_layout.addWidget(header)
        main_layout.addWidget(scroll)
        # main_layout.addStretch(1)
        main_layout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(main_layout)
        self._reload()

        dispatcher = QtCore.QAbstractEventDispatcher.instance()
        dispatcher.awake.connect(self._on_idle)
        dispatcher.aboutToBlock.connect(self._on_idle)

    def _create_header(self):
        self._face_cbox = QtWidgets.QComboBox()

        frame = QtWidgets.QFrame()
        layout = QtWidgets.QHBoxLayout()
        tools_left = QtWidgets.QToolBar()
        tools_right = QtWidgets.QToolBar()
        reload_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('reload.png')),
            'Reload',
            self)
        reset_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('reset.png')),
            'Reset',
            self)
        mirror_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('mirror.png')),
            'Mirror',
            self)
        symm_L_to_R_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('symm_L_to_R.png')),
            'Match right side to left side',
            self)
        symm_R_to_L_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('symm_R_to_L.png')),
            'Match left side to right side',
            self)

        frame.setStyleSheet('''
            QWidget {
                background: palette(dark);
                color: palette(windowtext);
                font-size: 16px;
            }''')

        self._face_cbox.setMinimumContentsLength(1)
        self._face_cbox.setSizeAdjustPolicy(
            QtWidgets.QComboBox.AdjustToContents)
        self._face_cbox.setMinimumWidth(150)

        for _ in [tools_left, tools_right]:
            _.setIconSize(self.ICON_SIZE)
        tools_left.addAction(reload_act)
        for _ in [reset_act, mirror_act, symm_R_to_L_act, symm_L_to_R_act]:
            tools_right.addAction(_)

        layout.addWidget(QtWidgets.QLabel('Character'))
        layout.addWidget(self._face_cbox)
        layout.addWidget(tools_left)
        layout.addStretch(1)
        layout.addWidget(tools_right)
        layout.setContentsMargins(4, 4, 4, 4)
        frame.setLayout(layout)

        reload_act.triggered.connect(
            self._reload)
        reset_act.triggered.connect(
            self._reset_expression)
        mirror_act.triggered.connect(
            self._mirror_expression)
        symm_L_to_R_act.triggered.connect(
            self._symmetrize_expression_from_left_to_right)
        symm_R_to_L_act.triggered.connect(
            self._symmetrize_expression_from_right_to_left)

        return frame

    def _clear(self):
        try:
            self._face_cbox.currentIndexChanged.disconnect(
                self._handle_char_cbox_index_changed)
        except RuntimeError:
            pass
        self.clear_layout(layout=self._grid_layout)
        self._face_cbox.clear()
        self._face_map = {}
        self._icon_cache.clear()

    def _reload(self):

        def _get_label(data):
            label = data.asset_name
            if data.namespaces:
                label = label + ' [' + ':'.join(data.namespaces) + ']'
            return label

        LOG.info('\n{}\nPicker content is reset.'.format('#' * 79))
        self._clear()

        self._face_map = {
            obj: self._create_face_data(root=obj)
            for obj in self._helper.get_faces()
        }
        LOG.info('\n'.join([str(_) for _ in self._face_map.values()]))

        # fill the combo box storing all faces present in the scene
        labels = [
            (_get_label(data=v), k) for k, v in self._face_map.items()
        ]
        labels.sort(key=lambda x: (x[0].lower(), 0))
        for i, (label, key) in enumerate(labels):
            self._face_cbox.insertItem(i, label, userData=key)

        self._face_cbox.currentIndexChanged.connect(
            self._handle_char_cbox_index_changed)

        self._display_face(
            face_key=self._get_face_key_from_index(
                self._face_cbox.currentIndex()))

    def _create_face_data(self, root):
        p = root
        if p.startswith('|'):
            p = p[1:]
        ns = p.split(':')[:-1]

        name = self.cmds.get_attribute(
            obj=root,
            attr=extra_attr.ExtraAttr.ASSET_NAME)
        if name is None or not name:
            name = self.cmds.get_short_name(obj=root)
            name = name.split(':')[-1]

        ctrl_map = {
            obj: self._create_controller_data(obj=obj, root=root)
            for obj in self._helper.get_controllers(top=root)
        }
        # filter out unusable controller entries
        # ctrl_map = {k: v for k, v in ctrl_map.items() if v.attr}

        mat = self._helper.get_material(root=root)

        data = picker_data.FaceData()
        data.asset_name = name
        data.namespaces = ns
        data.root = root
        data.ctrl_map = ctrl_map
        data.atlas_path = self._helper.get_atlas_path(mat=mat)
        data.atlas_grid = self._helper.get_atlas_grid_resolution(mat=mat)

        self._icon_cache.declare_atlas(
            path=data.atlas_path,
            grid_resolution=data.atlas_grid)

        return data

    def _create_controller_data(self, obj, root):
        name = self.cmds.get_attribute(
            obj=obj,
            attr=extra_attr.ExtraAttr.SPRT_NAME)
        if name is None or not name:
            name = self.cmds.get_short_name(obj=obj)
            name = name.split(':')[-1]

        flags = self.cmds.get_attribute(
            obj=obj,
            attr=extra_attr.ExtraAttr.CTRL_FLAGS)
        flags = flags if flags is not None else 0

        attr = \
            extra_attr.ExtraAttr.SPRT_IDX \
            if self.cmds.exists(obj=obj, attr=extra_attr.ExtraAttr.SPRT_IDX) \
            else ''  # will be filtered out

        idx_range = 0
        if attr:
            min_v, max_v = self.cmds.get_attribute_range(obj=obj, attr=attr)
            if max_v is not None:
                LOG.info('{}.{} max = {}'.format(obj, attr, max_v))
                idx_range = max_v

        idx_offset = self.cmds.get_attribute(
            obj=root,
            attr=extra_attr.ExtraAttr.index_offset(sprt=name))
        if idx_offset is None:
            idx_offset = 0

        data = picker_data.ControllerData()
        data.sprite_name = name
        data.flags = flags
        data.obj = obj
        data.attr = attr
        data.index_offset = idx_offset
        data.index_range = idx_range
        return data

    def _handle_char_cbox_index_changed(self, index):
        self._display_face(self._get_face_key_from_index(index=index))

    def _get_current_face_data(self):
        return self._face_map.get(
            self._face_cbox.itemData(
                self._face_cbox.currentIndex(),
                QtCore.Qt.UserRole),
            None)

    def _get_face_key_from_index(self, index):
        return self._face_cbox.itemData(index, QtCore.Qt.UserRole)

    def _order_controllers(self, ctrl_map):
        def _get_order_from_sequence(ctrl_name):
            for i, _ in enumerate(self.SPRT_SEQUENCE):
                if _ in ctrl_name:
                    return i
            return -1

        ret = {}
        num_extras = 0
        for k, v in ctrl_map.items():
            row = _get_order_from_sequence(ctrl_name=v.sprite_name)
            if row == -1:
                row = len(self.SPRT_SEQUENCE) + num_extras
                num_extras = num_extras + 1
            col = 0 \
                if v.flags & ctrl_flag.CtrlFlag.RIGHT \
                else 1 if v.flags & ctrl_flag.CtrlFlag.LEFT else -1
            ret[k] = (row, col)
        return ret

    def _display_face(self, face_key):
        MainWidget.clear_layout(layout=self._grid_layout)
        LOG.info('Display face "{}"'.format(face_key))

        if face_key not in self._face_map:
            return
        face_data = self._face_map[face_key]
        coords_map = self._order_controllers(ctrl_map=face_data.ctrl_map)
        for ctrl_key, ctrl_data in face_data.ctrl_map.items():
            if any([
                not ctrl_data.attr,  # anim controller not associated w/ sprite
                ctrl_key not in coords_map,
            ]):
                continue
            coords = coords_map[ctrl_key]
            widget = ctrl_preview.ControllerPreviewWidget(
                cmds=self.cmds,
                icon_cache=self._icon_cache)
            widget.configure(face_data=face_data, ctrl_data=ctrl_data)

            if coords[1] == -1:
                self._grid_layout.addWidget(widget, coords[0], 0, 1, 2)
            else:
                self._grid_layout.addWidget(widget, coords[0], coords[1])

    def _on_idle(self):
        self._icon_cache.process_requests(num_requests=3)

    def _reset_expression(self):
        face_edit.FaceEdit(cmds=self.cmds).reset(
            face_data=self._get_current_face_data())

    def _mirror_expression(self):
        face_edit.FaceEdit(cmds=self.cmds).mirror(
            face_data=self._get_current_face_data())

    def _symmetrize_expression_from_left_to_right(self):
        face_edit.FaceEdit(cmds=self.cmds).symmetrize(
            face_data=self._get_current_face_data(),
            target_side=face_edit.FaceSide.RIGHT)

    def _symmetrize_expression_from_right_to_left(self):
        face_edit.FaceEdit(cmds=self.cmds).symmetrize(
            face_data=self._get_current_face_data(),
            target_side=face_edit.FaceSide.LEFT)
