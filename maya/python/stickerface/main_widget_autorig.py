# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging

from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import atlas_preview  # noqa I100
from stickerface import attr_choicer
from stickerface import attr_type
from stickerface import extra_attr
from stickerface import facial_autorig
from stickerface import facial_helper
from stickerface import gui_util
from stickerface import main_widget_base
from stickerface.cgfx import shader_proj
from stickerface.utils import error
from stickerface.utils import qmisc

reload(shader_proj)
reload(error)
reload(qmisc)
reload(attr_type)
reload(attr_choicer)
reload(gui_util)
reload(facial_autorig)
reload(facial_helper)
reload(extra_attr)
reload(main_widget_base)
reload(atlas_preview)

LOG = logging.getLogger(__name__)


#     _         _             _        __        ___     _            _
#    / \  _   _| |_ ___  _ __(_) __ _  \ \      / (_) __| | __ _  ___| |_
#   / _ \| | | | __/ _ \| '__| |/ _` |  \ \ /\ / /| |/ _` |/ _` |/ _ \ __|
#  / ___ \ |_| | || (_) | |  | | (_| |   \ V  V / | | (_| | (_| |  __/ |_
# /_/   \_\__,_|\__\___/|_|  |_|\__, |    \_/\_/  |_|\__,_|\__, |\___|\__|
#                               |___/                      |___/

class MainWidget(main_widget_base.MainWidgetBase):

    @staticmethod
    def _get_margins_for_layout_in_box():
        return QtCore.QMargins(5, 0, 5, 5)

    def __init__(self, cmds, parent=None, f=QtCore.Qt.WindowFlags()):
        super(MainWidget, self).__init__(cmds=cmds, parent=parent, f=f)
        self._finalize()

    def _finalize(self):
        self._asset = ''
        self._face = ''
        self._shader_path = ''
        self._body_texture_path = ''
        self._sprite_atlas_path = ''

        self._parent = ''
        self._scale_attr = ()  # (object fullname, attribute name)

        asset_group = self._create_asset_group()
        shape_group = self._create_shape_group()
        shader_group = self._create_shader_group()
        texture_group = self._create_texture_group()
        opts_group = self._create_options_group()
        tools = QtWidgets.QToolBar()
        exec_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('right.png')),
            'Next',
            self)
        exec_layout = QtWidgets.QHBoxLayout()

        tools.setIconSize(self.ICON_SIZE / 2)
        tools.addAction(exec_act)
        exec_layout.addStretch(1)
        exec_layout.addWidget(tools)

        main_layout = QtWidgets.QVBoxLayout()
        main_layout.addWidget(asset_group)
        main_layout.addWidget(shape_group)
        main_layout.addWidget(shader_group)
        main_layout.addWidget(texture_group)
        main_layout.addWidget(opts_group)
        main_layout.addLayout(exec_layout)
        main_layout.addStretch(1)
        self.setLayout(main_layout)

        exec_act.triggered.connect(
            self._handle_exec_triggered)

    def _create_asset_group(self):

        def _get_asset_name_conflicts(name):
            cur_faces = self._helper.get_faces()
            attr = extra_attr.ExtraAttr.ASSET_NAME
            return [
                _
                for _ in cur_faces
                if self.cmds.get_attribute(obj=_, attr=attr) == name
            ]

        def _get_unique_asset_name():
            incr = 0
            name = self.cmds.get_asset_name()
            while True:
                num_conflicts = len(_get_asset_name_conflicts(name=name))
                if num_conflicts == 0:
                    return name
                else:
                    incr = incr + 1
                    name = '{:s}{:d}'.format(self.cmds.get_asset_name(), incr)
            return name

        self._asset = _get_unique_asset_name()

        group = QtWidgets.QGroupBox('Asset')
        group_layout = QtWidgets.QHBoxLayout()
        pix_label = QtWidgets.QLabel()

        pix_label.setPixmap(QtGui.QPixmap(gui_util.path(
            'asset.png' if self._asset else 'warning.png')))
        group_layout.addWidget(pix_label)
        if self._asset:
            group_layout.addWidget(QtWidgets.QLabel('\'' + self._asset + '\''))
        else:
            label = QtWidgets.QLabel('Please save your scene and reload')
            font = label.font()
            font.setBold(True)
            label.setFont(font)
            label.setStyleSheet('''
                QLabel
                {
                    margin-left: 3px;
                    margin-right: 3px;
                    background: palette(Text);
                    color: palette(Window);
                    border-radius: 3px;
                }''')
            group_layout.addWidget(label)

        group_layout.addStretch(1)
        group_layout.setContentsMargins(self._get_margins_for_layout_in_box())
        group.setLayout(group_layout)

        return group

    def _create_shape_group(self):
        self._face_edit = QtWidgets.QLineEdit()

        group = QtWidgets.QGroupBox('Face Shape')
        label = QtWidgets.QLabel()
        tools = QtWidgets.QToolBar()
        select_shape_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('select_shape.png')),
            'Pick shape from selection',
            self)

        label.setPixmap(QtGui.QPixmap(gui_util.path('face_shape.png')))
        label.setToolTip('Face mesh')
        tools.setIconSize(self.ICON_SIZE)
        tools.addAction(select_shape_act)
        self._face_edit.setReadOnly(True)

        group_layout = QtWidgets.QHBoxLayout()
        group_layout.addWidget(label)
        group_layout.addWidget(self._face_edit)
        group_layout.addWidget(tools)
        group_layout.setContentsMargins(self._get_margins_for_layout_in_box())
        group.setLayout(group_layout)

        select_shape_act.triggered.connect(
            self._handle_select_shape_triggered)

        return group

    def _create_shader_group(self):
        self._shader_edit = QtWidgets.QLineEdit()
        self._shader_proj_group = self._create_shader_proj_group()

        group = QtWidgets.QGroupBox('Hardware Shader')
        tools = QtWidgets.QToolBar()
        browse_loc_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('save.png')),
            'Browse location',
            self)

        tools.setIconSize(self.ICON_SIZE)
        tools.addAction(browse_loc_act)
        self._shader_edit.setReadOnly(True)
        self._shader_edit.setMinimumWidth(
            self.LINE_EDIT_WIDTH - 3 * self.ICON_SIZE.width())

        layout = QtWidgets.QHBoxLayout()
        for _ in self._shader_proj_group.buttons():
            layout.addWidget(_)
        layout.addWidget(self._shader_edit)
        layout.addWidget(tools)

        layout.setContentsMargins(self._get_margins_for_layout_in_box())
        group.setLayout(layout)

        browse_loc_act.triggered.connect(
            self._handle_browse_shader_triggered)

        return group

    def _create_shader_proj_group(self):

        group = QtWidgets.QButtonGroup()
        for proj in [
            shader_proj.ShaderProj.SPHERICAL,
            shader_proj.ShaderProj.CUBIC,
            shader_proj.ShaderProj.PLANAR,
        ]:
            button = QtWidgets.QToolButton()
            button.setToolTip(
                shader_proj.DESCR.get(proj, ''))
            button.setIcon(QtGui.QIcon(gui_util.path(
                shader_proj.ICON_FILE.get(proj, 'question.png'))))
            button.setIconSize(QtCore.QSize(36, 36))
            button.setCheckable(True)

            group.addButton(button, proj)

        group.setExclusive(True)
        if group.buttons():
            group.buttons()[0].setChecked(True)
        self._shader_proj = group.checkedId()
        return group

    def _create_texture_group(self):
        group = QtWidgets.QGroupBox('Images')
        layout = QtWidgets.QVBoxLayout()

        for widget in [
            self._create_sprite_atlas_widget(),
            self._create_body_texture_widget()
        ]:
            layout.addWidget(widget)
            widget.setContentsMargins(0, 0, 0, 0)
            widget.setStyleSheet('QWidget { padding: 0; } ')
        layout.addStretch(1)

        layout.setSpacing(0)
        layout.setContentsMargins(self._get_margins_for_layout_in_box())
        group.setLayout(layout)
        return group

    def _create_sprite_atlas_widget(self):
        self._sprite_altas_edit = QtWidgets.QLineEdit()

        widget = QtWidgets.QWidget()
        label = QtWidgets.QLabel()
        tools = QtWidgets.QToolBar()
        act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('browse_location.png')),
            'Browse location',
            self)
        layout = QtWidgets.QHBoxLayout()

        label.setToolTip('Sprite atlas')
        label.setPixmap(QtGui.QPixmap(gui_util.path('sprite_sheet.png')))
        tools.setIconSize(self.ICON_SIZE)
        tools.addAction(act)
        self._sprite_altas_edit.setReadOnly(True)
        self._sprite_altas_edit.setMinimumWidth(self.LINE_EDIT_WIDTH)

        layout.addWidget(label)
        layout.addWidget(self._sprite_altas_edit)
        layout.addWidget(tools)

        layout.setContentsMargins(0, 0, 0, 0)
        widget.setLayout(layout)

        act.triggered.connect(
            self._handle_browse_atlas_triggered)

        return widget

    def _create_body_texture_widget(self):
        self._body_texture_edit = QtWidgets.QLineEdit()

        widget = QtWidgets.QWidget()
        label = QtWidgets.QLabel()
        tools = QtWidgets.QToolBar()
        act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('browse_location.png')),
            'Browse location',
            self)
        layout = QtWidgets.QHBoxLayout()

        label.setToolTip('Body texture')
        label.setPixmap(QtGui.QPixmap(gui_util.path('body_texture.png')))
        tools.setIconSize(self.ICON_SIZE)
        tools.addAction(act)
        self._body_texture_edit.setReadOnly(True)
        self._body_texture_edit.setMinimumWidth(self.LINE_EDIT_WIDTH)

        layout.addWidget(label)
        layout.addWidget(self._body_texture_edit)
        layout.addWidget(tools)

        layout.setContentsMargins(0, 0, 0, 0)
        widget.setLayout(layout)

        act.triggered.connect(
            self._handle_browse_body_triggered)

        return widget

    def _create_options_group(self):
        group = QtWidgets.QGroupBox('Optional Controls')
        layout = QtWidgets.QVBoxLayout()

        for widget in [
            self._create_parent_cstr_widget(),
            self._create_scaling_widget()
        ]:
            layout.addWidget(widget)
            widget.setContentsMargins(0, 0, 0, 0)
            widget.setStyleSheet('QWidget { padding: 0; } ')
        layout.addStretch(1)

        layout.setSpacing(0)
        layout.setContentsMargins(self._get_margins_for_layout_in_box())
        group.setLayout(layout)

        return group

    def _create_parent_cstr_widget(self):
        self._parent_edit = QtWidgets.QLineEdit()

        ret = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel()
        label.setPixmap(QtGui.QPixmap(gui_util.path('parent_constraint.png')))
        label.setToolTip('Parent constraint')
        tools = QtWidgets.QToolBar()
        select_parent_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('select_obj.png')),
            'Select object',
            self)

        tools.setIconSize(self.ICON_SIZE)
        tools.addAction(select_parent_act)
        self._parent_edit.setReadOnly(True)

        layout.addWidget(label)
        layout.addWidget(self._parent_edit)
        layout.addWidget(tools)
        layout.setContentsMargins(0, 0, 0, 0)
        ret.setLayout(layout)

        select_parent_act.triggered.connect(
            self._handle_select_parent_triggered)

        return ret

    def _create_scaling_widget(self):
        self._scale_edit = QtWidgets.QLineEdit()

        ret = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel()
        label.setPixmap(QtGui.QPixmap(gui_util.path('scaling.png')))
        label.setToolTip('Global scaling')
        tools = QtWidgets.QToolBar()
        select_scale_act = QtWidgets.QAction(
            QtGui.QIcon(gui_util.path('select_attr.png')),
            'Select attribute',
            self)

        tools.setIconSize(self.ICON_SIZE)
        tools.addAction(select_scale_act)
        self._scale_edit.setReadOnly(True)

        layout.addWidget(label)
        layout.addWidget(self._scale_edit)
        layout.addWidget(tools)
        layout.setContentsMargins(0, 0, 0, 0)
        ret.setLayout(layout)

        select_scale_act.triggered.connect(
            self._handle_select_scale_triggered)

        return ret

    def _handle_select_shape_triggered(self):
        selected = self.cmds.get_selected_shapes()
        self._face = selected[0] if selected else ''
        self._face_edit.setText(
            self.cmds.get_short_name(obj=self._face))

    def _handle_browse_shader_triggered(self):
        self._shader_path = qmisc.browse_location(
            action=qmisc.BrowseAct.SAVE_FILE,
            parent=self,
            cur_path=self._shader_path,
            line_edit=self._shader_edit,
            caption='Save shader at',
            filtr='shaders (*.cgfx)')

    def _handle_browse_body_triggered(self):
        self._body_texture_path = qmisc.browse_location(
            action=qmisc.BrowseAct.OPEN_FILE,
            parent=self,
            cur_path=self._body_texture_path,
            line_edit=self._body_texture_edit,
            caption='Find body texture',
            filtr='textures (*.jpg *.png *.exr *.tiff *.tif)')

    def _handle_browse_atlas_triggered(self):
        self._sprite_atlas_path = qmisc.browse_location(
            action=qmisc.BrowseAct.OPEN_FILE,
            parent=self,
            cur_path=self._sprite_atlas_path,
            line_edit=self._sprite_altas_edit,
            caption='Find sprite atlas',
            filtr='textures (*.png)')
        LOG.info('atlas = {}'.format(self._sprite_atlas_path))

    def _handle_select_parent_triggered(self):
        selected = self.cmds.get_selection()
        self._parent = selected[0] if selected else ''
        self._parent_edit.setText(
            self.cmds.get_short_name(obj=self._parent))

    def _handle_select_scale_triggered(self):
        self._scale_attr = ()
        self._scale_edit.setText('')

        attrs = []
        selected = self.cmds.get_selection()
        obj = selected[0] if selected else ''
        attrs = self.cmds.get_attributes(
            obj=obj,
            typ=attr_type.AttrType.NUMBER)
        if attrs:
            choicer = attr_choicer.AttributeChoicer(
                obj=obj,
                attrs=attrs,
                parent=self,
                f=QtCore.Qt.Popup)
            choicer.exec_()
            self._scale_attr = choicer.choice
            if self._scale_attr:
                self._scale_edit.setText('{:s}.{:s}'.format(
                    self.cmds.get_short_name(obj=self._scale_attr[0]),
                    self._scale_attr[1]))

    def _handle_exec_triggered(self):
        proj = \
            self._shader_proj_group.checkedId() \
            if self._shader_proj_group \
            else shader_proj.ShaderProj.UNSET
        if proj == shader_proj.ShaderProj.UNSET:
            return

        # default values
        num_sprite_cols = 10
        num_sprite_rows = 10
        sprite_indices = {}
        try:
            dialog = atlas_preview.AtlasPreviewDialog(
                cmds=self.cmds,
                shader_proj=proj,
                atlas_path=self._sprite_atlas_path,
                num_sprite_cols=num_sprite_cols,
                num_sprite_rows=num_sprite_rows)
        except error.Error as e:
            LOG.warning(e.message)
        else:
            status = dialog.exec_()
            LOG.info('-> atlas preview dialog code = {}'.format(status))
            if status != QtWidgets.QDialog.Accepted:
                return
            num_sprite_cols = dialog.num_sprite_columns
            num_sprite_rows = dialog.num_sprite_rows
            sprite_indices = dialog.sprite_indices

        LOG.info(
            'sprite atlas info\n'
            '\t- {} x {}\n'
            '\t- offsets = {}\n'
            '\t- ranges = {}\n'
            '\t- defaults = {}'.format(
                num_sprite_cols,
                num_sprite_rows,
                {k: v[0] for k, v in sprite_indices.items()},
                {k: v[1] for k, v in sprite_indices.items()},
                {k: v[2] for k, v in sprite_indices.items()}))

        facial_autorig.FacialAutorig(helper=self._helper).create_rig(
            face_shape=self._face,
            shader_proj=proj,
            shader_path=self._shader_path,
            parent_source=self._parent,
            scale_attr=self._scale_attr,
            body_texture_path=self._body_texture_path,
            sprite_atlas_path=self._sprite_atlas_path,
            num_sprite_cols=num_sprite_cols,
            num_sprite_rows=num_sprite_rows,
            sprite_indices=sprite_indices,
            asset_name=self._asset)

        self.finished.emit()
