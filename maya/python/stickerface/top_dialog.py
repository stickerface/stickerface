# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging
import os
import re

from stickerface.Qt import QtCore
from stickerface.Qt import QtWidgets

LOG = logging.getLogger(__name__)


class TopDialog(QtWidgets.QDialog):

    def __init__(self, main_widget, parent=None):

        TopDialog._cleanup(parent=parent)

        super(TopDialog, self).__init__(parent=parent)

        self.setObjectName(TopDialog._name())
        self.setWindowFlags(QtCore.Qt.Window)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        LOG.info(
            'Created dialog:\n\t- ID = "{}"\n\t- parent = {}'.format(
                self.objectName(),
                self.parent()))

        main_layout = QtWidgets.QVBoxLayout()
        main_layout.addWidget(main_widget)
        main_layout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(main_layout)
        self.setStyleSheet('''
            QWidget {
                font-size: 12px;
            }''')
        self.adjustSize()

    def closeEvent(self, evt):
        TopDialog._cleanup(parent=self.parent())

    @staticmethod
    def _name():
        return re.sub(
            pattern='[^0-9a-zA-Z]+',
            repl='_',
            string=os.path.splitext(__file__)[0])

    @staticmethod
    def _cleanup(parent):
        if parent is None:
            return
        name = TopDialog._name()
        for obj in parent.children():
            try:
                obj_name = obj.objectName()
            except Exception:
                continue
            if all([
                obj.__class__.__name__ == TopDialog.__name__,
                obj_name == name
            ]):
                LOG.info('Deleting instance {}'.format(obj))
                obj.close()
                obj.setParent(None)
                obj.deleteLater()
