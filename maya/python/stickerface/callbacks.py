# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
from stickerface import attr_type

reload(attr_type)


#   ____                                          _
#  / ___|___  _ __ ___  _ __ ___   __ _ _ __   __| |___
# | |   / _ \| '_ ` _ \| '_ ` _ \ / _` | '_ \ / _` / __|
# | |__| (_) | | | | | | | | | | | (_| | | | | (_| \__ \
#  \____\___/|_| |_| |_|_| |_| |_|\__,_|_| |_|\__,_|___/


class Callbacks(object):

    def begin_undo(self):
        raise NotImplementedError('begin_undo')

    def end_undo(self):
        raise NotImplementedError('end_undo')

    def exists(self, obj, attr=''):
        raise NotImplementedError('exists')

    def get_type(self, obj):
        raise NotImplementedError('get_type')

    def filter_objects(self, cb=None):
        raise NotImplementedError('filter_objects')

    def filter_descendents(self, obj, cb=None, keep_1st=False):
        raise NotImplementedError('filter_descendents')

    def set_attribute(
        self,
        obj,
        attr,
        value,
        as_color=False,
        lock=False,
        min_value=None,
        max_value=None
    ):
        raise NotImplementedError('set_attribute')

    def get_attribute(self, obj, attr):
        '''
        Returns: None upon failure
        '''
        raise NotImplementedError('get_attribute')

    def get_attribute_range(self, obj, attr):
        '''
        Returns: (min/None, max/None)
        '''
        raise NotImplementedError('get_attribute_range')

    def force_step_key_tangents(self, obj, attr):
        raise NotImplementedError('force_step_key_tangents')

    def get_time_range(self):
        '''
        Returns: (start, end)
        '''
        raise NotImplementedError('get_time_range')

    def get_current_time(self):
        '''
        Returns: key value at current time if any, else None.
        '''
        raise NotImplementedError('get_current_time')

    def eval_key_at_current_time(self, obj, attr):
        raise NotImplementedError('eval_key_at_current_time')

    def get_attributes(self, obj, typ=attr_type.AttrType.ALL):
        raise NotImplementedError('get_attributes')

    def get_input_connection(self, obj, attr):
        '''
        Returns: (obj, attr)
        '''
        raise NotImplementedError('get_input_connection')

    def filter_inputs(self, obj, attr, cb, keep_1st=False):
        '''
        Returns: list of input connected nodes s.t. cb(obj) = True (recursive)
        '''
        raise NotImplementedError('filter_inputs')

    def get_output_connections(self, obj, attr):
        '''
        Returns: [(obj, attr)]
        '''
        raise NotImplementedError('get_output_connections')

    def filter_outputs(self, obj, attr, cb, keep_1st=False):
        '''
        Returns: list of output connected nodes s.t. cb(obj) = True (recursive)
        '''
        raise NotImplementedError('filter_outputs')

    def connectable(self, obj, attr):
        raise NotImplementedError('connectable')

    def connect(self, src, dst, attrs):
        '''
        attrs: either a list of attribute names, either a dictionary in
        {dst_attr: src_attr} format.
        Returns: True upon success
        '''
        raise NotImplementedError('connect')

    def set_selection(self, objs=[]):
        raise NotImplementedError('set_selection')

    def get_selection(self):
        raise NotImplementedError('get_selection')

    def get_selected_shapes(self):
        raise NotImplementedError('get_selected_shapes')

    def assign_new_material(self, obj, typ, name=''):
        raise NotImplementedError('assign_new_material')

    def assign_new_cgfxshader(self, obj, shader_path, name=''):
        '''
        Returns: created nodes
        '''
        raise NotImplementedError('assign_new_cgfxshader')

    def assign_new_vraymaterial(self, obj, name=''):
        '''
        Returns: created nodes
        '''
        raise NotImplementedError('assign_new_vraymaterial')

    def create_new_vraytexture(self, proj_type, name=''):
        '''
        Returns: created node
        '''
        raise NotImplementedError('create_new_vraytexture')

    def create_node(self, name, typ, parent=''):
        '''
        Returns: created node
        '''
        raise NotImplementedError('create_node')

    def create_group(self, name, parent='', ws_mtx=None):
        '''
        Returns: created node
        '''
        raise NotImplementedError('create_group')

    def create_curve(self, name, parent='', cvs=[], ctrl_flags=0):
        '''
        cvs: expected to store the CVs of a cubic Bezier path
        Returns: created nodes
        '''
        raise NotImplementedError('create_curve')

    def create_locator(self, name, parent='', ws_pos=None, show=False):
        '''
        Returns: created nodes
        '''
        raise NotImplementedError('create_locator')

    def create_texture_from_path(self, path):
        '''
        Returns: created nodes
        '''
        raise NotImplementedError('create_texture_from_path')

    def gamma_correct(self, name, gamma=None, input_value=None):
        '''
        Returns: created node
        '''
        raise NotImplementedError('gamma_correct')

    def mathop(self, op, name, in1, in2, attrs):
        '''
        Returns: (created node, output attribute)
        '''
        raise NotImplementedError('mathop')

    def condition(self, lvalue, op, rvalue, if_true, if_false, name):
        '''
        Returns: (created node, output attribute)
        '''
        raise NotImplementedError('condition')

    def apply_matrix(self, mtx, v3f, as_point, name):
        '''
        Returns: (created node, output attribute)
        '''
        raise NotImplementedError('apply_matrix')

    def dot(self, in1, in2, name):
        '''
        Returns: (created node, output attribute)
        '''
        raise NotImplementedError('dot')

    def switch(self, ctrl, ctrl_name, in0, in1, attrs):
        '''
        Returns: (created node, output attribute)
        '''
        raise NotImplementedError('blend')

    def parent_constrain(self, obj, parent, keep_offset=True):
        '''
        Returns: created constraint
        '''
        raise NotImplementedError('parent_constrain')

    def set_parent(self, child, parent, keep_local=False):
        '''
        Returns: child's new fullpath
        '''
        raise NotImplementedError('set_parent')

    def get_parents(self, obj):
        raise NotImplementedError('get_parents')

    def get_children(self, obj):
        raise NotImplementedError('get_children')

    def create_object_set(self, objs, name, parent=''):
        '''
        Returns: created selection
        '''
        raise NotImplementedError('create_object_set')

    def get_object_sets(self):
        '''
        Returns: {'set': ['parent sets']}
        '''
        raise NotImplementedError('get_object_sets')

    def set_pivot(self, obj, ws_pos):
        raise NotImplementedError('set_pivot')

    def set_world_matrix(self, obj, mtx):
        raise NotImplementedError('set_world_matrix')

    def get_world_matrix(self, obj):
        raise NotImplementedError('get_world_matrix')

    def get_world_bbox(self, objs):
        raise NotImplementedError('get_world_bbox')

    def get_short_name(self, obj):
        raise NotImplementedError('get_short_name')

    def get_asset_name(self):
        raise NotImplementedError('get_asset_name')
