# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
from stickerface.Qt import QtCore
from stickerface.Qt import QtGui
from stickerface.Qt import QtWidgets

from stickerface import facial_helper  # noqa I100
from stickerface import gui_util

reload(facial_helper)
reload(gui_util)


# __        ___     _            _     ____
# \ \      / (_) __| | __ _  ___| |_  | __ )  __ _ ___  ___
#  \ \ /\ / /| |/ _` |/ _` |/ _ \ __| |  _ \ / _` / __|/ _ \
#   \ V  V / | | (_| | (_| |  __/ |_  | |_) | (_| \__ \  __/
#    \_/\_/  |_|\__,_|\__, |\___|\__| |____/ \__,_|___/\___|
#                     |___/

class MainWidgetBase(QtWidgets.QWidget):
    ICON_SIZE = QtCore.QSize(36, 36)
    LINE_EDIT_WIDTH = 250
    DEFAULT_H_MARGINS = 11

    #  ____  _                   _
    # / ___|(_) __ _ _ __   __ _| |___
    # \___ \| |/ _` | '_ \ / _` | / __|
    #  ___) | | (_| | | | | (_| | \__ \
    # |____/|_|\__, |_| |_|\__,_|_|___/
    #          |___/

    class _Signals(QtCore.QObject):
        finished = QtCore.Signal()

    _SIGNALS = _Signals()

    finished = _SIGNALS.finished

    def __init__(self, cmds, parent=None, f=QtCore.Qt.WindowFlags()):
        super(MainWidgetBase, self).__init__(parent=parent, f=f)
        self._helper = facial_helper.FacialHelper(cmds=cmds)
        self._bg_pixmap = QtGui.QPixmap(gui_util.path('background.png'))

        # for versions of Maya superior to 2015, a 'plastique' style
        # must be imposed.
        self.setStyle(QtWidgets.QStyleFactory.create('plastique'))
        self.setStyleSheet('''
            QLineEdit {
                background-color: rbga(0, 0, 0, 0);
                border-bottom: 1px solid palette(light);
                border-right: 1px solid palette(light);
                border-radius: 0;
            }
            QToolBar {
                padding: 0;
            }
            QToolBar QToolButton {
                padding: 0;
                margin: 0;
            }
            QToolBar QToolButton:hover {
                border: 1px solid palette(light);
                border-radius: 4px;
            }
            QComboBox {
                border: 1px solid palette(light);
                font-size: 14px;
            }
            QComboBox::drop-down {
            }
            QScrollArea {
                border: none;
            }
            QGroupBox {
                background-color: rgba(0, 0, 0, 0);
            }
            QListWidget {
                background-color: rgba(255, 0, 0, 0);
            }''')

    @property
    def helper(self):
        return self._helper

    @property
    def cmds(self):
        return self.helper.cmds

    def paintEvent(self, evt):
        p = QtGui.QPainter(self)
        p.drawPixmap(
            self.width() - self._bg_pixmap.width(),
            self.height() - self._bg_pixmap.height(),
            self._bg_pixmap)
        super(MainWidgetBase, self).paintEvent(evt)

    @staticmethod
    def clear_layout(layout):
        while layout and layout.count():
            child = layout.takeAt(0)
            if child.widget():
                child.widget().deleteLater()
