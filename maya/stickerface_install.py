# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import os
import sys


def _stickerface_path(*args):
    return os.path.abspath(os.path.join(
        os.path.dirname(__file__),
        'python',
        'stickerface',
        *args))


def _add_shelf_button(module_path, icon_path, tooltip):
    import maya.cmds as mc
    import maya.mel as mm

    for _ in [module_path, icon_path]:
        if not os.path.isfile(_):
            raise RuntimeError('Failed to find "{:s}".'.format(_))
    module_name = os.path.splitext(os.path.basename(module_path))[0]
    cmd = '''
import sys
sys.path.insert(0, r"{path}")
from stickerface import {module}
reload({module})
{module}.main()
    '''.format(module=module_name, path=_stickerface_path('..'))
    print(cmd)

    # get current shelf and add button to it
    shelf = mm.eval('$gShelfTopLevel = $gShelfTopLevel')
    parent = mc.tabLayout(shelf, query=True, selectTab=True)
    mc.shelfButton(
        command=cmd,
        annotation=tooltip,
        sourceType='Python',
        image=icon_path,
        image1=icon_path,
        parent=parent)


def _stickerface_install():
    '''
    Script invoked upon the drag-drop of the 'stickerface_install.mel' file
    onto Maya.

    It initializes the sys.path and creates the three shelf buttons invoking
    all tools provided by the StickerFace project.
    '''
    try:
        for (key, tooltip) in [
            ('autorig', 'StickerFace Autorig'),
            ('picker', 'StickerFace Picker'),
            ('vray', 'VRay for StickerFace'),
        ]:
            module_path = _stickerface_path(
                'main_{}.py'.format(key))
            icon_path = _stickerface_path(
                'gui',
                'main_icon_{}.png'.format(key))
            _add_shelf_button(
                module_path=module_path,
                icon_path=icon_path,
                tooltip=tooltip)

    except Exception as e:
        sys.stderr.write(
            'Failed to install StickerFace\n{:s}'.format(e.message))
