
![Alt text](logo.png "StickerFace")

StickerFace — Toolkit for Sprite-Based Facial Animation
=======================================================

## Presentation

StickerFace consists in the collection of scripts and plugins conceived for rigging, animating and rendering cartoon-inspired, sprite-based facial expressions with Autodesk Maya and VRay.

## Documentation

Beside the source code for the autorig scripts, the sprite picker and the VRay plugin, an in-depth documentation is available [here](https://stickerface.gitlab.io).

## Authors

* Pierre-Edouard LANDES (pel@supamonks.com)

Copyright 2019 SUPAMONKS STUDIO