// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <cmath>
#include <sstream>
#include <vrayplugins.h>
#include <vrayinterface.h>
#include <vrayrenderer.h>
#include <vraytexutils.h>
#include <defparams.h>

#include <stickerface/BitmapPNG.hpp>
#include <stickerface/vray_utils.hpp>

#include "stickerface/sphere/sprite.hpp"
#include "stickerface/sphere/AllParms.hpp"
#include "stickerface/sphere/Precomps.hpp"

namespace stickerface { namespace sphere
{
    static std::string g_BODY_TEXTURE = "bodyTexture";
    static std::string g_SPRITESHEET = "spriteSheet";
    static std::string g_SPRITESHEET_FILE = "spriteSheet";
    static VectorParmDecl<3> g_SPRITESHEET_GAMMA("spriteGamma");
    static std::string g_NUM_SPRITE_COLS = "numSpriteCols";
    static std::string g_NUM_SPRITE_ROWS = "numSpriteRows";
    static VectorParmDecl<3> g_CENTER_WPOS("centerWPosition");
    static VectorParmDecl<3> g_UP_WPOS("upWPosition");
    static std::string g_ALPHA_OUT = "outAlpha";

    static SpriteParmDecl g_SPRITES[] =
    {
        SpriteParmDecl("extraBack"),
        SpriteParmDecl("eyeballL"),
        SpriteParmDecl("eyeballR"),
        SpriteParmDecl("pupilL"),
        SpriteParmDecl("pupilR"),
        SpriteParmDecl("lowerEyelidL"),
        SpriteParmDecl("lowerEyelidR"),
        SpriteParmDecl("upperEyelidL"),
        SpriteParmDecl("upperEyelidR"),
        SpriteParmDecl("mouth"),
        SpriteParmDecl("browL"),
        SpriteParmDecl("browR"),
        SpriteParmDecl("extraFront"),
        SpriteParmDecl("extraTop")
    };
    static const size_t g_NUM_SPRITES = sizeof(g_SPRITES) / sizeof(SpriteParmDecl);
    enum {
        EXTRA_BACK = 0,
        EYEBALL_L,
        EYEBALL_R,
        PUPIL_L,
        PUPIL_R,
        LOWER_EYELID_L,
        LOWER_EYELID_R,
        UPPER_EYELID_L,
        UPPER_EYELID_R,
        MOUTH,
        BROW_L,
        BROW_R,
        EXTRA_FRONT,
        EXTRA_TOP
    };

    ////////////////////////
    // struct SphereTextureParms
    ////////////////////////
    struct SphereTextureParms:
        VR::VRayParameterListDesc
    {
        inline
        SphereTextureParms();
    };

    ///////////////////
    // struct SphereTexture
    ///////////////////
    struct SphereTexture:
        VR::VRayTexture, VR::RTTexInterface
    {
        friend struct AlphaOutput;
    private:

        /////////////////////
        // struct AlphaOutput
        /////////////////////
        struct AlphaOutput:
            VR::TextureFloatInterface
        {
            SphereTexture& parent;
        public:
            explicit inline
            AlphaOutput(
                SphereTexture& parent
            ):
                parent(parent)
            { }

            inline
            VR::real getTexFloat(const VR::VRayContext &rc) VRAY_OVERRIDE
            {
                float alpha = 0.0f;
                parent.getTexColor(rc, alpha);
                return alpha;
            }

            inline
            void
            getTexFloatBounds(VR::real& fmin,
                              VR::real& fmax)
            {
                fmin = 0.0f;
                fmax = 1.0f;
            }

            PluginBase*
            getPlugin()
            {
                return (PluginBase*)(this);
            }
        };
        /////////////////////

    private:
        VR::TextureInterface* _bodyTexture;
        BitmapPNG             _spriteSheet;
        VectorParmCache<3>    _spriteSheetGamma;
        int                   _numSpriteCols;
        int                   _numSpriteRows;
        VectorParmCache<3>    _center;
        VectorParmCache<3>    _upWPosition;
        SpriteParmCache**     _sprites;

        AllParms       _parms;
        Precomps       _pc;
        VR::TracePoint _sceneOffset;

        AlphaOutput _alphaOutput;

    public:
        explicit inline
        SphereTexture(VR::VRayPluginDesc*);

        inline
        ~SphereTexture();

        inline
        PluginBase*
        getPlugin() VRAY_OVERRIDE
        {
            return static_cast<PluginBase*>(this);
        }

        inline
        PluginInterface*
        newInterface(InterfaceID id) VRAY_OVERRIDE
        {
            return id == EXT_RTTEX
                ? static_cast<VR::RTTexInterface*>(this)
                : VR::VRayTexture::newInterface(id);
        }

        void
        renderBegin(VR::VRayRenderer*) VRAY_OVERRIDE;

        void
        frameBegin(VR::VRayRenderer*) VRAY_OVERRIDE;

        // TextureInterface
        //-----------------
        VR::AColor
        getTexColor(const VR::VRayContext&) VRAY_OVERRIDE;

        inline
        void
        getTexColorBounds(VR::AColor& fmin,
                          VR::AColor& fmax) VRAY_OVERRIDE
        {
            fmin.set(-FLT_MAX, -FLT_MAX, -FLT_MAX, 1.0f);
            fmax.set(FLT_MAX, FLT_MAX, FLT_MAX, 1.0f);
        }

        // RTTexInterface
        //---------------
        inline int
        getDataSize()
        {
            return 0;
        }

        inline
        void
        getData(float*)
        { }

        inline
        int
        getSource(const char*,
                  const char*,
                  const char*,
                  VR::ShaderSource&,
                  VR::OCLData&)
        {
            return 0;
        }

    private:
        VR::AColor
        getTexColor(const VR::VRayContext&,
                    float& alpha);

        void
        precomputeSpriteInfo(double time);

        void
        updateSpriteSheet();

        void
        updateAllParms(double time);

        inline
        int
        getInt(std::string&,
               double time);

        inline
        float
        getFloat(std::string&,
                 double time);

        template<int Dim> inline
        Eigen::Matrix<float, Dim, 1>
        getValue(VectorParmDecl<Dim>&,
                 double time);

        Eigen::Matrix3f
        getSpriteFrame(const Eigen::Vector3f& up,
                       const Eigen::Vector3f& frontWPosition,
                       float                  rotateD) const;

        VR::AColor
        getSpriteTexel(size_t                 sprite,
                       const Eigen::Vector3f& dp,
                       const Eigen::Vector2f& rNumSprites) const;
    };
} }

using namespace VR;
using namespace stickerface;
using namespace stickerface::sphere;

////////////////////////
// struct SphereTextureParms
////////////////////////
SphereTextureParms::SphereTextureParms()
{
    addParamTexture(&g_BODY_TEXTURE.front(), Color(0.0f, 0.0f, 0.0f), -1);
    addParamString(&g_SPRITESHEET_FILE.front(), "", -1);
    addParamInt(&g_NUM_SPRITE_COLS.front(), 0, -1);
    addParamInt(&g_NUM_SPRITE_ROWS.front(), 0, -1);

    g_SPRITESHEET_GAMMA.addTo(*this, 1.0f);
    g_CENTER_WPOS.addTo(*this);
    g_UP_WPOS.addTo(*this);

    for (size_t i = 0; i < g_NUM_SPRITES; ++i)
        g_SPRITES[i].addTo(*this);

    addOutputParamTextureFloat(&g_ALPHA_OUT.front());
}

///////////////////
// struct SphereTexture
///////////////////
// explicit
SphereTexture::SphereTexture(
    VRayPluginDesc* pluginDesc
):
    VRayTexture      (pluginDesc),
    _spriteSheet     (),
    _spriteSheetGamma(g_SPRITESHEET_GAMMA),
    _numSpriteCols   (0),
    _numSpriteRows   (0),
    _center          (g_CENTER_WPOS),
    _upWPosition     (g_UP_WPOS),
    _sprites         (nullptr),
    _parms           (),
    _pc              (),
    _sceneOffset     (),
    _alphaOutput     (*this)
{
    paramList->setParamCache(&g_BODY_TEXTURE.front(), &_bodyTexture);
    paramList->setParamCache(&g_NUM_SPRITE_COLS.front(), &_numSpriteCols);
    paramList->setParamCache(&g_NUM_SPRITE_ROWS.front(), &_numSpriteRows);

    _spriteSheetGamma.setup(*paramList);
    _center.setup(*paramList);
    _upWPosition.setup(*paramList);

    _sprites = new SpriteParmCache*[g_NUM_SPRITES];
    for (size_t i = 0; i < g_NUM_SPRITES; ++i)
    {
        _sprites[i] = new SpriteParmCache(g_SPRITES[i]);
        _sprites[i]->setup(*paramList);
    }

    paramList->setOutputParamCache(&g_ALPHA_OUT.front(), &_alphaOutput);
}

inline
SphereTexture::~SphereTexture()
{
    for (size_t i = 0; i < g_NUM_SPRITES; ++i)
        delete _sprites[i];
    delete[] _sprites;
}

void
SphereTexture::precomputeSpriteInfo(double time)
{
    _pc.clear();

    updateAllParms(time);

    const size_t numSprites = _parms.indices.size();
    if (numSprites == 0 ||
        _parms.numSpriteCols == 0 ||
        _parms.numSpriteRows == 0)
        return;

    _pc.colRow.resize(numSprites);
    _pc.frames.resize(numSprites);

    Eigen::Vector3f up = (_parms.upWPosition - _parms.center).normalized();

    for (size_t i = 0; i < numSprites; ++i)
    {
        _pc.colRow[i][0] = _parms.indices[i] % _parms.numSpriteCols;
        _pc.colRow[i][1] = _parms.indices[i] / _parms.numSpriteCols;
        _pc.frames[i]    = getSpriteFrame(up, _parms.frontWPositions[i], _parms.mappings[i].z());
    }

    {
        std::stringstream sstr;
        sstr << _pc << "\n";
        printf(sstr.str().c_str());
    }
}

void
SphereTexture::updateAllParms(double time)
{
    _parms.clear();

    Eigen::Vector3f spriteGamma = getValue<3>(g_SPRITESHEET_GAMMA, time);

    for (int k = 0; k < 3; ++k)
        _parms.rSprtGamma[k] = spriteGamma[k] > 0.0f
            ? 1.0f / spriteGamma[k]
            : 1.0f;
    _parms.center        = getValue<3>(g_CENTER_WPOS, time);
    _parms.upWPosition   = getValue<3>(g_UP_WPOS, time);
    _parms.numSpriteCols = getInt(g_NUM_SPRITE_COLS, time);
    _parms.numSpriteRows = getInt(g_NUM_SPRITE_ROWS, time);

    _parms.indices        .resize(g_NUM_SPRITES);
    _parms.frontWPositions.resize(g_NUM_SPRITES);
    _parms.mappings       .resize(g_NUM_SPRITES);
    for (size_t i = 0; i < g_NUM_SPRITES; ++i)
    {
        //_parms.indices[i]         = getInt(g_SPRITES[i].indexName, time);
        _parms.indices[i]         = static_cast<int>(getFloat (g_SPRITES[i].indexName, time));
        _parms.frontWPositions[i] = getValue<3>(g_SPRITES[i].frontWPositionDecl, time);
        _parms.mappings[i]        = getValue<3>(g_SPRITES[i].mappingDecl, time);
    }

    {
        std::stringstream sstr;
        sstr
            << "time = " << time << "\n"
            << _parms << "\n"
            << "scene offset = " << _sceneOffset.x << " " << _sceneOffset.y << " " << _sceneOffset.z
            << std::endl;
        printf(sstr.str().c_str());
    }
}

Eigen::Matrix3f
SphereTexture::getSpriteFrame(const Eigen::Vector3f& up,
                              const Eigen::Vector3f& frontWPosition,
                              float                  rotateD) const
{
    Eigen::Vector3f frontAxis = (frontWPosition - _parms.center).normalized();
    Eigen::Vector3f sideAxis  = up.cross(frontAxis);
    Eigen::Vector3f upAxis    = frontAxis.cross(sideAxis);

    const float rotate = static_cast<float>(M_PI) * rotateD / 180.0f;
    const float cRotate = cosf(rotate);
    const float sRotate = sinf(rotate);

    Eigen::Matrix3f frame;
    frame.col(0) = cRotate * sideAxis + sRotate * upAxis; // rotated side
    frame.col(1) = - sRotate * sideAxis + cRotate * upAxis; // rotated up
    frame.col(2) = frontAxis;

    return frame;
}

AColor
SphereTexture::getSpriteTexel(size_t                 sprite,
                              const Eigen::Vector3f& dp,
                              const Eigen::Vector2f& rNumSprites) const
{
    AColor texel(0.0f, 0.0f, 0.0f, 0.0f);

    if (sprite >= _pc.frames.size())
        return texel;
    const Eigen::Vector3f& mapping = _parms.mappings[sprite];
    if (fabsf(mapping.x()) < 1e-6f ||
        fabsf(mapping.y()) < 1e-6f)
        return texel;

    //-------------------------------------------------
    //return toAColor(dp);  // DEBUG
    //-------------------------------------------------

    // get dp projection in sprite's local frame
    const Eigen::Matrix3f&  frame = _pc.frames[sprite];

    Eigen::Vector3f dpCoords(
        dp.dot(frame.col(0)),
        dp.dot(frame.col(1)),
        dp.dot(frame.col(2)));

    //-------------------------------------------------
    // return toAColor(dpCoords);    // DEBUG
    //-------------------------------------------------
    dpCoords.x() /= mapping.x();
    dpCoords.y() /= mapping.y();

    // compute texture coordinate in sprite sheet
    const Eigen::Vector2i& colRow = _pc.colRow[sprite];

    const float rsqrt_2  = 1.0f / sqrt(2.0f);
    const float sAngSize = sinf(radians(16.0f));
    const float cst      = fabsf(sAngSize) > 1e-6f ? rsqrt_2 / sAngSize : 0.0f;
    float u = 0.5f + cst * dpCoords.x();
    float v = 0.5f + cst * dpCoords.y();
    float mask = dp.dot(frame.col(2)) > 0.0f &&
        0.0f < u && u < 1.0f &&
        0.0f < v && v < 1.0f
        ? 1.0f
        : 0.0f;
    ////-------------------------------------------------
    //return toAColor(Eigen::Vector2f(u, v), mask);    // DEBUG
    ////-------------------------------------------------

    u = (u + static_cast<float>(colRow.x())) * rNumSprites.x();
    v = (v + static_cast<float>(colRow.y())) * rNumSprites.y();
    //-------------------------------------------------
    //return toAColor(Eigen::Vector2f(u, v), mask);    // DEBUG
    //-------------------------------------------------

    if (mask > 1e-6f)
    {
        _spriteSheet.get(u, v, texel);
        for (int k = 0; k < 3; ++k)
            if (fabsf(_parms.rSprtGamma[k] - 1.0f) > 1e-4f)
                texel[k] = powf(
                    static_cast<float>(texel[k]),
                    _parms.rSprtGamma[k]);
    }
    texel.alpha *= mask;
    texel.color *= texel.alpha;

    return texel;
}

int
SphereTexture::getInt(std::string& name,
                      double       time)
{
    if (name.empty())
        return 0;
    VRayPluginParameter* parm = getParameter(&name.front());
    return parm ? parm->getInt(0, time) : 0;
}

float
SphereTexture::getFloat(std::string& name,
                        double       time)
{
    if (name.empty())
        return 0.0f;
    VRayPluginParameter* parm = getParameter(&name.front());
    return parm ? parm->getFloat(0, time) : 0.0f;
}

template<int Dim>
Eigen::Matrix<float, Dim, 1>
SphereTexture::getValue(VectorParmDecl<Dim>& decl,
                        double               time)
{
    Eigen::Matrix<float, Dim, 1> ret;
    for (int i = 0; i < Dim; ++i)
    {
        VRayPluginParameter* parm = getParameter(&decl.names[i].front());
        ret[i] = parm ? parm->getFloat(0, time) : 0.0f;
    }
    return ret;
}

void
SphereTexture::renderBegin(VR::VRayRenderer* vray)
{
    VRayTexture::renderBegin(vray);
    //---
    // grab sprite sheet bitmap
    updateSpriteSheet();
}

void
SphereTexture::updateSpriteSheet()
{
    _spriteSheet.clear();

    VRayPluginParameter* parm = getParameter(&g_SPRITESHEET_FILE.front());
    std::string          file = parm
        ? std::string(parm->getString(0, 0.0f))
        : std::string();
    if (file.empty())
        return;

    _spriteSheet.load(file);
}

void
SphereTexture::frameBegin(VR::VRayRenderer *vray)
{
    VRayTexture::frameBegin(vray);
    //---
    double time = vray
        ? vray->getFrameData().t
        : 0.0;

    _sceneOffset = vray
        ? vray->getFrameData().sceneOffset
        : VUtils::TracePoint(0.0f, 0.0f, 0.0f);
    precomputeSpriteInfo(time);
}

AColor
SphereTexture::getTexColor(const VRayContext& rc)
{
    float alpha = 0.0f;
    return getTexColor(rc, alpha);
}

AColor
SphereTexture::getTexColor(const VRayContext& rc,
                           float&             alpha)
{
    AColor rgba;

    rgba.set(0.0f, 0.0f, 0.0f, 0.0f);
    alpha = 0.0f;

    if (_bodyTexture)
    {
        rgba = _bodyTexture->getTexColor(rc);
    }

    const size_t numSprites    = _parms.indices.size();
    const int    numSpriteCols = _parms.numSpriteCols;
    const int    numSpriteRows = _parms.numSpriteRows;
    if (numSprites == 0 || numSpriteCols == 0 || numSpriteRows == 0 ||
        static_cast<bool>(_spriteSheet) == false)
        return rgba;

    const VUtils::TracePoint& wpos = rc.getRayResult().wpoint + _sceneOffset;
    //-------------------------------------------------
    //return toAColor(wpos);    // DEBUG
    //-------------------------------------------------

    Eigen::Vector3f dp(
        static_cast<float>(wpos.x) - _parms.center.x(),
        static_cast<float>(wpos.y) - _parms.center.y(),
        static_cast<float>(wpos.z) - _parms.center.z());
    dp.normalize();

    Eigen::Vector2f rNumSprites(
        1.0f / static_cast<float>(numSpriteCols),
        1.0f / static_cast<float>(numSpriteRows));

    //-------------------------------------------------
    //return getSpriteTexel(0, dp, rNumSprites);    // DEBUG
    //-------------------------------------------------

    AColor* texels = new AColor[numSprites];
    for (size_t i = 0; i < numSprites; ++i)
        texels[i] = getSpriteTexel(i, dp, rNumSprites);

    texels[PUPIL_L].color *= texels[EYEBALL_L].alpha;
    texels[PUPIL_L].alpha *= texels[EYEBALL_L].alpha;

    texels[PUPIL_R].color *= texels[EYEBALL_R].alpha;
    texels[PUPIL_R].alpha *= texels[EYEBALL_R].alpha;

    texels[UPPER_EYELID_R].color *= texels[EYEBALL_R].alpha;
    texels[UPPER_EYELID_R].alpha *= texels[EYEBALL_R].alpha;

    texels[UPPER_EYELID_L].color *= texels[EYEBALL_L].alpha;
    texels[UPPER_EYELID_L].alpha *= texels[EYEBALL_L].alpha;

    texels[LOWER_EYELID_L].color *= texels[EYEBALL_L].alpha;
    texels[LOWER_EYELID_L].alpha *= texels[EYEBALL_L].alpha;

    texels[LOWER_EYELID_R].color *= texels[EYEBALL_R].alpha;
    texels[LOWER_EYELID_R].alpha *= texels[EYEBALL_R].alpha;

    for (size_t i = 0; i < numSprites; ++i)
    {
        if (alpha < texels[i].alpha)
            alpha = texels[i].alpha;
        rgba = over(texels[i], rgba);
    }
    delete[] texels;

    return rgba;
}

//----------------------------------------------------------------------------
//  ____  _             _         ____
// |  _ \| |_   _  __ _(_)_ __   |  _ \  ___  ___  ___
// | |_) | | | | |/ _` | | '_ \  | | | |/ _ \/ __|/ __|
// |  __/| | |_| | (_| | | | | | | |_| |  __/\__ \ (__
// |_|   |_|\__,_|\__, |_|_| |_| |____/ \___||___/\___|
//                |___/
PLUGIN_DESC(201902191258,
            EXT_TEXTURE,
            "StickerFace_SphereTexture",
            SphereTexture,
            SphereTextureParms);
//----------------------------------------------------------------------------
