// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <algorithm>

#include <lodepng.h>
#include <vrayinterface.h>

#include "stickerface/BitmapPNG.hpp"

using namespace stickerface;

BitmapPNG::BitmapPNG():
    _width  (0),
    _height (0),
    _image  ()
{ }

void
BitmapPNG::clear()
{
    _width = 0;
    _height = 0;
    _image.clear();
    _image.shrink_to_fit();
}

BitmapPNG::operator bool() const
{
    return
        _width > 0 &&
        _height > 0 &&
        _image.size() == (_width * _height) << 2;
}

void
BitmapPNG::load(const std::string& file)
{
    clear();
    if (file.empty())
        return;

    unsigned error = lodepng::decode(_image, _width, _height, file.c_str());
    if (error)
    {
        std::stringstream sstr;
        sstr
            << "ERROR\nfailed to load PNG bitmap from '" << file << "': "
            << lodepng_error_text(error)
            << "\n";
        printf(sstr.str().c_str());
    }
    else
    {
        std::stringstream sstr;
        sstr
            << "bitmap loaded from '" << file << "' "
            << "-> " << _width << " x " << _height << " (#image elts = " << _image.size() << ")"
            << "\n";
        printf(sstr.str().c_str());
    }
}

void
BitmapPNG::get(float           u,
               float           v,
               VUtils::AColor& rgba) const
{
    static const float M_1_255 = 1.0f / 255.0f;

    float x    = u * static_cast<float>(_width);
    float y    = (1.0f - v) * static_cast<float>(_height); // y-flipped
    float x0   = floorf(x);
    float y0   = floorf(y);
    float dx   = x - x0;
    float dy   = y - y0;
    float dxdy = dx * dy;

    int i0 = std::max<int>(0, std::min<int>(_width - 1, static_cast<int>(x0)));
    int j0 = std::max<int>(0, std::min<int>(_height - 1, static_cast<int>(y0)));
    int i1 = std::max<int>(0, std::min<int>(_width - 1, i0 + 1));
    int j1 = std::max<int>(0, std::min<int>(_height - 1, j0 + 1));

    VUtils::AColor A, B, C, D;

    getRawRGBA(i0, j0, A);
    getRawRGBA(i1, j0, B);
    getRawRGBA(i0, j1, C);
    getRawRGBA(i1, j1, D);

    rgba = (
        (1.0f - dx - dy + dxdy) * A +
        (dx - dxdy) * B +
        (dy - dxdy) * C +
        dxdy * D
    ) * M_1_255;
}

void
BitmapPNG::getRawRGBA(int             i,
                      int             j,
                      VUtils::AColor& rgba) const
{
    rgba.color.set(0.0f, 0.0f, 0.0f);
    rgba.alpha = 0.0f;
    if (i < 0 || i >= static_cast<int>(_width) ||
        j < 0 || j >= static_cast<int>(_height))
        return;

    const int idx = (i + _width * j) << 2;

    rgba.color.set(
        static_cast<float>(_image[idx + 0]),
        static_cast<float>(_image[idx + 1]),
        static_cast<float>(_image[idx + 2])
    );
    rgba.alpha = static_cast<float>(_image[idx + 3]);
}
