// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <cmath>
#include <sstream>
#include <vrayplugins.h>
#include <vrayinterface.h>
#include <vrayrenderer.h>
#include <vraytexutils.h>
#include <defparams.h>

#include <stickerface/BitmapPNG.hpp>
#include <stickerface/vray_utils.hpp>

#include "stickerface/plane/sprite.hpp"
#include "stickerface/plane/AllInputs.hpp"
#include "stickerface/plane/Precomps.hpp"

namespace stickerface { namespace plane
{
    static std::string g_BODY_TEXTURE = "bodyTexture";
    static std::string g_SPRITESHEET = "spriteSheet";
    static std::string g_SPRITESHEET_FILE = "spriteSheet";
    static VectorParmDecl<3> g_SPRITESHEET_GAMMA("spriteGamma");
    static std::string g_NUM_SPRITE_COLS = "numSpriteCols";
    static std::string g_NUM_SPRITE_ROWS = "numSpriteRows";
    static VectorParmDecl<3> g_CUBE_CENTER_WPOS("cubeCenterWPosition");
    static VectorParmDecl<3> g_CUBE_UP_WPOS("cubeUpWPosition");
    static VectorParmDecl<3> g_CUBE_FRONT_WPOS("cubeFrontWPosition");
    static std::string g_ALPHA_OUT = "outAlpha";

    static SpriteParmDecl g_SPRITES[] =
    {
        SpriteParmDecl("extraBack"),
        SpriteParmDecl("eyeballL"),
        SpriteParmDecl("eyeballR"),
        SpriteParmDecl("pupilL"),
        SpriteParmDecl("pupilR"),
        SpriteParmDecl("lowerEyelidL"),
        SpriteParmDecl("lowerEyelidR"),
        SpriteParmDecl("upperEyelidL"),
        SpriteParmDecl("upperEyelidR"),
        SpriteParmDecl("mouth"),
        SpriteParmDecl("browL"),
        SpriteParmDecl("browR"),
        SpriteParmDecl("extraFront"),
        SpriteParmDecl("extraTop")
    };
    static const size_t g_NUM_SPRITES = sizeof(g_SPRITES) / sizeof(SpriteParmDecl);
    enum {
        EXTRA_BACK = 0,
        EYEBALL_L,
        EYEBALL_R,
        PUPIL_L,
        PUPIL_R,
        LOWER_EYELID_L,
        LOWER_EYELID_R,
        UPPER_EYELID_L,
        UPPER_EYELID_R,
        MOUTH,
        BROW_L,
        BROW_R,
        EXTRA_FRONT,
        EXTRA_TOP
    };

    ///////////////////////////
    // struct PlaneTextureParms
    ///////////////////////////
    struct PlaneTextureParms:
        VR::VRayParameterListDesc
    {
        inline
        PlaneTextureParms();
    };

    //////////////////////
    // struct PlaneTexture
    //////////////////////
    struct PlaneTexture:
        VR::VRayTexture, VR::RTTexInterface
    {
        friend struct AlphaOutput;
    private:
        enum { NUM_TIME_STEPS = 16 };
    private:

        /////////////////////
        // struct AlphaOutput
        /////////////////////
        struct AlphaOutput:
            VR::TextureFloatInterface
        {
            PlaneTexture& parent;
        public:
            explicit inline
            AlphaOutput(
                PlaneTexture& parent
            ):
                parent(parent)
            { }

            inline
            VR::real
            getTexFloat(const VR::VRayContext &rc) VRAY_OVERRIDE
            {
                float alpha = 0.0f;
                parent.getTexColor(rc, alpha);
                return alpha;
            }

            inline
            void
            getTexFloatBounds(VR::real& fmin,
                              VR::real& fmax)
            {
                fmin = 0.0f;
                fmax = 1.0f;
            }

            PluginBase*
            getPlugin()
            {
                return (PluginBase*)(this);
            }
        };
        /////////////////////

    private:
        VR::TextureInterface* _bodyTexture;
        BitmapPNG             _spriteSheet;
        VectorParmCache<3>    _spriteSheetGamma;
        int                   _numSpriteCols;
        int                   _numSpriteRows;
        VectorParmCache<3>    _center;
        VectorParmCache<3>    _upWPosition;
        SpriteParmCache**     _sprites;

        std::vector<AllInputs>     _inputs; //<! indexed by time steps (for motion blur)
        std::vector<AllSpritesPcp> _pcp;    //<! indexed by time steps (for motion blur)
        VR::TracePoint             _sceneOffset;

        AlphaOutput _alphaOutput;

    public:
        explicit inline
        PlaneTexture(VR::VRayPluginDesc*);

        inline
        ~PlaneTexture();

        inline
        PluginBase*
        getPlugin() VRAY_OVERRIDE
        {
            return static_cast<PluginBase*>(this);
        }

        inline
        PluginInterface*
        newInterface(InterfaceID id) VRAY_OVERRIDE
        {
            return id == EXT_RTTEX
                ? static_cast<VR::RTTexInterface*>(this)
                : VR::VRayTexture::newInterface(id);
        }

        void
        renderBegin(VR::VRayRenderer*) VRAY_OVERRIDE;

        void
        frameBegin(VR::VRayRenderer*) VRAY_OVERRIDE;

        // TextureInterface
        //-----------------
        VR::AColor
        getTexColor(const VR::VRayContext&) VRAY_OVERRIDE;

        inline
        void
        getTexColorBounds(VR::AColor& fmin,
                          VR::AColor& fmax) VRAY_OVERRIDE
        {
            fmin.set(-FLT_MAX, -FLT_MAX, -FLT_MAX, 1.0f);
            fmax.set(FLT_MAX, FLT_MAX, FLT_MAX, 1.0f);
        }

        // RTTexInterface
        //---------------
        inline int
        getDataSize()
        {
            return 0;
        }

        inline
        void
        getData(float*)
        { }

        inline
        int
        getSource(const char*,
                  const char*,
                  const char*,
                  VR::ShaderSource&,
                  VR::OCLData&)
        {
            return 0;
        }

    private:
        VR::AColor
        getTexColor(const VR::VRayContext&,
                    float& alpha);

        void
        updateSpriteSheet();

        void
        getInputValues(double time,
                       AllInputs&);

        inline
        int
        getInt(std::string&,
               double time);

        inline
        float
        getFloat(std::string&,
                 double time);

        template<int Dim> inline
        Eigen::Matrix<float, Dim, 1>
        getValue(VectorParmDecl<Dim>&,
                 double time);

        static
        void
        precomputeValuesForAllSprites(const AllInputs&,
                                      AllSpritesPcp&);

        static
        void
        getCubeFrame(const AllInputs&,
                     Eigen::Matrix3f&);

        static
        void
        getSpriteFrame(const Eigen::Matrix3f&,
                       float,
                       Eigen::Matrix3f&);
        void
        interpolate(double time,
                    AllInputs&,
                    AllSpritesPcp&) const;

        void
        interpolate(int   i0,
                    int   i1,
                    float w1,
                    AllInputs&) const;

        void
        interpolate(int   i0,
                    int   i1,
                    float w1,
                    AllSpritesPcp&) const;

        VR::AColor
        getSpriteTexel(const Eigen::Vector3f& wPosition,
                       const Eigen::Vector3f& wNormal,
                       const Eigen::Vector3f& sprtFrontWPosition,
                       const Eigen::Vector3f& sprtMapping,
                       const Eigen::Vector3f&,
                       const SpritePcp&,
                       const Eigen::Vector2f& rNumSprites) const;
    };

} }

using namespace VR;
using namespace stickerface;
using namespace stickerface::plane;

///////////////////////////
// struct PlaneTextureParms
///////////////////////////
PlaneTextureParms::PlaneTextureParms()
{
    addParamTexture(&g_BODY_TEXTURE.front(), Color(0.0f, 0.0f, 0.0f), -1);
    addParamString (&g_SPRITESHEET_FILE.front(), "", -1);
    addParamInt    (&g_NUM_SPRITE_COLS.front(), 0, -1);
    addParamInt    (&g_NUM_SPRITE_ROWS.front(), 0, -1);

    g_SPRITESHEET_GAMMA.addTo(*this, 1.0f);
    g_CUBE_CENTER_WPOS.addTo(*this);
    g_CUBE_UP_WPOS.addTo(*this);
    g_CUBE_FRONT_WPOS.addTo(*this);

    for (size_t i = 0; i < g_NUM_SPRITES; ++i)
        g_SPRITES[i].addTo(*this);

    addOutputParamTextureFloat(&g_ALPHA_OUT.front());
}

//////////////////////
// struct PlaneTexture
//////////////////////

// explicit inline
PlaneTexture::PlaneTexture(
    VRayPluginDesc* pluginDesc
):
    VRayTexture(pluginDesc),
    _spriteSheet     (),
    _spriteSheetGamma(g_SPRITESHEET_GAMMA),
    _numSpriteCols   (0),
    _numSpriteRows   (0),
    _center          (g_CUBE_CENTER_WPOS),
    _upWPosition     (g_CUBE_UP_WPOS),
    _sprites         (nullptr),
    _inputs          (),
    _pcp             (),
    _sceneOffset     (),
    _alphaOutput     (*this)
{
    _inputs.resize(NUM_TIME_STEPS);
    _pcp   .resize(NUM_TIME_STEPS);

    paramList->setParamCache(&g_BODY_TEXTURE.front(),    &_bodyTexture);
    paramList->setParamCache(&g_NUM_SPRITE_COLS.front(), &_numSpriteCols);
    paramList->setParamCache(&g_NUM_SPRITE_ROWS.front(), &_numSpriteRows);

    _spriteSheetGamma.setup(*paramList);
    _center.setup(*paramList);
    _upWPosition.setup(*paramList);

    _sprites = new SpriteParmCache*[g_NUM_SPRITES];
    for (size_t i = 0; i < g_NUM_SPRITES; ++i)
    {
        _sprites[i] = new SpriteParmCache(g_SPRITES[i]);
        _sprites[i]->setup(*paramList);
    }

    paramList->setOutputParamCache(&g_ALPHA_OUT.front(), &_alphaOutput);
}

PlaneTexture::~PlaneTexture()
{
    for (size_t i = 0; i < g_NUM_SPRITES; ++i)
        delete _sprites[i];
    delete[] _sprites;
}

void
PlaneTexture::getInputValues(double     time,
                             AllInputs& inputs)
{
    inputs.clear();

    Eigen::Vector3f gamma = getValue<3>(g_SPRITESHEET_GAMMA, time);
    for (int k = 0; k < 3; ++k)
        inputs.rSprtGamma[k] = gamma[k] > 0.0f ? 1.0f / gamma[k] : 1.0f;

    inputs.cubeCenterWPosition = getValue<3>(g_CUBE_CENTER_WPOS, time);
    inputs.cubeUpWPosition     = getValue<3>(g_CUBE_UP_WPOS, time);
    inputs.cubeFrontWPosition  = getValue<3>(g_CUBE_FRONT_WPOS, time);
    inputs.numSpriteCols       = getInt(g_NUM_SPRITE_COLS, time);
    inputs.numSpriteRows       = getInt(g_NUM_SPRITE_ROWS, time);

    inputs.sprtIndices        .resize(g_NUM_SPRITES);
    inputs.sprtFrontWPositions.resize(g_NUM_SPRITES);
    inputs.sprtMappings       .resize(g_NUM_SPRITES);
    for (size_t i = 0; i < g_NUM_SPRITES; ++i)
    {
        inputs.sprtIndices[i]         = static_cast<int>(getFloat (g_SPRITES[i].indexName, time));
        inputs.sprtFrontWPositions[i] = getValue<3>(g_SPRITES[i].frontWPositionDecl, time);
        inputs.sprtMappings[i]        = getValue<3>(g_SPRITES[i].mappingDecl, time);
    }
}

// static
void
PlaneTexture::precomputeValuesForAllSprites(const AllInputs& inputs,
                                            AllSpritesPcp&   allPcp)
{
    allPcp.clear();

    const size_t numSprites = inputs.sprtIndices.size();
    if (numSprites == 0 ||
        inputs.numSpriteCols == 0 ||
        inputs.numSpriteRows == 0)
        return;

    Eigen::Matrix3f cubeMtx;

    getCubeFrame(inputs, cubeMtx);
    allPcp.resize(numSprites);
    for (size_t i = 0; i < numSprites; ++i)
    {
        const float sprtRotateD = inputs.sprtMappings[i].z();
        SpritePcp&  pcp         = allPcp[i];

        pcp.colRow[0] = inputs.sprtIndices[i] % inputs.numSpriteCols;
        pcp.colRow[1] = inputs.sprtIndices[i] / inputs.numSpriteCols;

        Eigen::Matrix3f sprtMtx;
        getSpriteFrame(
            cubeMtx,
            sprtRotateD,
            sprtMtx);
        pcp.quat = Eigen::Quaternionf(sprtMtx);
    }

    {
        std::stringstream sstr;

        for (size_t i = 0; i < allPcp.size(); ++i)
            sstr
                << "sprite " << i << ":\n"
                << "\t(col, row) = " << allPcp[i].colRow.transpose() << "\n"
                << "\tmtx = \n" << allPcp[i].quat.toRotationMatrix() << "\n";
                //<< "\tmtx = \n" << allPcp[i].mtx << "\n";
        sstr << "\n";
        printf(sstr.str().c_str());
    }
}

// static
void
PlaneTexture::getCubeFrame(const AllInputs& inputs,
                           Eigen::Matrix3f& mtx)
{
    set(mtx, 2, (inputs.cubeFrontWPosition - inputs.cubeCenterWPosition).normalized());
    set(mtx, 1, (inputs.cubeUpWPosition - inputs.cubeCenterWPosition).normalized());
    set(mtx, 0, get(mtx, 1).cross(get(mtx, 2)));
    set(mtx, 1, get(mtx, 2).cross(get(mtx, 0)));
}

// static
void
PlaneTexture::getSpriteFrame(const Eigen::Matrix3f& cubeMtx,
                             float                  sprtRotateD,
                             Eigen::Matrix3f&       mtx)
{
    mtx = cubeMtx;

    // rotate x/y axes
    const float ang = radians(sprtRotateD);
    const float cAng = cosf(ang);
    const float sAng = sinf(ang);

    Eigen::Vector3f rotatedX = cAng * get(cubeMtx, 0) + sAng * get(cubeMtx, 1);  // rotated side
    Eigen::Vector3f rotatedY = - sAng * get(cubeMtx, 0) + cAng * get(cubeMtx, 1);  // rotated up
    set(mtx, 0, rotatedX);
    set(mtx, 1, rotatedY);
}

AColor
PlaneTexture::getSpriteTexel(const Eigen::Vector3f& wPosition,
                             const Eigen::Vector3f& wNormal,
                             const Eigen::Vector3f& sprtFrontWPosition,
                             const Eigen::Vector3f& sprtMapping,
                             const Eigen::Vector3f& rSprtGamma,
                             const SpritePcp&       sprtPcp,
                             const Eigen::Vector2f& rNumSprites) const
{
    AColor texel(0.0f, 0.0f, 0.0f, 0.0f);

    if (fabsf(sprtMapping[0]) < 1e-6f ||
        fabsf(sprtMapping[1]) < 1e-6f)
        return texel;

    const Eigen::Matrix3f& mtx    = sprtPcp.quat.toRotationMatrix();
    const Eigen::Vector2i& colRow = sprtPcp.colRow;
    Eigen::Vector3f        proj   = mulMtxVec(mtx, wPosition - sprtFrontWPosition);

    const float K = 0.2f;
    float u = 0.0f;
    float v = 0.0f;

    u = 0.5f + 0.5f * (proj[0] / (sprtMapping[0] * K));
    v = 0.5f + 0.5f * (proj[1] / (sprtMapping[1] * K));

    float frontFacing = wNormal.normalized().dot(get(mtx, 2));

    float mask = frontFacing > 0.01f &&
        0.0f < u && u < 1.0f &&
        0.0f < v && v < 1.0f
        ? 1.0f
        : 0.0f;

    // map sprite's local texture coordinates to the complete spritesheet
    u = (u + static_cast<float>(colRow.x())) * rNumSprites.x();
    v = (v + static_cast<float>(colRow.y())) * rNumSprites.y();

    if (mask > 1e-6f)
    {
        _spriteSheet.get(u, v, texel);
        for (int k = 0; k < 3; ++k)
            if (fabsf(rSprtGamma[k] - 1.0f) > 1e-4f)
                texel[k] = powf(
                    static_cast<float>(texel[k]),
                    rSprtGamma[k]);
    }
    texel.alpha *= mask;
    texel.color *= texel.alpha;

    return texel;
}

int
PlaneTexture::getInt(std::string& name,
                     double       time)
{
    if (name.empty())
        return 0;
    VRayPluginParameter* parm = getParameter(&name.front());
    return parm ? parm->getInt(0, time) : 0;
}

float
PlaneTexture::getFloat(std::string& name,
                       double       time)
{
    if (name.empty())
        return 0.0f;
    VRayPluginParameter* parm = getParameter(&name.front());
    return parm ? parm->getFloat(0, time) : 0.0f;
}

template<int Dim>
Eigen::Matrix<float, Dim, 1>
PlaneTexture::getValue(VectorParmDecl<Dim>& decl,
                       double               time)
{
    Eigen::Matrix<float, Dim, 1> ret;
    for (int i = 0; i < Dim; ++i)
    {
        VRayPluginParameter* parm = getParameter(&decl.names[i].front());
        ret[i] = parm ? parm->getFloat(0, time) : 0.0f;
    }
    return ret;
}

void
PlaneTexture::renderBegin(VRayRenderer* vray)
{
    VRayTexture::renderBegin(vray);
    //---
    // grab sprite sheet bitmap
    updateSpriteSheet();
}

void
PlaneTexture::updateSpriteSheet()
{
    _spriteSheet.clear();

    VRayPluginParameter*    parm = getParameter(&g_SPRITESHEET_FILE.front());
    std::string             file = parm
        ? std::string(parm->getString(0, 0.0f))
        : std::string();
    if (file.empty())
        return;

    _spriteSheet.load(file);
}

void
PlaneTexture::frameBegin(VRayRenderer *vray)
{
    VRayTexture::frameBegin(vray);
    //---
    const double frameStart = vray
        ? vray->getFrameData().frameStart
        : 0.0;
    const double frameEnd = vray
        ? vray->getFrameData().frameEnd
        : 0.0;

    _sceneOffset = vray
        ? vray->getFrameData().sceneOffset
        : VUtils::TracePoint(0.0f, 0.0f, 0.0f);

    // for all requested time samples,
    // precompute sprite data
    assert(NUM_TIME_STEPS > 0);
    _inputs.resize(NUM_TIME_STEPS);
    _pcp   .resize(NUM_TIME_STEPS);

    double timeStep = (frameEnd - frameStart) / static_cast<double>(NUM_TIME_STEPS);
    double time     = frameStart;

    for (size_t i = 0; i < NUM_TIME_STEPS; ++i)
    {
        AllInputs&     inputs = _inputs[i];
        AllSpritesPcp& allPcp = _pcp[i];

        getInputValues(time, inputs);
        precomputeValuesForAllSprites(inputs, allPcp);
        time += timeStep;
    }
}

AColor
PlaneTexture::getTexColor(const VRayContext& rc)
{
    float alpha = 0.0f;
    return getTexColor(rc, alpha);
}

void
PlaneTexture::interpolate(double         rayTime,
                          AllInputs&     inputs,
                          AllSpritesPcp& allPcp) const
{
    float   f0 = static_cast<float>(rayTime * NUM_TIME_STEPS);
    int     i0 = static_cast<int>(floor(f0));
    float   w1 = f0 - static_cast<float>(i0);

    if (i0 < 0)
        i0 = 0;
    if (i0 >= NUM_TIME_STEPS)
        i0 = NUM_TIME_STEPS - 1;

    int i1 = i0 + 1;
    if (i1 >= NUM_TIME_STEPS)
        i1 = NUM_TIME_STEPS - 1;

    interpolate(i0, i1, w1, inputs);
    interpolate(i0, i1, w1, allPcp);
}

void
PlaneTexture::interpolate(int        i0,
                          int        i1,
                          float      w1,
                          AllInputs& ret) const
{
    const float      w0  = 1.0f - w1;
    const AllInputs& in0 = _inputs[i0];
    const AllInputs& in1 = _inputs[i1];

    //--------------------------------------
    // not animated
    ret.numSpriteCols = in0.numSpriteCols;
    ret.numSpriteRows = in0.numSpriteRows;
    ret.sprtIndices   = in0.sprtIndices;
    //--------------------------------------

    ret.cubeCenterWPosition = w0 * in0.cubeCenterWPosition + w1 * in1.cubeCenterWPosition;
    ret.cubeUpWPosition     = w0 * in0.cubeUpWPosition + w1 * in1.cubeUpWPosition;
    ret.cubeFrontWPosition  = w0 * in0.cubeFrontWPosition + w1 * in1.cubeFrontWPosition;
    {
        const std::vector<Eigen::Vector3f>& inVec0 = in0.sprtFrontWPositions;
        const std::vector<Eigen::Vector3f>& inVec1 = in1.sprtFrontWPositions;
        std::vector<Eigen::Vector3f>&       retVec = ret.sprtFrontWPositions;

        retVec.resize (inVec0.size());
        for (size_t i = 0; i < ret.sprtFrontWPositions.size(); ++i)
            retVec[i] = w0 * inVec0[i] + w1 * inVec1[i];
    }
    {
        const std::vector<Eigen::Vector3f>& inVec0 = in0.sprtMappings;
        const std::vector<Eigen::Vector3f>& inVec1 = in1.sprtMappings;
        std::vector<Eigen::Vector3f>&       retVec = ret.sprtMappings;

        retVec.resize (inVec0.size());
        for (size_t i = 0; i < ret.sprtFrontWPositions.size(); ++i)
            retVec[i] = w0 * inVec0[i] + w1 * inVec1[i];
    }
}

void
PlaneTexture::interpolate(int            i0,
                          int            i1,
                          float          w1,
                          AllSpritesPcp& ret) const
{
    const AllSpritesPcp& allPcp0 = _pcp[i0];
    const AllSpritesPcp& allPcp1 = _pcp[i1];

    ret.resize (allPcp0.size());
    for (size_t i = 0; i < ret.size(); ++i)
    {
        //---------------------------------
        // not animated
        ret[i].colRow = allPcp0[i].colRow;
        //---------------------------------
        ret[i].quat = allPcp0[i].quat;
        ret[i].quat.slerp (w1, allPcp1[i].quat);
        //ret[i].mtx = allPcp0[i].mtx;
    }
}

AColor
PlaneTexture::getTexColor(const VRayContext& rc,
                          float&             alpha)
{
    AColor rgba;

    rgba.set(0.0f, 0.0f, 0.0f, 0.0f);
    alpha = 0.0f;

    if (_bodyTexture)
    {
        rgba = _bodyTexture->getTexColor(rc);
    }

    AllInputs     inputs;
    AllSpritesPcp allPcp;

    interpolate(rc.getRayParams().rayTime, inputs, allPcp); // for motion blur

    const size_t numSprites    = inputs.sprtIndices.size();
    const int    numSpriteCols = inputs.numSpriteCols;
    const int    numSpriteRows = inputs.numSpriteRows;
    if (numSprites == 0 || numSpriteCols == 0 || numSpriteRows == 0 ||
        static_cast<bool>(_spriteSheet) == false)
        return rgba;

    const VUtils::TracePoint& wPosition_ = rc.getRayResult().wpoint + _sceneOffset;
    const VUtils::Vector&     wNormal_   = rc.getRayResult().origNormal;

    //-------------------------------------------------
    //return toAColor(wpos);    // DEBUG
    //-------------------------------------------------
    const Eigen::Vector3f wPosition(
        static_cast<float>(wPosition_.x),
        static_cast<float>(wPosition_.y),
        static_cast<float>(wPosition_.z));
    const Eigen::Vector3f wNormal(
        static_cast<float>(wNormal_.x),
        static_cast<float>(wNormal_.y),
        static_cast<float>(wNormal_.z));

    Eigen::Vector2f rNumSprites(
        1.0f / static_cast<float>(numSpriteCols),
        1.0f / static_cast<float>(numSpriteRows));

    //-------------------------------------------------
    //return getSpriteTexel(0, dp, rNumSprites);    // DEBUG
    //-------------------------------------------------

    AColor* texels = new AColor[numSprites];
    for (size_t i = 0; i < numSprites; ++i)
        texels[i] = getSpriteTexel(
            wPosition,
            wNormal,
            inputs.sprtFrontWPositions[i],
            inputs.sprtMappings[i],
            inputs.rSprtGamma,
            allPcp[i],
            rNumSprites);

    texels[PUPIL_L].color *= texels[EYEBALL_L].alpha;
    texels[PUPIL_L].alpha *= texels[EYEBALL_L].alpha;

    texels[PUPIL_R].color *= texels[EYEBALL_R].alpha;
    texels[PUPIL_R].alpha *= texels[EYEBALL_R].alpha;

    texels[UPPER_EYELID_R].color *= texels[EYEBALL_R].alpha;
    texels[UPPER_EYELID_R].alpha *= texels[EYEBALL_R].alpha;

    texels[UPPER_EYELID_L].color *= texels[EYEBALL_L].alpha;
    texels[UPPER_EYELID_L].alpha *= texels[EYEBALL_L].alpha;

    texels[LOWER_EYELID_L].color *= texels[EYEBALL_L].alpha;
    texels[LOWER_EYELID_L].alpha *= texels[EYEBALL_L].alpha;

    texels[LOWER_EYELID_R].color *= texels[EYEBALL_R].alpha;
    texels[LOWER_EYELID_R].alpha *= texels[EYEBALL_R].alpha;

    for (size_t i = 0; i < numSprites; ++i)
    {
        if (alpha < texels[i].alpha)
            alpha = texels[i].alpha;
        rgba = over(texels[i], rgba);
    }
    delete[] texels;

    return rgba;
}

//----------------------------------------------------------------------------
//  ____  _             _         ____
// |  _ \| |_   _  __ _(_)_ __   |  _ \  ___  ___  ___
// | |_) | | | | |/ _` | | '_ \  | | | |/ _ \/ __|/ __|
// |  __/| | |_| | (_| | | | | | | |_| |  __/\__ \ (__
// |_|   |_|\__,_|\__, |_|_| |_| |____/ \___||___/\___|
//                |___/
PLUGIN_DESC(201902181338,
            EXT_TEXTURE,
            "StickerFace_PlaneTexture",
            PlaneTexture,
            PlaneTextureParms);
//----------------------------------------------------------------------------
