// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once
#include <string>
#include <vrayplugins.h>

/////////////////////////////
// struct VectorParmDecl<Dim>
/////////////////////////////
namespace stickerface
{
    template<int Dim>
    struct VectorParmDecl
    {
        std::string names[Dim];

    public:
        explicit inline
        VectorParmDecl(const std::string& name)
        {
            for (int i = 0; i < Dim; ++i)
                names[i] = name + vectorSuffix(i);
        }

        inline
        VectorParmDecl(const VectorParmDecl& other)
        {
            copy(other);
        }

        inline
        VectorParmDecl& operator=(const VectorParmDecl& other)
        {
            copy(other);
            return *this;
        }

        inline
        void
        addTo(VR::VRayParameterListDesc& desc,
              float                      defaultValue = 0.0f)
        {
            const int COUNT = -1;
            for (int i = 0; i < Dim; ++i)
                desc.addParamTextureFloat(
                    &names[i].front(),
                    defaultValue,
                    COUNT);
        }

    private:
        inline
        void
        copy(const VectorParmDecl& other)
        {
            for (int i = 0; i < Dim; ++i)
                names[i] = other.names[i];
        }

        inline
        char
        vectorSuffix(int i) const
        {
            if (i == 1)      return 'Y';
            else if (i == 2) return 'Z';
            else if (i == 3) return 'W';
            else             return 'X';
        }
    };
}
