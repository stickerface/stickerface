// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <stickerface/math.hpp>

namespace stickerface { namespace plane
{
    // precomputation for each sprite
    //-------------------------------
    struct SpritePcp
    {
        Eigen::Vector2i    colRow;
        Eigen::Quaternionf quat;
    public:
        inline
        SpritePcp():
            colRow(0, 0),
            quat  (Eigen::Quaternionf::Identity())
        { }
    };

    // precomputation for all sprites
    //-------------------------------
    typedef std::vector<SpritePcp> AllSpritesPcp;
} }
