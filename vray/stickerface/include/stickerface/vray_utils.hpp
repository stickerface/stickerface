// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <color.h> // vray
#include <vector.h>

#include "math.hpp"

namespace stickerface
{
    // PREREQUISITES : colors A and B must be alpha-premultiplied
    inline
    VR::AColor
    over(const VR::AColor& A,
         const VR::AColor& B)
    {
        VR::AColor ret;

        ret.color = A.color + (1.0f - A.alpha) * B.color;
        ret.alpha = A.alpha + (1.0f - A.alpha) * B.alpha;

        return ret;
    }

    inline
    VR::AColor
    toAColor(const Eigen::Vector3f& v)
    {
        float rgb[3] = { 0.0f, 0.0f, 0.0f };    // DEBUG
        for (int k = 0; k < 3; ++k)
            if (v[k] < -0.5f)
                rgb[k] = 0.0f;
            else if (v[k] > 0.5f)
                rgb[k] = 1.0f;
            else
                rgb[k] = 0.5f;
        return VR::AColor(rgb[0], rgb[1], rgb[2], 1.0f);
    }

    inline
    VR::AColor
    toAColor(const Eigen::Vector2f& uv,
             float                  mask)
    {
        return VR::AColor(
            mask > 0.5f ? uv[0] : 0.5f,
            mask > 0.5f ? uv[1] : 0.5f,
            0.5f,
            1.0f
        );

        //return VR::AColor(
        //    std::max<float>(0.0f, std::min<float>(1.0f, uv[0])),
        //    std::max<float>(0.0f, std::min<float>(1.0f, uv[1])),
        //    mask,
        //    1.0f);
    }

    inline
    VR::AColor
    toAColor(const VUtils::TracePoint& p)
    {
        float xyz[3] = {
            static_cast<float>(p.x),
            static_cast<float>(p.y),
            static_cast<float>(p.z)
        };
        float rgb[3] = { 0.0f, 0.0f, 0.0f };    // DEBUG
        for (int k = 0; k < 3; ++k)
            if (xyz[k] < -0.5f)
                rgb[k] = 0.0f;
            else if (xyz[k] > 0.5f)
                rgb[k] = 1.0f;
            else
                rgb[k] = 0.5f;
        return VR::AColor(rgb[0], rgb[1], rgb[2], 1.0f);
    }
}
