// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once
#include <string>
#include <vrayplugins.h>

/////////////////////////////
// class VectorParmCache<Dim>
/////////////////////////////
namespace stickerface
{
    template<int Dim>
    class VectorParmCache
    {
    private:
        VectorParmDecl<Dim>&           _decl;
        VUtils::TextureFloatInterface* _comps[Dim];

    private:
        // non-copyable
        VectorParmCache(const VectorParmCache&);
        VectorParmCache& operator=(const VectorParmCache&);

    public:
        explicit inline
        VectorParmCache(
            VectorParmDecl<Dim>& decl
        ):
            _decl(decl)
        {
            for (int i = 0; i < Dim; ++i)
                _comps[i] = nullptr;
        }

        inline
        void
        setup(VUtils::VRayParameterList& paramList)
        {
            for (int i = 0; i < Dim; ++i)
                if (!_decl.names[i].empty())
                    paramList.setParamCache(&_decl.names[i].front(), &_comps[i]);
        }

        inline
        void
        getValue(const VR::VRayContext& rc,
                 VectorParm<Dim>&       v) const
        {
            for (int i = 0; i < Dim; ++i)
                v.data[i] = _comps[i]
                    ? static_cast<float>(_comps[i]->getTexFloat(rc))
                    : 0.0f;
        }
    };
}
