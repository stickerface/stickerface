// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <stickerface/math.hpp>

namespace stickerface { namespace sphere
{
    struct Precomps
    {
        std::vector<Eigen::Vector2i> colRow; // (column, row)
        std::vector<Eigen::Matrix3f> frames;
    public:
        inline
        Precomps():
            colRow(),
            frames()
        { }

        inline
        void
        clear()
        {
            colRow.clear();
            frames.clear();
        }

        friend inline
        std::ostream&
        operator<<(std::ostream&   out,
                   const Precomps& p)
        {
            const size_t numSprites = p.frames.size();
            for (size_t i = 0; i < numSprites; ++i)
                out
                    << "sprite " << i << ":\n"
                    << "\t(col, row) = " << p.colRow[i].transpose() << "\n"
                    << "\tframe = \n" << p.frames[i] << "\n";
            return out;
        }
    };
} }
