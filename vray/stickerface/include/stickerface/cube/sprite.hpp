// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>

#include <stickerface/VectorParm.hpp>
#include <stickerface/VectorParmDecl.hpp>
#include <stickerface/VectorParmCache.hpp>

namespace stickerface { namespace cube
{
    /////////////////////
    // struct SpriteParms
    /////////////////////
    struct SpriteParms
    {
        int           index;
        VectorParm<3> frontWPosition;
        VectorParm<3> mapping;
    };

    ////////////////////////
    // struct SpriteParmDecl
    ////////////////////////
    struct SpriteParmDecl
    {
        std::string       indexName;
        VectorParmDecl<3> frontWPositionDecl;
        VectorParmDecl<3> mappingDecl;
    public:
        explicit inline
        SpriteParmDecl(
            const std::string& name
        ):
            indexName         (name + "_index"),
            frontWPositionDecl(name + "_frontWPosition"),
            mappingDecl       (name + "_mapping")
        { }

        inline
        SpriteParmDecl(
            const SpriteParmDecl& other
        ):
            indexName         (other.indexName),
            frontWPositionDecl(other.frontWPositionDecl),
            mappingDecl       (other.mappingDecl)
        { }

        inline
        SpriteParmDecl& operator=(const SpriteParmDecl& other)
        {
            indexName          = other.indexName;
            frontWPositionDecl = other.frontWPositionDecl;
            mappingDecl        = other.mappingDecl;
            return *this;
        }

        inline
        void
        addTo(VR::VRayParameterListDesc& desc)
        {
            const int COUNT = -1;
            desc.addParamTextureFloat(&indexName.front(), 0.0f, COUNT);
            frontWPositionDecl.addTo(desc);
            mappingDecl.addTo(desc);
        }
    };

    ////////////////////////
    // class SpriteParmCache
    ////////////////////////
    class SpriteParmCache
    {
    private:
        SpriteParmDecl&            _decl;
        VR::TextureFloatInterface* _indexCache;
        VectorParmCache<3>         _frontWPositionCache;
        VectorParmCache<3>         _mappingCache;

    private:
        // non-copyable
        SpriteParmCache(const SpriteParmCache&);
        SpriteParmCache& operator=(const SpriteParmCache&);

    public:
        explicit inline
        SpriteParmCache(
            SpriteParmDecl& decl
        ):
            _decl               (decl),
            _indexCache         (nullptr),
            _frontWPositionCache(decl.frontWPositionDecl),
            _mappingCache       (decl.mappingDecl)
        { }

        inline
        void
        setup(VR::VRayParameterList& paramList)
        {
            if (!_decl.indexName.empty())
                paramList.setParamCache(&_decl.indexName.front(), &_indexCache);
            _frontWPositionCache.setup(paramList);
            _mappingCache.setup(paramList);
        }

        inline
        void
        getValue(const VR::VRayContext& rc,
                 SpriteParms&           p) const
        {
            p.index = static_cast<int>(
                _indexCache
                ? static_cast<float>(_indexCache->getTexFloat(rc))
                : 0.0f);
            _frontWPositionCache.getValue(rc, p.frontWPosition);
            _mappingCache.getValue(rc, p.mapping);
        }
    };
} }
