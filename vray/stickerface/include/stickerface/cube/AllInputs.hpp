// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "stickerface/math.hpp"

namespace stickerface { namespace cube
{
    struct AllInputs
    {
        Eigen::Vector3f              rSprtGamma; // 1.0 / gamma
        Eigen::Vector3f              cubeCenterWPosition;
        Eigen::Vector3f              cubeUpWPosition;
        Eigen::Vector3f              cubeFrontWPosition;
        int                          numSpriteCols;
        int                          numSpriteRows;
        std::vector<int>             sprtIndices;
        std::vector<Eigen::Vector3f> sprtFrontWPositions;
        std::vector<Eigen::Vector3f> sprtMappings;
    public:
        inline
        AllInputs():
            rSprtGamma         (Eigen::Vector3f::Ones()),
            cubeCenterWPosition(Eigen::Vector3f::Zero()),
            cubeUpWPosition    (Eigen::Vector3f::Zero()),
            cubeFrontWPosition (Eigen::Vector3f::Zero()),
            numSpriteCols      (0),
            numSpriteRows      (0),
            sprtIndices        (),
            sprtFrontWPositions(),
            sprtMappings       ()
        { }

        inline
        void
        clear()
        {
            rSprtGamma.setOnes();
            cubeCenterWPosition.setZero();
            cubeUpWPosition.setZero();
            cubeFrontWPosition.setZero();
            numSpriteCols = 0;
            numSpriteRows = 0;
            sprtIndices.clear();
            sprtFrontWPositions.clear();
            sprtMappings.clear();
        }

        friend inline
        std::ostream&
        operator<<(std::ostream&    out,
                   const AllInputs& p)
        {
            out
                << "rcp(gamma) = " << p.rSprtGamma.transpose() << "\n"
                << "cube center world position = " << p.cubeCenterWPosition.transpose() << "\n"
                << "cube up world position = " << p.cubeUpWPosition.transpose() << "\n"
                << "cube front world position = " << p.cubeFrontWPosition.transpose() << "\n"
                << "# sprites = " << p.numSpriteCols << " x " << p.numSpriteRows << "\n";
            const size_t numSprites = p.sprtIndices.size();
            for (size_t i = 0; i < numSprites; ++i)
                out
                    << "sprite " << i << ":"
                    << "\tindex = " << p.sprtIndices[i]
                    << "\tfront world pos = " << p.sprtFrontWPositions[i].transpose()
                    << "\tmapping = " << p.sprtMappings[i].transpose() << "\n";
            return out;
        }
    };
} }
