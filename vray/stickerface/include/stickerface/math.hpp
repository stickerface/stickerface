// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <Eigen/Eigen>
#include <Eigen/StdVector>

EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Vector2i)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Vector3f)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Vector4f)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Matrix3f)

namespace stickerface
{
    inline
    float
    sign(float x)
    {
        return x < 0.0f ? -1.0f : 1.0f;
    }

    inline
    float
    radians(float angD)
    {
        static const float M_DEG_2_RAD = static_cast<float>(0.01745329251994329576923690768489);
        return angD * M_DEG_2_RAD;
    }

    inline
    void
    set(Eigen::Matrix3f&       mtx,
        int                    i,
        const Eigen::Vector3f& value)
    {
        mtx.row(i) = value;
    }

    inline
    Eigen::Vector3f
    get(const Eigen::Matrix3f& mtx,
        int                    i)
    {
        return mtx.row(i);
    }

    inline
    Eigen::Vector3f
    mulMtxVec(const Eigen::Matrix3f& mtx,
              const Eigen::Vector3f& v)
    {
        return mtx * v;
    }

    inline
    Eigen::Matrix3f
    mulMtx(const Eigen::Matrix3f& mtx1,
           const Eigen::Matrix3f& mtx2)
    {
        return mtx1 * mtx2;
    }
}
