-- ========================================================================= --
-- Copyright 2019 SUPAMONKS_STUDIO                                           --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --

include '../../script/lua'

workspace 'LodePNG'
    platforms {'x64'}
    configurations {'Release', 'Debug'}
    location(string.format('../%s', compiler_key()))

    project 'LodePNG'
        language 'C++'
        kind 'StaticLib'
        targetdir(string.format('../%s/lib/%%{cfg.buildcfg}', compiler_key()))

        filter {}

        includedirs
        {
            '../include',
        }
        files
        {
            '../**.h',
            '../**.cpp',
        }

        filter 'configurations:Debug'
            symbols 'On'
            defines { 'DEBUG' }

        filter 'configurations:Release'
            optimize 'On'
            defines { 'NDEBUG' }
