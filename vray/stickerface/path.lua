-- ========================================================================= --
-- Copyright 2019 SUPAMONKS_STUDIO                                           --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


-- Edit the following functions to match the paths of the dependencies on
-- your workstation.

EIGEN_ROOT = path.join(
    'E:',
    'dev',
    'repos',
    'smks',
    'supadep',
    'contrib',
    'eigen')

VRAY_SDK_ROOT = path.join(
    'I:',
    'bin',
    '_OFFICIALVRAY',
    'vray_adv_35701_11_maya2015_x64',
    'vray')

--       _
--   ___(_) __ _  ___ _ __
--  / _ \ |/ _` |/ _ \ '_ \
-- |  __/ | (_| |  __/ | | |
--  \___|_|\__, |\___|_| |_|
--         |___/

function eigen_path(...)
    return path.translate(path.join(EIGEN_ROOT, ...), '/')
end


-- __     ______                                           _ _
-- \ \   / /  _ \ __ _ _   _    __ _ _ __  _ __    ___  __| | | __
--  \ \ / /| |_) / _` | | | |  / _` | '_ \| '_ \  / __|/ _` | |/ /
--   \ V / |  _ < (_| | |_| | | (_| | |_) | |_) | \__ \ (_| |   <
--    \_/  |_| \_\__,_|\__, |  \__,_| .__/| .__/  |___/\__,_|_|\_\
--                     |___/        |_|   |_|

function vray_sdk_path(...)
    return path.translate(path.join(VRAY_SDK_ROOT, ...), '/')
end
